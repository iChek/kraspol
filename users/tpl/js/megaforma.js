
function selHotels(){
    var selector = "select#select-service option[value=gostinicy]";
    var className = $(selector).data("class");
    $(selector ).prop("selected", true);
    $("select#select-service").change();
    $(".common_form_elements").show();
    $("." + className).show();
}

$(function(){

    $(".openMegaForm").on("click", function(){
        $("#myMegaForm").modal();
        document.getElementById("feedback_from").reset();
        setTimeout(selHotels,100);
    });

    $("#select-service").on('change', function(){
        var val = $(this).val(),
            selector = "#select-service option[value=" + val + "]",
            className = $(selector).data("class");

        $(selCl).hide();
        if (val != "none") {
            $(".common_form_elements").show();
            $("." + className).show();
        }
    });

    $("form#feedback_from").on('submit', function(ev){
        var fdata = $(this).serialize();

        $("#waiter").modal({
            'backdrop':'static',
            'show':true,
            'keyboard': false
        });

        $.ajax({
            url:'/_ff',
            type: 'post',
            data: fdata,
            dataType: 'json',
            success: function(data) {
                var ids = $("#ids").val().split(",");
                console.log(data,ids);
                $(selCl).hide();
                document.getElementById("feedback_from").reset();
                $("#order_button").attr("disabled", "disabled");
                $("#waiter").modal('hide');
                $("#myMegaForm").modal('hide');
                $("#endsend").modal('show');
                $(".sel_obj").prop("checked", false);
                for(var i = 0; i < ids.length; i++) {
                    $("#basket_checkbox_" + ids[i]).html("");
                    $("#basket_button_" + ids[i]).html("Заявка отправлена");
                    $("#basket_button2_" + ids[i]).html("Заявка отправлена");
                }
            }
        });
        ev.preventDefault();
    });

    $("#agree").on("click", function () {
        if ($(this).prop("checked")) {
            $("#order_button").removeAttr("disabled");
        } else {
            $("#order_button").attr("disabled","disabled");
        }

    });

});
