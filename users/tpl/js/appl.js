
var currentSlide = 0;
function sliderNext() {
    var slides = $("#slider").data("slides").split(',');
    $("#slider").css("background-image","url(/userfiles/moduls/slider/image/" + slides[currentSlide] + ")");
    currentSlide++; if (currentSlide >= slides.length) { currentSlide = 0;}
    setTimeout(sliderNext,slider_delay);
}
if (document.getElementById("slider")) {
    sliderNext();
}

function getChar(event) {
    if (event.which == null) { // IE
        if (event.keyCode < 32) return null; // спец. символ
        return String.fromCharCode(event.keyCode)
    }

    if (event.which != 0 && event.charCode != 0) { // все кроме IE
        if (event.which < 32) return null; // спец. символ
        return String.fromCharCode(event.which); // остальные
    }
    return null; // спец. символ
}

function urlHrefClick() {
    $("a.url-href").unbind().on("click", function(ev){
        var url = $(this).data("href").toLowerCase().trim();
        if (url.indexOf("http://") == -1 && url.indexOf("https://") == -1) {
            url = "http://" + url;
        }
        console.log(url);
        window.open(url);
        ev.preventDefault();
    });
}

$(document).ready(function(){

    $("input.onlyDigit").each(function(){

        $(this).keydown(function(event) {
            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                // Разрешаем: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                // Ничего не делаем
                return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

    });

    $("input.moneyInput").on('keypress', function(event) {

        var sym = getChar(event),
            code = sym.charCodeAt(0);

        // 46 == .
        // 48 == 0 ..  57 == 9
        if ( code == 46 || (code >= 48 && code <= 57)) {
            return;
        } else {
            event.preventDefault();
        }

    });

    $("input.datepicker").datetimepicker({
        weekStart:1,
        language: "ru",
        pickTime: false
    });

    $("a.show-visitings").on('click', function (ev) {

        var member_id = $(this).data("member-id");
        console.log(member_id);
        $("#show-visit-body").html('<i class="fa fa-spinner fa-spin"></i>');
        $("#show-visit-dialog").modal();

        $.get('/sv?mid=' + member_id, function (data) {
            $("#show-visit-body").html(data);
        });
        ev.preventDefault();
    });

    $("#members_table").dataTable({
        "paging":    false,
        "columnDefs": [ {
            "targets"  : "no-sort",
            "orderable": false
        }]
    });

    $("#email_subscr_btn").on("click", function(ev){
        var key_code = $("#key_code").val(),
            src_code = $("#src_code").val(),
            email = $("#email_subscr").val();

        if (email != "") {

            swal({
                    title: "Подписаться на рассылку?",
                    text: "Нажимая 'Да' Вы соглашаетесь на обработку персональных данных и получение от нас информационных писем",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "green",
                    confirmButtonText: "Да",
                    cancelButtonText: "Нет",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        url:'/_subscr',
                        type: 'post',
                        dataType: 'text',
                        data: { subscr:1, key_code:key_code, src_code:src_code, email:email },
                        success: function(resp) {
                            if (resp == 'good') {
                                swal('Адрес добавлен','Ваш e-mail добавлен в нашу базу! Спасибо за подписку.','info');
                            }
                            $("#email_subscr").val("");
                        }
                    });
                }
            );

        }
        ev.preventDefault();
    });

    // $(".btn-search-main").on("click", function() {
    //
    //     var serv_category = $("#serv_category").val();
    //     if (serv_category == 'none') {
    //         swal("Внимание!",  "Не выбран раздел поиска!", "info");
    //     } else {
    //         $("#form-search-main").submit();
    //     }
    //
    // });

    urlHrefClick();


    $(".selected-control").hover(
        function(){
            $(this).find(".ul-value").show("fast");
        },
        function(){
            $(this).find(".ul-value").hide("fast");
        }
    );

    $("a.item-click").on("click", function(ev){
        var name = $(this).data("input");
        var itemid = $(this).data("itemid");
        var title = $(this).text();
        $("#" + name).val(itemid);
        $(this).closest(".selected-control").find(".text-arrow").text(title);
        $(this).closest(".selected-control").find("li.list-item-select-active").removeClass("list-item-select-active");
        $(this).closest("li").addClass("list-item-select-active");
        $(".selected-control").find(".ul-value").hide("fast");
        ev.preventDefault();
    });

});