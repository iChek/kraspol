$(function(){

    var count = $("#detail-thumbs img").length,
        current = -1,
        galTimer = 0;

    function drawGalleryImg(currentImg) {

        var obj = $("#detail-thumbs a:eq(" + currentImg + ")"),
            src = obj.find("img").attr("src");

        $("#detail-thumbs a").removeClass("active");
        obj.addClass("active");
        $("#detail-info").css("background-image","url(" + src + ")");

        clearTimeout(galTimer);
        galTimer = setTimeout(nextGalleryImg, 10000);
    }

    function nextGalleryImg() {

        current++;
        if (current >= count) {
            current = 0;
        }
        drawGalleryImg(current);

    }
    nextGalleryImg();

    $("a.detail-show-img-gallery").on("click", function(ev){
        current = $(this).data("indx");
        drawGalleryImg(current);
        ev.preventDefault();
    });

    $("a.slide_nav_next").on("click", function(ev){
        nextGalleryImg();
        ev.preventDefault();
    });

    $("a.slide_nav_prev").on("click", function(ev){
        current--; if (current < 0) { current = count-1; }
        drawGalleryImg(current);
        ev.preventDefault();
    });

    // Gallery
    if (document.getElementById("detail-gallery")) {

        console.log("data-fancyboxz=gallery");
        $("[data-fancyboxz=gallery]").fancybox({
            infobar : true,
            buttons : true,
            slideShow  : true,
            fullScreen : true,
            thumbs     : true,
            closeBtn   : true,
            smallBtn : 'auto'
        });

    }

});