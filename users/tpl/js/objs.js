var startItem = 0,
    numItems = 15,
    isMap = false;

function getBlockItems() {
    $("#waitSpinner").show();
    $("#addBtn").hide();
    $.ajax({
        url: "/ajaxobj",
        type: "post",
        data: $("#form-search-data").serialize(), //  { "type": blockName, "startItem":startItem, "numItems":numItems },
        dataType: "json",
        success: function (resp) {
            if (resp.result) {
                $("#waitSpinner").hide();
                if (resp.cnt == 15) $("#addBtn").show();
                $("#objTable").append(resp.data);
                $("#objTable2").append(resp.data2);
                
                $(".sel_obj_form").bind().on("click", function(ev){
                    f_sel_obj_form(this);
                    ev.preventDefault();
                });
                
                if (resp.data3 != 'no') {
                    $("#objTable3").append(resp.data3);
                    isMap = true;
                    $(".objtab3").show();
                }

                startItem += numItems;

                $('[data-toggle=popover]').popover({trigger:'hover'});

                $(".sel_obj_form").unbind().on("click", function(ev){
                    f_sel_obj_form(this);
                    ev.preventDefault();
                });

                urlHrefClick();
            }
        },
        error: function(a,b,c,d) {
            console.log(a.responseText);
        }
    });
}

function f_sel_obj_form(obj){

    var selname = $(obj).data("selname"),
        formname = $(obj).data("formname"),
        id = $(obj).data("id");

    $("#basket_button_" + id + " a").hide();
    $("#basket_button2_" + id + " a").hide();
    $("#basket_button_spinner_" + id).show();
    $("#basket_button2_spinner_" + id).show();

    $.ajax({
        url: '/_getobj',
        type: 'get',
        data: { id:id, type:formname },
        dataType: 'json',
        success: function(resp) {

            var obj = resp.object;
            var detail = resp[formname];

            $("#select-service-form-group").hide();

            $("#ids").val(id);
            $("select#select-service option[value=" + selname + "]").prop("selected", true);
            $("select#select-service").change();

            $("select#territory option[value=" + obj.territory_id + "]").prop("selected", true);
            if (detail.hoteltype_id != undefined) $("select#hoteltype option[value=" + detail.hoteltype_id + "]").prop("selected", true);
            if (detail.resttype_id != undefined) $("select#resttype option[value=" + detail.resttype_id + "]").prop("selected", true);
            if (detail.transporttype_id != undefined) $("select#transporttype option[value=" + detail.transporttype_id + "]").prop("selected", true);
            if (detail.beautytype_id != undefined) $("select#beautiestype option[value=" + detail.beautytype_id + "]").prop("selected", true);
            if (detail.servicetype_id != undefined) $("select#servicetype option[value=" + detail.servicetype_id + "]").prop("selected", true);

            $(".gostinicy-hides").hide();
            $(".restorany-kafe-bary-hides").hide();
            $(".transport-hides").hide();
            $(".beauties-hides").hide();
            $(".services-hides").hide();

            $("#basket_button_" + id + " a").show();
            $("#basket_button2_" + id + " a").show();
            $("#basket_button_spinner_" + id).hide();
            $("#basket_button2_spinner_" + id).hide();
            $("#myMegaForm").modal();

        },
        error: function(resp) {
            console.log(resp);
            var msg = "Внутренная ошибка!";
            if (resp.responseJSON != undefined) {
                msg = resp.responseJSON.message;
            }
            swal("Ошибка!", msg, "error");
        }
    });

}

$(function(){

    $(".addPositions").on("click", function(ev){ getBlockItems(); });

    $("a.thsort").on("click", function(ev){
        var field = $(this).data("sort-field");
        $("#order-field").val(field);
        $("#order-form").submit();
        ev.preventDefault();
    });

    if (typeof noGetBlockItems === "undefined") getBlockItems();

    $("a.objtab2").on('click', function(ev){
        $(this).siblings().removeClass("sactive");
        $(this).addClass("sactive");
        $("#objTable").hide();
        $("#objTable2").show();
        $("#objTable3").hide();
        ev.preventDefault();
    });

    $("a.objtab").on('click', function(ev){
        $(this).siblings().removeClass("sactive");
        $(this).addClass("sactive");
        $("#objTable").show();
        $("#objTable2").hide();
        $("#objTable3").hide();
        ev.preventDefault();
    });

    $("a.objtab3").on('click', function(ev){
        $(this).siblings().removeClass("sactive");
        $(this).addClass("sactive");
        $("#objTable").hide();
        $("#objTable2").hide();
        $("#objTable3").show();
        if (isMap) initMap();
        ev.preventDefault();
    });

    $(".sel-many-objs-send").on("click", function (ev) {

        var selname = $(this).data("selname");
        var count = 0, checkit = false, ids = "";
        $(".sel_obj").each(function(){
            if ($(this).prop("checked")) {
                count++;
                checkit = true;
                if (ids != "") ids += ",";
                ids += $(this).data("id");
            }
        });

        if (checkit && count >= 1 && count <= 5) {
            $("#select-service-form-group").hide();
            $("#ids").val(ids);
            $("select#select-service option[value=" + selname + "]").prop("selected", true);
            $("select#select-service").change();
            $("#myMegaForm").modal();

            // скрывать не нужные элементы в зависимости от типа выбранного
            $("." + selname + "-hides").hide();

        } else {
            swal("Внимание!","Для оптимальной работы сервиса заказа услуг Вы можете выбрать от 1 до 5 позиций","info");
        }

        ev.preventDefault();
    });

})

