<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'enterts';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Развлечения',
        '_title' => 'Развлечения | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Развлечения',
        '_description' => 'Развлечения в Красной Поляне, расположение, контакты, найти',
        '_sliders' => 'svoya_priroda_20.jpg',
        'searchForm' => '[%html/searches/enterts%]'
    );

    //$checkboxes_class = 'col-sm-1';
    $checkboxes_offset = '';
    $checkboxes_array = Array();

    $nameForm = '';
    $selectedTerritory = 0;
    $selectedTransporttype = 0;
    $selectedServicetype = 0;

    include '_common_block1.inc';

    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name']))
            $nameForm = $srch['name'];
        if (isset($srch['territory']))
            $selectedTerritory = $srch['territory'];
        if (isset($srch['servicetype']))
            $selectedServicetype = $srch['servicetype'];
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;

    $sort_field = 'sort_' . $ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-' . ($ords['dir'] == '' ? 'asc' : 'desc') . '"></i>';

//    $tpl->addArray($meta);
//    $tpl->add('hoteltypeSelect', getHoteltypeSelect());
//    $tpl->add('distCblcarSelect', getCblcarSelect());
//    $tpl->add('priceSelect', getPriceSelect());
    $_telo .= '
        <tr class="thead">
            <td style="width:1%;">фото</td>
            <td><a href="#" class="thsort" data-sort-field="title">Наименование' . (isset($sort_title) ? $sort_title : '') . '</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение' . (isset($sort_territory_id) ? $sort_territory_id : '') . '</a></td>
            <td>Контакты</td>
            <!-- <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="krasota-i-zdorove">отправить запрос<br>выбранным</button></td> -->
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
        $_telo .= '<div class="row">
                       <div class="col-md-12">
                           <h3>В этом разделе сайта Вы можете найти все развлечения в Красной Поляне.</h3> 
                           Развлекательные центры, кинотеатр, бильярд или боулинг, аквапарки, бассейны и многое другое на курортах Роза Хутор, ГТЦ "Газпром", Альпика и Горки Город.
                        </div>
                    </div>';
    $_telo .= '<script> var blockName = "enterts"; </script>';
    include '_common_block2.inc';
} else {

    $ses->redirect('/');
}