<?php

function newsSetTemplateVariables($n, $row) {

    $image = getCuteImage($n, 'news', $row['id']);
    if (!$image) { $image = '/userfiles/moduls/news/image/' . $row['image']; }

    $materialType = $row['partner'] == 1 ? 'Реклама' : 'Новости';
    $pubDate = date('d.m.Y', $row['pubdate']);

    $item = <<<ITEM
    <li class="news__item">
        <div class="news__div">
            <a href="/event-blog-news/{$row['folder']}" class="news__href">
                <img src="{$image}" class="news__img">
            </a>
            <div class="news__material">{$materialType}</div>
            <div class="news__date">{$pubDate}</div>
            <div class="news__title">{$row['title']}</div>
            <div class="news__anonce">{$row['anonce']}</div>
            <a href="/event-blog-news/{$row['folder']}" class="news__next">читать далее</a>
        </div>
    </li>
ITEM;
    return trim($item);
}

function newsSetTemplateVariables_old($n, $row) {

    $image = getCuteImage($n, 'news', $row['id']);
    if (!$image) { $image = '/userfiles/moduls/news/image/' . $row['image']; }

    $materialType = $row['partner'] == 1 ? 'Реклама' : 'Новости';
    $item = '';
    $item .= '<li class="news__item">';
    $item .= '    <a href="/event-blog-news/'.$row['folder'].'" class="news__href">';
    $item .= '        <div class="news__div">';
    $item .= '            <img src="' . $image . '" class="news__img">';
    $item .= '            <div class="news__material">' . $materialType . '</div>';
    $item .= '            <div class="news__link">';
    $item .= '                <a href="/event-blog-news/'.$row['folder'].'" class="news__href">' . $row['title'] . '</a>';
    $item .= '            </div>';
    $item .= '            <div class="news_anonce">' . $row['anonce'] . '</div>';
    $item .= '        </div>';
    $item .= '    </a>';
    $item .= '</li>';

    return $item;
}


$structs = new Table('structs');
$structs->setOptions(Array('where'=>" (`folder`='main') AND (`status`='1') "));
$row = $structs->getRow();
//for($i = 0; $i < 2000; $i++) { unset($ses['smo_'.$i]); }

$tpl->add('_title', 'Единый туристический справочник Красной Поляны');
$tpl->add('_description', $row['seo_description']);

//$tpl->add('addit_styles','<link rel="stylesheet" href="/users/tpl/css/body.css">');

$n = new Table('news');

$w  = " (`pubdate`<='".time()."') and ";
$w .= " (`dtstart`<='".time()."') and ";
$w .= " (`dtfinish`>='".time()."') and ";
$w .= " (`partner`=1) ";

for($i = 0; $i < 100000; $i++) $parts[$i] = '';

$allcounts = 0;
unset($ids);
$n->setOptions(Array('where' => $w, 'order' => " `pubdate` desc "));
$news = $n->getAllRows();
if ($news) {
    $allcounts += count($news);
    $x = 0;
    foreach ($news as $key => $row) {
        $parts[$x] = newsSetTemplateVariables($n, $row);
        $x += 2;
        $ids[$row['id']] = $row['id'];
    }
}

$w  = " (`pubdate`<='".time()."') and ";
$w .= (isset($ids)?" (`id` not in (".join(',',$ids).")) and ":'');
$w .= " (`partner` = 0) ";

$n->setOptions(Array('where'=> $w, 'order' => " `pubdate` desc "));
$news = $n-> getAllRows();
if ($news) {
    $allcounts += count($news);
    $x = 1;
    foreach ($news as $key => $row) {
        $parts[$x] = newsSetTemplateVariables($n, $row);
        $x += 2;
    }
}

if (isset($parts)) {

    $counts = 0;
    $cols = Array('','','','');
    foreach ($parts as $key => $row) {
        if (trim($row) != '') {
            $cols[$counts%4] .= $row;
            $counts++;
            if ($counts >= 20) {
                break;
            }
        }
    }

    for($i = 0; $i < 4; $i++) $_telo .= '<div class="news_rows col-lg-3 col-md-4 col-xs-12"><ul id="news">'.$cols[$i].'</ul></div>';

    if ($allcounts > 20) {

        $_telo .= '
        <div class="clearfix">
            <div class="btn_more">
                <a href="/blogs_and_news/" class="btn btn_blackself form-control">дальше</a>
            </div>
        </div>
        <p>&nbsp;</p>
        ';

    }

    //blogs_and_news


} else {
    $_telo .= '<p>Нет данных</p>';
}
