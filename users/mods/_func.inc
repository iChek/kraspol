<?php

$pr = new Table('sprav_price');

function getWherePrice($table, $price_from, $price_until){

    $price_from = round($price_from);
    $price_until = round($price_until);

    if ($price_from == 0 && $price_until == 0) {
        return null;
    }

    global $pr;
    $where = '';
    if ($price_from != 0) {
        $pr1 = $pr->getRowByID(round($price_from));
        $where .= " (`from` >= '".$pr1['from']."') ";
    }
    if ($price_until != 0) {
        $pr2 = $pr->getRowByID(round($price_until));
        if ($where != '') { $where .= ' and '; }
        $where .= " (`until` <= '".$pr2['until']."') ";
    }

    $pr->setOptions(Array('where' => $where));
    $prs = $pr->getAllRows();
    if ($prs) {
        unset($ids);
        foreach($prs as $kprs => $vprs) {
            $ids[] = $vprs['id'];
        }
        if (isset($ids)) {
            if ($table == 'hotels') {
                return ' ((`summerprice_id` in (' . join(',', $ids) . ')) or 
                      (`winterprice_id` in (' . join(',', $ids) . '))) ';
            } elseif ($table == 'restaurants') {
                return ' (`middlecheck_id` in (' . join(',', $ids) . ')) ';
            }
        }
    }
    return null;
}
function getWherePriceHotels($table, $price_from, $price_until){

    $price_from = round($price_from);
    $price_until = round($price_until);

    if ($price_from == 0 && $price_until == 0) {
        return null;
    }

    
    return '('
            . ' (`summerprice_id` >= "' . (int)$price_from . '" AND `summerprice_id` <= "' . (int)$price_until . '")'
            . ' OR '
            . '(`winterprice_id` >= "' . (int)$price_from . '" AND `winterprice_id` <= "' . (int)$price_until . '")'
            . ') ';
    
}

function getWherePriceRestaraunts($table, $price_from, $price_until){

    $price_from = round($price_from);
    $price_until = round($price_until);

    if ($price_from == 0 && $price_until == 0) {
        return null;
    }

    
    return '(`middlecheck_id` >= "' . (int)$price_from . '" AND `middlecheck_id` <= "' . (int)$price_until . '") ';
    
}

function getSpravArray($name, $order = ' `title` ') {
    $t = new Table($name);
    $t->setOptions(Array( 'order' => $order ));
    $ts = $t->getAllRows();
    if ($ts) {
        unset($result);
        $result[0] = (object)Array('id' => 0, 'title' => '---');
        $result['none'] = (object)Array('id' => 'none', 'title' => '---');
        foreach ($ts as $row) {
            $result[$row['id']] = (object)$row;
        }
        return $result;
    } else {
        return Array(0 => (object)Array('title' => '---'), 'none' => (object)Array('title' => '---'));
    }
}

function getSpravSelect($name, $none, $first, $selected) {

    $t = new Table($name);
    $t->setOptions(Array('order' => ' `pos`, `title` '));
    $ts = $t->getAllRows();

    $result = '';
    if ($first) {
        $result .= '<option value="none">'.$none.'</option>';
    }
    if ($ts) {
        foreach ($ts as $k => $row) {
            $result .= '<option value="'.$row['id'].'" '.($row['id'] == $selected?'selected':'').'>' . $row['title'] . '</option>';
        }
    }
    return $result;
}

function getResttypeSelect($none = '... ресторан ...', $first = true, $selected = 0) { return getSpravSelect('sprav_resttype', $none, $first, $selected); }
function getKitchenSelect($none = '... кухня ...', $first = true, $selected = 0) { return getSpravSelect('sprav_kitchen', $none, $first, $selected); }
function getMiddleCheckSelect($none = '... средний чек ...', $first = true, $selected = 0) { return getSpravSelect('sprav_middle_check', $none, $first, $selected); }
function getTerritorySelect($none = '... расположение ...', $first = true, $selected = 0) { return getSpravSelect('sprav_territory', $none, $first, $selected); }
function getHoteltypeSelect($none = '... тип отеля ...', $first = true, $selected = 0) { return getSpravSelect('sprav_hoteltype', $none, $first, $selected); }
function getCblcarSelect($none = '... до канатки ...', $first = true, $selected = 0) { return getSpravSelect('sprav_distance_cabcar', $none, $first, $selected); }
function getPriceSelect($none = '... цена ...', $first = true, $selected = 0) { return getSpravSelect('sprav_price', $none, $first, $selected); }
function getWorktimeSelect($none = '... время работы ...', $first = true, $selected = 0) { return getSpravSelect('sprav_price', $none, $first, $selected); }
function getTransporttypeSelect($none = '... вид транспорта ...', $first = true, $selected = 0) { return getSpravSelect('sprav_transport_type', $none, $first, $selected); }
function getСhildrenactivitySelect($none = '... вид активности ...', $first = true, $selected = 0) { return getSpravSelect('sprav_children_activity', $none, $first, $selected); }
function getBeautytypeSelect($none = '... вид ...', $first = true, $selected = 0) { return getSpravSelect('sprav_beautytype', $none, $first, $selected); }
function getServicetypeSelect($none = '... вид ...', $first = true, $selected = 0) { return getSpravSelect('sprav_servicetype', $none, $first, $selected); }
function getShoptypeSelect($none = '... вид ...', $first = true, $selected = 0) { return getSpravSelect('sprav_shoptype', $none, $first, $selected); }

function getSpravList($name, $sprav_name, $sprav_title, $selected) {

    $selected = $selected != 0 ? $selected : 'none';

    $t = new Table($sprav_name);
    $t->setOptions(Array('order' => ' `pos`, `title` '));
    $ts = $t->getAllRows();

    $html = '';
    if ($ts) {
        $html .= '<input type="hidden" name="'.$name.'" id="'.$name.'" value="'.$selected.'">';
        $text_value = $sprav_title;

        $list = '';

        if ($selected != 0 && $selected != 'none') {
            $list .= '<li><a href="#" data-itemid="none" data-input="'.$name.'" class="item-click">Всё (сбросить параметр)</a></li>';
        }

        foreach ($ts as $k => $row) {
            $id = $row['id'];
            $title = $row['title'];
            $active = '';
            if ($id == $selected) {
                $text_value = $title;
                $active = 'list-item-select-active';
            }
            $list .= '<li class="'.$active.'"><a href="#" data-itemid="'.$id.'" data-input="'.$name.'" class="item-click">'.$title.'</a></li>';
        }

        $html .= '<div class="text-value"><div class="text-arrow">'.$text_value.'</div></div>';
        $html .= '<div class="ul-value"><ul>'.$list.'</ul></div>';
    }
    return $html;

}


function getMapScript($coords) {

    return '
        <style>
            #map {
                height: 600px;
                width: 100%;
            }
        </style>
        <div id="map"></div>
        <script>
            var loc = [' . join(',', $coords) . '];
            function initMap() {
                var map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 13,
                    center: new google.maps.LatLng(43.6808951, 40.2387271)
                });
                
                var infowindow = new google.maps.InfoWindow();
                var marker, i;
                
                for (i = 0; i < loc.length; i++) {  
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(loc[i][1], loc[i][2]),
                        map: map
                    });
                
                    google.maps.event.addListener(marker, "click", (function(marker, i) {
                        return function() {
                            infowindow.setContent(loc[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDViRzlYb0tx2sK3yDFq39kfMmtxGTjrhk"></script>
    ';
}

function getObjectContacts($object_id, $row_title) {

    $l = 0;
    unset($coords);
    $objc = new Table('objects_contacts');
    $contacts1 = '';
    $contacts2 = '';
    $objc->setOptions(Array('where' => " `object_id`='$object_id' "));
    $objcs = $objc->getAllRows();
    if ($objcs) {

        $contacts2 .= '<td class="address2" colspan="3" style="/*width: 1%;*/">';
        foreach($objcs as $oc){
            $contacts1 .= '<div class="tel_wrap" style="margin-bottom: 7px;">';
            if (trim($oc['address']) != '') $contacts1 .= '<i class="fa fa-globe"></i> '.$oc['address'].'<br>';
            if (trim($oc['phone']) != '') $contacts1 .= '<span class="tel" style="white-space:nowrap;"><i class="fa fa-phone"></i> '.$oc['phone'].'</span><br>';
            if (trim($oc['email']) != '') $contacts1 .= '<i class="fa fa-at"></i> '.$oc['email'].'<br>';
            $contacts1 .= '</div>';

            if (trim($oc['address']) != '') $contacts2 .= '<i class="fa fa-globe"></i> '.$oc['address'].'<br>';
            if (trim($oc['phone']) != '') $contacts2 .= '<i class="fa fa-phone"></i> '.$oc['phone'].'<br>';
            if (trim($oc['email']) != '') $contacts2 .= '<i class="fa fa-at"></i> '.$oc['email'].'<br>';

            $titl = str_replace('"','&quot;',$row_title);
            $addr = str_replace('"','&quot;',$oc['address']);
            $phone = str_replace("\n",'#',$oc['phone']);

            $contentString = '<div><h3>' . $titl . '</h3><br>Адрес: ' . $addr . '<br>Телефон: ' . $phone . '<br>E-mail: ' . $oc['email'] . '</div>';
            $lat = $oc['latitude'];
            $lng = $oc['longitude'];
            if ($lat != 0 && $lng != 0) {
                $coords[$l] = '["' . $contentString . '", ' . $lat . ', ' . $lng . ']';
                $l++;
            }
        }
        $contacts2 .= '</td>';
    }

    return Array($contacts1, $contacts2, isset($coords) ? $coords : null);

}


function getObjAllItems($h, $table, $where, $order, $startItems, $numItems) {

    $numItems = $numItems + 1000;
//    $hash_name  = 'cache/razdels'; @mkdir($hash_name);
//    $hash_name .= '/'.$table; @mkdir($hash_name);
//    $hash_name .= '/'.md5($table.$where.$order.$startItems.$numItems).'.dat';
//
//    if (!file_exists($hash_name)) {

        $nowDt = time();
        $sql = "
    select
        h.*,
        ifnull(adv.pubtype_id, 99) as pubtype,
        adv.pubcategory_id,
        adv.dtstart,
        adv.dtfinish
    from (
        select
            tab.*,
            obj.title, obj.linktype, obj.folder, obj.client_id, obj.territory_id, obj.site, obj.worksummer, obj.workwinter, obj.descr, obj.short, obj.noorder
        from " . cfg::$prfx . $table . " tab
        join " . cfg::$prfx . "objects obj on tab.object_id = obj.id
    ) h
    left join " . cfg::$prfx . "adv as adv on (adv.object_id = h.object_id) and (`adv`.`dtstart` <= '" . $nowDt . "') and (`adv`.`dtfinish` >= '" . $nowDt . "')
    where " . $where . "
    order by `pubtype`, " . $order . "
    limit " . $startItems . "," . $numItems . "
    ";

        //##echo $sql; exit;

        unset($hs);
        $sth = $h->_q($sql);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $hs = $sth->fetchAll();
//        file_put_contents($hash_name, serialize($hs));
//
//    } else {
//        $hs = unserialize(file_get_contents($hash_name));
//    }

    return $hs;

}

function getAllItems($h, $where, $order, $startItems, $numItems = null) {

    if ($numItems == null) {
        $numItems = 1000;
    }
    $nowDt = time();
    $sql = "
    select
        h.*,
        ifnull(adv.pubtype_id, 99) as pubtype,
        adv.pubcategory_id,
        adv.dtstart,
        adv.dtfinish
    from (
        select * from ".cfg::$prfx."objects obj
    ) h
    left join ".cfg::$prfx."adv as adv on (adv.object_id = h.id) and (`adv`.`dtstart` <= '".$nowDt."') and (`adv`.`dtfinish` >= '".$nowDt."')
    where ".$where."
    order by `pubtype`, ".$order."
    limit ".$startItems.",".$numItems."
    ";

    //##echo '<pre>'.$sql; exit;

    unset($hs);
    $sth = $h->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $hs = $sth->fetchAll();
    return $hs;
}

function getImagePic($h, $id) {

    $error = false;
    @mkdir('cache/cover/');
    global $helper;
    $sql = "SELECT * FROM `krasp_objects_gallery` WHERE (`parent_id` = $id) and (`cover` = 1) limit 0,1";
    $sth = $h->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $imgrow = $sth->fetch();
    if ($imgrow) {

        $tname = 'galleries/objects/'.$id.'/'.$imgrow['image'];

        if (file_exists($tname)) {

            $x1 = $imgrow['x1'];
            $x2 = $imgrow['x2'];
            $y1 = $imgrow['y1'];
            $y2 = $imgrow['y2'];
            $cname = 'cache/cover/' . md5($tname . $x1 . $x2 . $y1 . $y2) . '.jpg';

            if ($x2 != 0 && $y2 != 0 && ($x2 - $x1) != 0 && ($y2 - $y1) != 0) {

                if (!file_exists($cname)) {
                    $ext = strtolower(substr($tname,strrpos($tname,'.'),100));

                    $w = $x2 - $x1;
                    $h = $y2 - $y1;
                    if ($ext=='.jpg' || $ext=='.jpeg') {
                        $imo = imagecreatefromjpeg($tname);
                    } elseif ($ext=='.png') {
                        $imo = imagecreatefrompng($tname);
                    }
                    $im = imagecreatetruecolor($w, $h);
                    imagecopy($im, $imo, 0, 0, $x1, $y1, $w, $h);
                    if ($ext=='.jpg' || $ext=='.jpeg') {
                        imagejpeg($im, $cname, 100);
                    } elseif ($ext=='.png') {
                        imagepng($im, $cname);
                    }
                }
                $image = '/' . $cname;
            } else {
                $image = '/' . $tname;
            }

        } else {
            $error = true;
        }
    } else {
        $error = true;
    }

    if ($error) {
        $image = '/users/tpl/images/logocube.png';
    }
    return $image;
}

function getCuteImage($t, $activity, $id) {

    @mkdir('cache/cover/');
    @mkdir('cache/cover/'.$activity);

    $data = Array(
        'news' => Array(
            'field' => 'image',
            'path' => 'userfiles/moduls/news/image/'
        ),
        'objects' => Array(
            'field' => 'logo',
            'path' => 'userfiles/moduls/objects/logo/'
        )
    );

    $item = $t->getRowByID($id);
    if ($item) {

        $tname = $data[$activity]['path'].$item[$data[$activity]['field']];
        if (file_exists($tname)) {

            $x1 = $item['x1'];
            $x2 = $item['x2'];
            $y1 = $item['y1'];
            $y2 = $item['y2'];
            $cname = 'cache/cover/'.$activity.'/'.md5($tname . $x1 . $x2 . $y1 . $y2) . '.jpg';

            if (!file_exists($cname)) {
                if ($x2 != 0 && $y2 != 0 && ($x2 - $x1) != 0 && ($y2 - $y1) != 0) {

                    $ext = strtolower(substr($tname,strrpos($tname,'.'),100));

                    $w = $x2 - $x1;
                    $h = $y2 - $y1;

                    if ($ext=='.jpg' || $ext=='.jpeg') {
                        $imo = imagecreatefromjpeg($tname);
                    } elseif ($ext=='.png') {
                        $imo = imagecreatefrompng($tname);
                    }
                    $im = imagecreatetruecolor($w, $h);
                    imagecopy($im, $imo, 0, 0, $x1, $y1, $w, $h);
                    if ($ext=='.jpg' || $ext=='.jpeg') {
                        imagejpeg($im, $cname, 100);
                    } elseif ($ext=='.png') {
                        imagepng($im, $cname);
                    }

                } else {
                    return false;
                }
            } else {}
            return '/'.$cname;

        } else {
            return false;
        }

    } else {
        return false;
    }

}