<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'restaurants';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Рестораны',
        '_title' => 'Рестораны | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Кафе, бары, рестораны ',
        '_description' => 'Рестораны, кофейни, кафе, бары в Красной ПолянеНа курортах Роза Хутор, Альпика, Горки Город и  ГТЦ Газпром. Полный перечень заведений, где можно всегда не только вкусно поесть, но и отлично провести время в компании друзей и единомышленников.',
        'searchForm' => '[%html/searches/restaurants%]'
    );

    $checkboxes_offset = 'col-md-offset-2';
    $checkboxes_array = Array(
        Array(
            'name' => 'alltime',
            'title' => 'Круглосуточно',
            'col' => 2
        ),
        Array(
            'name' => 'delivery',
            'title' => 'Доставка',
            'col' => 2
        ),
        Array(
            'name' => 'childroom',
            'title' => 'Детская комната',
            'col' => 2
        ),
        Array(
            'name' => 'terrace',
            'title' => 'Терраса',
            'col' => 2
        )
    );

    $max_slider = 10000;
    
    $price_from2 = 0;
    $price_until2 = $max_slider;
    
//    $middlecheck = 0;
//    $middlecheck2 = 0;
    $nameForm = '';
        $_telo .= '<div class="row">
                    <div class="col-md-12" style="background: #f5f1f1;margin-top: -10px;padding: 0 10px;">
                        <h3>Рестораны, кофейни, кафе, бары в Красной Поляне</h3> 
<p>Представляем самый полный список «Где поесть в Красной Поляне» на зимний сезон 2018/19. Все-все рестораны, кофейни, бары, пиццерии и клубы Красной Поляны, Роза Хутор, Альпика, Горки Город и  ГТЦ "Газпром". Найдите недорогое кафе рядом с вами, чтобы перекусить, выберите ресторан для романтического ужина, забронируйте места для компании в пабе на вечер. В нашем списке полный перечень заведений с телефонами и адресами, где вы сможете покормить детей, провести время в компании друзей, сытно и недорого пообедать после катания на лыжах.</p>

<p>К началу зимнего сезона 2018/19 в Красной Поляне готовы почти 250 заведений для еды и выпивки, и представлены практически все кухни мира. Великолепные бургерные с крафтовым пивом, отличные пиццерии, где есть настоящий сыр, аутентичные рестораны кавказской кухни, и конечно, Макдональдс на Розе Хутор!</p>

                    </div>
                </div>';
    $selectedTerritory = 0;
    $selectedHoteltype = 0;
    $selectedDist_cblcar = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];
        if (isset($srch['resttype'])) $selectedResttype = $srch['resttype'];
        if (isset($srch['kitchen'])) $selectedKitchen = $srch['kitchen'];
        
        if (isset($srch['price_from2'])) $price_from2 = round($srch['price_from2']);
        if (isset($srch['price_until2'])) $price_until2 = round($srch['price_until2']);
//        if (isset($srch['middlecheck'])) $selectedMiddleCheck = round($srch['middlecheck']);
//        if (isset($srch['middlecheck2'])) $selectedMiddleCheck2 = round($srch['middlecheck2']);

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;
    $sort_resttype_id = $sort_item_pin;
    $sort_middlecheck_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="resttype_id">Тип&nbsp;заведения'.(isset($sort_resttype_id)?$sort_resttype_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="middlecheck_id">Средний&nbsp;чек'.(isset($sort_middlecheck_id)?$sort_middlecheck_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);
//
//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2"><a href="#" class="thsort" data-sort-field="title">Название ресторана'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th>Кухня</th>
//            <th><a href="#" class="thsort" data-sort-field="resttype_id">Тип&nbsp;заведения'.(isset($sort_resttype_id)?$sort_resttype_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="middlecheck_id">Средний&nbsp;чек'.(isset($sort_middlecheck_id)?$sort_middlecheck_id:'').'</a></th>
//            <th>Контакты</th>
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="restorany-kafe-bary">отправить выбранным</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td style="width:1%;">фото</td>
            <td style="width:20%;"><a href="#" class="thsort" data-sort-field="title">Название ресторана'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td>Кухня</td>
            <td><a href="#" class="thsort" data-sort-field="resttype_id">Тип&nbsp;заведения'.(isset($sort_resttype_id)?$sort_resttype_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="middlecheck_id">Средний&nbsp;чек'.(isset($sort_middlecheck_id)?$sort_middlecheck_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="restorany-kafe-bary">отправить запрос<br>выбранным</button></td>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12" style="background: #f5f1f1;">
<p>
Большинство недорогих столовых, семейных кафе и ресторанов расположены в поселках Красная Поляна и Эсто-Садок. Гастро-бары, пабы и клубы больше представлены на территории горных курортов Роза Хутор и Горки Город.</p> 

<p>Организуйте себе настоящее гастро-путешествие по Красной Поляне! Мы регулярно обновляем информацию о заведениях, чтобы облегчить вам составление маршрута.<br> Сейчас на сайте представлена актуальная информация, которую тем не менее, рекомендуем проверить перед визитом в заведение. Все очень быстро меняется. Вы можете позвонить и уточнить режим работы самостоятельно.</p> 
                    </div>
                </div>';
    $_telo .= '<script> var blockName = "restaurants"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}