<?php

function getGiftBalls($user_id)
{
    $gbballs = 0;
    
    $gb = new Table('giftballs');
    $gb->setOptions(Array('where' => " `user_id`='$user_id' "));
    $gbrows = $gb->getAllRows();
    if ($gbrows) foreach($gbrows as $gbk => $gbv) $gbballs += ($gbv['balls'] - $gbv['useballs']);
    return $gbballs;
}

function getActiveBalls($user_id)
{
    $tme = time();
    // расчёт доступных баллов
    $conts = new Table("contracts");
    $conts->setOptions(Array('where' => "(`user_id`='$user_id') 
                                     and (`status`='2')
                                     and (`activeballs`='1')
                                     and (`useballs` < `balls`)
                                     "
                            ));
                            
    $activeSummBalls = 0;
    $contracts = $conts->getAllRows();
    if ($contracts)
    {
        foreach($contracts as $ck => $cv) $activeSummBalls += ($cv['balls']-$cv['useballs']);
        $activeSummBalls += getGiftBalls($user_id);
        $tpl->add('activeSummBalls',$activeSummBalls);
    }
    return $activeSummBalls;
}

function useDownBalls($contracts, $price, $user_id)
{
    echo $price.'<br>';
    $conts = new Table('contracts');
    foreach($contracts as $ck => $cv)
    {
        $balls = ($cv['balls']-$cv['useballs']);
        if ($price >= $balls)
        {
            $useballs = ($balls + $cv['useballs']);
            $price -= $balls;
            $cv['status'] = 4;
            // тут мы гасим все баллы полиса
        }
        else
        {
            $useballs = $price;
            $price = 0;
            // тут мы частично гасим баллы полиса
        }
        $cv['useballs'] = $useballs;
        $conts->updateRow($cv);
        
        if ($price == 0) break;
    }

    if ($price > 0)
    {
        // не все баллы использованы
        $gb = new Table('giftballs');
        $gb->setOptions(Array('where' => " `user_id`='$user_id' ",
                              'order' => " `created` " ));
        $gbrows = $gb->getAllRows();
        foreach($gbrows as $gbk => $gbv)
        {
            $balls = ($gbv['balls'] - $gbv['useballs']);
            if ($price >= $balls)
            {
                $useballs = ($balls + $gbv['useballs']);
                $price -= $balls;
                $gbv['status'] = 1;
                // тут мы гасим все баллы полиса
            }
            else
            {
                $useballs = $price;
                $price = 0;
                // тут мы частично гасим баллы полиса
            }
            $gbv['useballs'] = $useballs;
            $gb->updateRow($gbv);
            if ($price == 0) break;
        }
        
    }
}