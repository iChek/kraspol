<?php

$fb = new Table('feedback'); $frow = $fb->getRowById(1);

$ff = new Table('feedback_fields');
$ff->setOptions(Array('where'=>" (`feedback_id`=1) and (`status`=1) ",'order'=>'`pos`'));
$frms = $ff->getAllRows();
$alldata['text'] = $frow['text'];
$alldata['name'] = $helper->createFolderName($frow['title']);

$forma = '';
foreach($frms as $formkey => $formval)
{
    $typefiled = $formval['typefiled'];
    unset($data);
    $data['title'] = $formval['title'];
    $data['id'] = $helper->createFolderName($formval['title']);
    $data['req'] = (($formval['require']=='1')?'require':'');
    $data['reqstyle'] = (($formval['require']=='1')?' style="color:red;"':'');
    
    if ($typefiled == 4)
    {
        $tmp = '';
        $t = explode("\r",$formval['values']);
        foreach($t as $k => $v) $tmp .= '<option value="'.$v.'">'.$v.'</option>';
        $data['options'] = $tmp;
    }
    
    $item = $tpl->loadHTML('feedback/item_'.$typefiled,$data);
    
    $forma .= $item;
    
}
$alldata['forma'] = $forma;

$_telo .= $tpl->loadHTML('feedback/forma',$alldata);

$modalData = Array('title' => 'Подтверждение', 'content' => $tpl->loadHTML('_capcha',Array()));
$_telo .= $tpl->loadHTML('modal/simple',$modalData);