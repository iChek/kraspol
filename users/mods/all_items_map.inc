<?php

$tpl->indexPage('kps');

$structs = new Table('structs');
$structs->setOptions(Array('where'=>" (`folder`='maps')"));
$row = $structs->getRow();
//for($i = 0; $i < 2000; $i++) { unset($ses['smo_'.$i]); }
$tpl->add('page_title', $row['seo_title']);
$tpl->add('_title', $row['seo_title']);
$tpl->add('_description', $row['seo_description']);


    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Общая карта',
        '_title' => 'Общая карта | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Общая карта, зимние карты, летние карты ',
        '_description' => 'Карта объектов в Красной Поляне, зимние карты - канатные дороги, карты лыжных трасс, обозначения основной инфраструктуры'
    );


$_telo .= '<h3 style="text-align:center; font-weight: bold; padding-bottom: 30px;">ОБЩАЯ КАРТА</h3>';

if (file_exists('cache/coords.data')) {

    $coords = json_decode(file_get_contents('cache/coords.data'));

} else {

    $l = 0;
    unset($coords);
    $o = new Table('objects');
    ////$o->setOptions(Array('where' => " `id` in (298,775,232,24,79,77,78,80,89) "));
    $items = $o->getAllRows();
    if ($items) {

        foreach ($items as $item) {

            $object_id = $item['id'];
            $c = new Table('objects_contacts');
            $c->setOptions(Array('where' => " (`object_id` = '$object_id') "));
            $contacts = $c->getAllRows();

            if ($contacts) {
                foreach ($contacts as $contact) {
                    $titl = str_replace('"','&quot;',$item['title']);
                    $addr = str_replace('"','&quot;',$contact['address']);
                    $contentString = '<div><h3>' . $titl . '</h3><br>Адрес: ' . $addr . '<br>Телефон: ' . $contact['phone'] . '<br>E-mail: ' . $contact['email'] . '</div>';
                    $lat = $contact['latitude'];
                    $lng = $contact['longitude'];
                    if ($lat != 0 && $lng != 0) {
                        $contentString = str_replace("\n",' ',$contentString);
                        $contentString = str_replace("\r",' ',$contentString);
                        $coords[$l] = '["' . $contentString . '", ' . $lat . ', ' . $lng . ']';
                        $l++;
                    }
                }
            }
        }
    }

    if (isset($coords)) {
        file_put_contents('cache/coords.data', json_encode($coords));
    }
}

//echo $helper->aprn($coords);

if (isset($coords)) {

    $_telo .= '
    <style>
        #map_container {
            position: relative;
            padding-top: 50%;
        }
        #map {
            height: 600px;
            width: 100%;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
        }
        .container.row:first-child {
            display: none !important;
        }
        #slider {
            height: 395px;
        }
        h2.page_title {
        margin-top: 60px;
        font-size: 50px;
        }
        @media (max-width: 768px) {
        #slider {
            height: 195px !important;
        }
        }
    </style>
    <div id="map_container"><div id="map"></div></div>
    <script>
        var loc = [' . join(',', $coords) . '];
        function initMap() {
            var map = new google.maps.Map(document.getElementById("map"), {
                zoom: 13,
                center: new google.maps.LatLng(43.6808951, 40.2387271)
            });
            
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            
            for (i = 0; i < loc.length; i++) {  
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(loc[i][1], loc[i][2]),
                    map: map
                });
            
                google.maps.event.addListener(marker, "click", (function(marker, i) {
                    return function() {
                        infowindow.setContent(loc[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                
                google.maps.event.addDomListener(window, "resize", function() {
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                });
            }
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDViRzlYb0tx2sK3yDFq39kfMmtxGTjrhk&callback=initMap"></script>

    ';
}


$texta = Array( 1 => '', 2 => 'ЗИМНИЕ КАРТЫ');

$tt = new Table('tracktime');

for($i = 1; $i < 3; $i++) {

    $tts = $tt->getRowByID($i);
    $_telo .= '
    <div class="row" style="padding: 0 20px;">
        <div class="col-sm-8 col-sm-offset-2" style="padding: 40px 0; font-size: 18px; ">
        <strong>'.$texta[$i].'</strong> ' . $tts['text'] . '
        </div>
    ';

    $tr = new Table('tracks');
    $tr->setOptions(Array('where' => " `yaertime`=$i "));
    $trs = $tr->getAllRows();
    if ($trs) {
        $_telo .= '<ul style="list-style: none; padding: 0; margin: 0;">';
        foreach ($trs as $key => $row) {

            $_telo .= '<li class="col-lg-4 col-md-4 col-sm-12">';
            $_telo .= '    <a href="/userfiles/moduls/tracks/pdf/' . $row['pdf'] . '" target="_blank">';
            $_telo .= '    <div style="padding: 0px; background-color: white; margin-bottom: 20px;">';
            $_telo .= '        <img src="/userfiles/moduls/tracks/image/' . $row['image'] . '" style="width:100%;" >';
            $_telo .= '        <div style="font-size: 16px; font-style:italic; border-radius: 10px; border: 1px solid white; padding: 10px 15px; color: white; position: relative; margin-top: -55px; margin-left: 15px; float: left;">скачать pdf</div>';
            $_telo .= '        <div style="font-size: 18px; font-weight: bold; color: #4a4a4a; padding: 30px 20px 20px;">' . $row['title'] . '</div>';
            $_telo .= '        <div style="font-size: 16px; color: #4a4a4a; padding: 0px 20px 30px;">' . $row['descr'] . '</div>';
            $_telo .= '    </div>';
            $_telo .= '    </a>';
            $_telo .= '</li>';
        }
        $_telo .= '</ul>';
    }
    $_telo .= '</div>';
}
