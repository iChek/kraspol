<?php

include '_sprav.inc';

$price['none'] = (object)Array('title' => '---');

$territory = getSpravArray('sprav_territory');

$post = $_POST;

$ids = $post['ids'];
$select_service = $post['select-service'];
$table = $kpArray[$select_service];
$rubr = $post[$table];

foreach($kpArray as $k => $val) {
    unset($post[$val]);
}

$msg  = '';
$msg .= 'Тип заказа услуги: '.$razdels[$select_service][0]."<br>\n";
$msg .= 'Территория: '.$territory[$post['territory']]->title."<br>\n";
$msg .= 'ФИО: '.$post['fio']."<br>\n";
$msg .= 'Email: '.$post['email']."<br>\n";
$msg .= 'Телефон: '.$post['phone']."<br>\n";
$msg .= 'Способ связи №1: '.$post['checkemail']."<br>\n";
$msg .= 'Способ связи №2: '.$post['checkphone']."<br>\n";
$msg .= 'Обработка перс. данных: '.$post['agree']."<br>\n";
$msg .= '<p>Комментарий:<br>'.$post['comment'].'</p>'."\n\n";
$msg .= "<p>===========================================================</p>\n\n";

unset($_re);
$_re['pubcategory_id'] = $razdels[$select_service][2];
$_re['territory_id'] = $post['territory'];
$_re['fio'] = $post['fio'];
$_re['email'] = $post['email'];
$_re['phone'] = $post['phone'];
$_re['ssemail'] = $post['checkemail'];
$_re['ssphone'] = $post['checkphone'];
$_re['processpd'] = $post['agree'];
$_re['comment'] = $post['comment'];

//$msg  = '';

if ($table == 'hotels') {

    $hoteltype = getSpravArray('sprav_hoteltype');

    $msg .= 'Тип отеля: '.$hoteltype[$rubr['hoteltype']]->title."<br>\n";
    $msg .= 'Цена от: '.$price[$rubr['price_from']]->title."<br>\n";
    $msg .= 'Цена до: '.$price[$rubr['price_until']]->title."<br>\n";
    $msg .= 'Дата заезда: '.$rubr['dtstart']."<br>\n";
    $msg .= 'Дата выезда: '.$rubr['dtfinish']."<br>\n";
    $msg .= 'Гостей: '.$rubr['guests']."<br>\n";
    $msg .= 'Комнат: '.$rubr['rooms']."<br>\n";

} elseif ($table == 'restaurants') {

    $resttype = getSpravArray('sprav_resttype');

    $msg .= 'Тип ресторана: '.$resttype[$rubr['resttype']]->title."<br>\n";
    $msg .= 'Цена от: '.$price[$rubr['price_from']]->title."<br>\n";
    $msg .= 'Цена до: '.$price[$rubr['price_until']]->title."<br>\n";
    $msg .= $rubr['delivery']."<br>\n";
    $msg .= 'Гостей: '.$rubr['guests']."<br>\n";
    $msg .= 'Дата: '.$rubr['date']."<br>\n";
    $msg .= 'Время: '.$rubr['time']."<br>\n";

} elseif ($table == 'transports') {

    $transporttype = getSpravArray('sprav_transport_type');

    $msg .= 'Тип транспорта: '.$transporttype[$rubr['transporttype']]->title."<br>\n";
    $msg .= $rubr['children']."<br>\n";
    $msg .= 'Гостей: '.$rubr['guests']."<br>\n";
    $msg .= 'Дата: '.$rubr['date']."<br>\n";
    $msg .= 'Время: '.$rubr['time']."<br>\n";

} elseif ($table == 'activitysummer' || $table == 'activitywinter') {

    $msg .= 'Гостей: '.$rubr['guests']."<br>\n";
    $msg .= 'Дата начала: '.$rubr['date1']."<br>\n";
    $msg .= 'Время начала: '.$rubr['time1']."<br>\n";
    $msg .= 'Дата окончания: '.$rubr['date2']."<br>\n";
    $msg .= 'Время окончания: '.$rubr['time2']."<br>\n";
    $msg .= 'Возраст детей: '.$rubr['ages']."<br>\n";

    unset($rubr['guests']);
    unset($rubr['date1']);
    unset($rubr['time1']);
    unset($rubr['date2']);
    unset($rubr['time2']);
    unset($rubr['ages']);

    foreach($rubr as $val) {
        $msg .= $val."<br>\n";
    }

} elseif ($table == 'activitychildren') {

    //$activity = getSpravArray('sprav_children_activity');
    //$msg .= 'Вид отдыха: '.$activity[$rubr['activity']]->title."<br>\n";
    $msg .= 'Участников: '.$rubr['guests']."<br>\n";
    $msg .= 'Дата начала: '.$rubr['date1']."<br>\n";
    $msg .= 'Время начала: '.$rubr['time1']."<br>\n";
    $msg .= 'Дата окончания: '.$rubr['date2']."<br>\n";
    $msg .= 'Время окончания: '.$rubr['time2']."<br>\n";
    $msg .= 'Возраст детей: '.$rubr['ages']."<br>\n";

    unset($rubr['guests']);
    unset($rubr['date1']);
    unset($rubr['time1']);
    unset($rubr['date2']);
    unset($rubr['time2']);
    unset($rubr['ages']);

    foreach($rubr as $val) {
        $msg .= $val."<br>\n";
    }

} elseif ($table == 'beauties') {

    $type = getSpravArray('sprav_beautytype');
    $msg .= 'Тип: '.$type[$rubr['type']]->title."<br>\n";
    $msg .= 'Участников: '.$rubr['guests']."<br>\n";
    $msg .= 'Дата начала: '.$rubr['date1']."<br>\n";
    $msg .= 'Время начала: '.$rubr['time1']."<br>\n";
    $msg .= 'Дата окончания: '.$rubr['date2']."<br>\n";
    $msg .= 'Время окончания: '.$rubr['time2']."<br>\n";
    $msg .= $rubr['children']."<br>\n";
    $msg .= 'Возраст детей: '.$rubr['ages']."<br>\n";

} elseif ($table == 'services') {

    $type = getSpravArray('sprav_servicetype');
    $msg .= 'Тип: '.$type[$rubr['type']]->title."<br>\n";
    $msg .= 'Дата начала: '.$rubr['date1']."<br>\n";
    $msg .= 'Время начала: '.$rubr['time1']."<br>\n";
    $msg .= 'Дата окончания: '.$rubr['date2']."<br>\n";
    $msg .= 'Время окончания: '.$rubr['time2']."<br>\n";

}

function send_pochta($mail, $msg) {

    global $_re;
    $ff = new Table('ff');
    $_re['msg'] = $msg;
    $ff->saveRow($_re, $_e);

    $eml = new Email();
    $eml->to = $mail;
    $eml->subj = 'Заказ услуги с сайта';
    $eml->body = $msg;
    $eml->send(true);

}

if ($ids == '') {
    send_pochta($_o_servmail, $msg);
} else {

    $objs = new Table('objects');
    $cls = new Table('clients');
    $ids = explode(',', $ids);
    foreach($ids as $id) {

        $ses['smo_'.$id] = $id;

        $obj = $objs->getRowByID($id);
        if ($obj) {

            $client_id = round($obj['client_id']);
            if ($client_id != 0) {
                $cl = $cls->getRowByID($client_id);

                if ($cl['klemail'] != '') {
                    $newmsg = "<p><strong>Попытка отправить сообщение объекту [" . $obj['title'] . " (ID: $id)]. У клиента ПРОПИСАН e-mail. Отправлено сообщение клиенту.</strong></p>" . $msg;
                    send_pochta($_o_servmail, $newmsg);
                    send_pochta($cl['klemail'], $msg);
                } else {
                    $newmsg = "<p><strong>Попытка отправить сообщение объекту [" . $obj['title'] . " (ID: $id)]. У клиента не прописан e-mail контактного лица</strong></p>" . $msg;
                    send_pochta($_o_servmail, $newmsg);
                }
            } else {
                $newmsg = "<p><strong>Попытка отправить сообщение объекту [" . $obj['title'] . " (ID: $id)]. Клиент не назначен</strong></p>".$msg;
                send_pochta($_o_servmail, $newmsg);
            }

        } else {
            $newmsg = "<p><strong>Попытка отправить сообщение объекту с ID = $id. Такого объекта нет</strong></p>".$msg;
            send_pochta($_o_servmail, $newmsg);
        }
    }
}

unset($ses);
echo '{"result":true}';
exit;