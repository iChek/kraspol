<?php

// ПОИСК
if (isset($_POST['submit_search'])) {
    $post = $_POST;
    unset($post['submit_search']);
    //$ses[$blockName.'_search'] = $post;
    //$ords = $post;

    if ($_POST['submit_search'] == 2) {
        unset($ses[$blockName . '_search']);
    }
}

// сортировка
if (isset($_POST['order_submit'])) {
    $field = $_POST['order_field'];
    $dir = '';

    if (isset($ses[$blockName . '_order'])) {
        $ords = $ses[$blockName . '_order'];
        $oldfield = $ords['field'];
        $olddir = $ords['dir'];

        if ($oldfield == $field) {
            $olddir = $olddir == '' ? 'desc' : '';
        } else {
            $oldfield = $field;
            $olddir = '';
        }
        $field = $oldfield;
        $dir = $olddir;
    }

    $ords = Array('field' => $field, 'dir' => $dir);
    $ses[$blockName . '_order'] = $ords;
}

if (isset($ses[$blockName . '_order'])) {
    $ords = $ses[$blockName . '_order'];
} else {
    $ords = Array('field' => 'title', 'dir' => '');
    $ses[$blockName . '_order'] = $ords;
}


$active_table = '';
$active_list = '';
$active_map = '';

$active_table = 'sactive';

//if ($blockName == 'hotels' || $blockName == 'restaurants') {
//} else {
//    $_telo .= '
//    <style>
//        .objtab2 { display: none; }
//        table#objTable2 { display: none; }
//    </style>
//    ';
//}


$_telo .= '<div id="scontrol">';
$_telo .= '<a href="#" class="objtab ' . $active_table . '"><i class="fa fa-table"></i> Таблица</a>';
//$_telo .= '<a href="#" class="objtab2 '.$active_list.'"><i class="fa fa-bars"></i> Список</a>';
$_telo .= '<a href="#" class="objtab3 ' . $active_map . '" style="display: none;"><i class="fa fa-map-marker"></i> На карте</a>';
$_telo .= '</div>';

//$_telo .= '<div style="border: 1px solid silver; border-radius: 5px; box-shadow: 2px 2px 2px rgba(100,100,100,.5);">';
$_telo .= '<div class="row">';
$_telo .= '<div id="objTable3" style="display:none;"></div>';
//$_telo .= '
//<table class="table" id="objTable2">
//    <tr><td colspan="20">
//        <table class="table" style="width: 1%;">
//            <tr style="background-color: #f0f7e9;">
//                <th>Сортировка: </th>
//                {%head_list%}
//            </tr>
//        </table>
//    </td></tr>
//</table>
//';
$_telo .= '<div></div><section id="no-more-tables"><table class="table" id="objTable">';

$checkboxes = '';
foreach ($checkboxes_array as $k => $val) {
    $key = $val['name'];
    $title = $val['title'];
    $col = $val['col'];
    $checkboxes .= <<<check
        <div class="checkbox-item">
            <input type="hidden" name="isCheck[{$key}]" value="0">
            <input type="checkbox" name="isCheck[{$key}]" id="{$key}" class="s-checkbox" value="1" {%check_{$key}%}>
            <span class="s-label">
                <label for="{$key}">{$title} 
                <span class="s-off"><i class="fa fa-square-o"></i></span>
                <span class="s-on"><i class="fa fa-check-square"></i></span>
                </label>
            </span>
        </div>
check;
    $checkboxes_offset = '';
};


if (!empty($activity_array)) {
    foreach ($activity_array as $k => $val) {
        $key = $val['name'];
        $title = $val['title'];
        $col = $val['col'];
        $checkboxes .= <<<check
        <div class="checkbox-item activity">
            <input type="hidden" name="activity[{$key}]" value="0">
            <input type="checkbox" name="activity[{$key}]" id="activity_{$key}" class="s-checkbox" value="1" {%activity_check_{$key}%}>
            <span class="s-label">
                <label for="activity_{$key}">{$title} 
                <span class="s-off"><i class="fa fa-square-o"></i></span>
                <span class="s-on"><i class="fa fa-check-square"></i></span>
                </label>
            </span>
        </div>
check;
        $checkboxes_offset = '';
    };
}

if (!empty($rental_array)) {
    foreach ($rental_array as $k => $val) {
        $key = $val['name'];
        $title = $val['title'];
        $col = $val['col'];
        $checkboxes .= <<<check
        <div class="checkbox-item rental">
            <input type="hidden" name="rental[{$key}]" value="0">
            <input type="checkbox" name="rental[{$key}]" id="rental_{$key}" class="s-checkbox" value="1" {%rental_check_{$key}%}>
            <span class="s-label">
                <label for="rental_{$key}">{$title} 
                <span class="s-off"><i class="fa fa-square-o"></i></span>
                <span class="s-on"><i class="fa fa-check-square"></i></span>
                </label>
            </span>
        </div>
check;
        $checkboxes_offset = '';
    };
}

$tpl->add('checkboxes', $checkboxes);