<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'services';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Услуги',
        '_title' => 'Услуги | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Услуги',
        '_description' => 'Услуги в Красной Поляне, расположение, фотоуслуги, уборка, ремонт одежды, банки, ремонт, контакты, найти',
        'searchForm' => '[%html/searches/services%]'
    );

    //$checkboxes_class = 'col-sm-1';
    $checkboxes_offset = '';
    $checkboxes_array = Array();

    $nameForm = '';
    $selectedTerritory = 0;
    $selectedTransporttype = 0;
    $selectedServicetype = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];
        if (isset($srch['servicetype'])) $selectedServicetype = $srch['servicetype'];
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;
    $sort_servicetype_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="beautytype_id">Тип'.(isset($sort_beautytype_id)?$sort_beautytype_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);
//
//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="beautytype_id">Тип'.(isset($sort_beautytype_id)?$sort_beautytype_id:'').'</a></th>
//            <th>Контакты</th>
//
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="krasota-i-zdorove">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td>фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="servicetype_id">Тип'.(isset($sort_servicetype_id)?$sort_servicetype_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="krasota-i-zdorove">отправить запрос<br>выбранным</button></td>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12">
                        Расположение банков и банкоматов, автозапчасти, фотоуслуги, ремонт техники или одежды, ремонт ювелирных изделий, услуги охраны - всё это можно легко найти в этом разделе нашего сайта.

                    </div>
                </div>';
    $_telo .= '<script> var blockName = "services"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}