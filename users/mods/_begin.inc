<?php

//if ($_SERVER['REMOTE_ADDR'] != '176.15.185.68') {
//    echo '<div style="color:red;font-size: 22px; font-weight: bold;padding: 30px;">Технические работы!</div>';
//    exit;
//}

$sort_item_pin = '&nbsp;<i class="fa fa-sort"></i>';

include '_func.inc';

$_telo .= '<script> var slider_delay = '.($_o_slider_delay * 1000).';</script>';
$sliderCategory = 1; // по умолчанию
$sliderCategories = Array(
    '/main/' => 2,
    '/kategoriya-spravochnika/gostinicy' => 3,
    '/kategoriya-spravochnika/restorany-kafe-bary' => 4,
    '/kategoriya-spravochnika/transport' => 5,
    '/kategoriya-spravochnika/letniy-otdyh' => 6,
    '/kategoriya-spravochnika/zimniy-otdyh' => 7,
    '/kategoriya-spravochnika/otdyh-s-detmi' => 8,
    '/kategoriya-spravochnika/krasota-i-zdorove' => 9,
    '/kategoriya-spravochnika/uslugi' => 10,
    '/kategoriya-spravochnika/magaziny' => 11,
);

$getCategory = '/'.$router->modul.'/'.$router->getAction();

if (isset($sliderCategories[$getCategory])) {
    $sliderCategory = $sliderCategories[$getCategory];
} else {
    $sliderCategory = 12; // общее
}

$_sliders = '';
$sl = new Table('slider');
$sl->setOptions(Array('where' => " (`category` = '$sliderCategory') and (`visible`=1) "));
$sls = $sl->getAllRows();
if ($sls) {
    unset($t);
    foreach ($sls as $key => $val) {
        $t[] = $val['image'];
    }
    $_sliders = join(',', $t);
} else {
    // По умолчанию
    $sl->setOptions(Array('where' => " `category` = '1' "));
    $sls = $sl->getAllRows();
    if ($sls) {
        unset($t);
        foreach ($sls as $key => $val) {
            $t[] = $val['image'];
        }
        $_sliders = join(',', $t);
    }
}

$menu = Array(
    '/kategoriya-spravochnika/gostinicy' => 'отели',
    '/kategoriya-spravochnika/restorany-kafe-bary' => 'рестораны',
    '/kategoriya-spravochnika/transport' => 'транспорт',
    '/kategoriya-spravochnika/letniy-otdyh' => 'летний отдых',
    '/kategoriya-spravochnika/zimniy-otdyh' => 'зимний отдых',
    '/kategoriya-spravochnika/otdyh-s-detmi' => 'отдых с детьми',
    '/kategoriya-spravochnika/krasota-i-zdorove' => 'здоровье и красота',
    '/kategoriya-spravochnika/uslugi' => 'услуги',
    '/kategoriya-spravochnika/magaziny' => 'магазины',
    '/all_items_map' => 'карта',
    '/ski-pass' => 'ски-пассы',
    '/kontakty' => 'о проекте',
    '/kategoriya-spravochnika/razvlecheniya' => 'развлечения'
);

$kpArray = Array(
    'gostinicy' => 'hotels',
    'restorany-kafe-bary' => 'restaurants',
    'transport' => 'transports',
    'letniy-otdyh' => 'activitysummer',
    'zimniy-otdyh' => 'activitywinter',
    'otdyh-s-detmi' => 'activitychildren',
    'krasota-i-zdorove' => 'beauties',
    'razvlecheniya' => 'enterts',
    'uslugi' => 'services',
    'razvlecheniya' => 'enterts',
    'magaziny' => 'shops'
);

$razdels = Array(
    'gostinicy' => Array('отели','hotels_form_element',1),
    'restorany-kafe-bary' => Array('рестораны','restaurants_form_element',2),
    'transport' => Array('транспорт','transports_form_element',3),
    'letniy-otdyh' => Array('летний отдых','activitysummer_form_element',5),
    'zimniy-otdyh' => Array('зимний отдых','activitywinter_form_element',6),
    'otdyh-s-detmi' => Array('отдых с детьми','activitychildren_form_element',10),
    'krasota-i-zdorove' => Array('здоровье и красота','beauties_form_element',4),
    'razvlecheniya' => Array('развлечения',''),
    'uslugi' => Array('услуги','services_form_element',8),
    'magaziny' => Array('магазины','',9)
);

$_upmenu = '';
$_dnmenu = '';
$cols = 0 ;
foreach($menu as $k => $v) {
    $_upmenu .= '<li><a href="'.$k.'">'.$v.'</a></li>';

    if ($cols == 0) {
        $_dnmenu .= '<div class="col-xs-6" style="padding: 0;">';
    }

    $_dnmenu .= '<a href="'.$k.'">'.$v.'</a><br>';

    $cols++;
    if ($cols == 7 || $cols = count($menu)) {
        $_dnmenu .= '</div>';
        $cols = 0;
    }
}

$tpl->add('_sliders', $_sliders);
$tpl->add('_upmenu', $_upmenu);
$tpl->add('_dnmenu', $_dnmenu);
$tpl->add('_main_phone', $_o_main_phone);

unset($cl);
$razdelSelect = '<option value="none">Раздел ...</option>';
foreach($razdels as $key => $value) {
    if ($key != 'magaziny' && $key != 'razvlecheniya') {
        $cl[] = ".".$value[1];
        $razdelSelect .= '<option value="' . $key . '" data-class="'. $value[1] .'">' . $value[0] . '</option>';
    }
}
$tpl->add('razdelSelect', $razdelSelect);

$_timeselect = '';
for($ho = 0; $ho < 24; $ho++) {
    for($mi = 0; $mi < 60; $mi+=30) {
        $tim = sprintf('%02d:%02d',$ho, $mi);
        $_timeselect .= '<option value="'.$tim.'">'.$tim.'</option>';
    }
}
$tpl->add('_timeselect', $_timeselect);
$tpl->add('_cl', join(', ',$cl));

include '_sprav.inc';

$soc_small = '';
$soc_big = '';
$soc = new Table('social');
$socs = $soc->getAllRows();
if ($socs) {
    foreach($socs as $s) {
        $soc_small .= '<a href="'.$s['link'].'" style="margin: 1px;" target="_blank">';
        $soc_small .= '<img src="/userfiles/moduls/social/icon_small/'.$s['icon_small'].'" title="'.$s['title'].'">';
        $soc_small .= '</a>';

        $soc_big .= '<a href="'.$s['link'].'" style="margin: 5px;" target="_blank">';
        $soc_big .= '<img src="/userfiles/moduls/social/icon_big/'.$s['icon_big'].'" title="'.$s['title'].'">';
        $soc_big .= '</a>';
    }
    $tpl->add('soc_small', $soc_small);
    $tpl->add('soc_big', $soc_big);
}

$tpl->add('_current_link_redirect', 'http://kpm.1btn.ru'.substr($_SERVER['REQUEST_URI'],1,1000));

$src_solt = ',m,nbvcfgtyuhjiklmnbvcxdfrtyu78i987ytghbnm,.';
$key_code = mt_rand(100000,199999);
$src_code = md5($key_code.$src_solt);
$tpl->add('key_code', $key_code);
$tpl->add('src_code', $src_code);