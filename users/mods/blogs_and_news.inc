<?php

$tpl->add('_title', 'Новости | Единый туристический справочник Красной Поляны');
$tpl->add('page_title', 'Новости');

$tpl->indexPage('kps');
//$_telo .= '<style>#slider { height:170px; }</style>';

$n = new Table('news');

$w  = " (`pubdate`<='".time()."') and ";
$w .= " (`dtstart`<='".time()."') and ";
$w .= " (`dtfinish`>='".time()."') and ";
$w .= " (`partner`=1) ";

for($i = 0; $i < 100000; $i++) $parts[$i] = '';

$allcounts = 0;
unset($ids);
$n->setOptions(Array('where' => $w, 'order' => " `pubdate` desc "));
$news = $n->getAllRows();
if ($news) {
    $allcounts += count($news);
    $x = 0;
    foreach ($news as $key => $row) {

        $image = getCuteImage($n, 'news', $row['id']);
        if (!$image) { $image = '/userfiles/moduls/news/image/' . $row['image']; }

        $item = '';
        $item .= '<li style="width:100%; display: inline-block; vertical-align:top;">';
        $item .= '    <div style="padding: 10px; background-color: white; margin-bottom: 20px;" class="clearfix">';
        $item .= '
        <a href="/event-blog-news/'.$row['folder'].'" style="color: #4a4a4a;">
        <div style="width:150px;height:150px;float:left;border-radius:50%;overflow:hidden;margin-right: 20px;
                background-image:url(' . $image . ');
                background-size: cover;
                background-position: center center">
        </div>
        </a>
        ';
        $item .= '        <div class="text-muted">'.date('d.m.Y',$row['pubdate']).'</div>';
        $item .= '        <div style="font-size: 16px; font-weight: bold; color: #4a4a4a; margin-top: 13px;">';
        $item .= '        <a href="/event-blog-news/'.$row['folder'].'" style="color: #4a4a4a;">' . $row['title'] . '</a>';
        $item .= '        </div>';
        $item .= '        <div style="font-size: 13px; color: #4a4a4a; margin-top: 7px;">' . $row['anonce'] . '</div>';

        $item .= '<br>
        <div class="share42init" style="padding-top: 5px;"
             data-url="http://'.HTTP_HOST.'/event-blog-news/'.$row['folder'].'"
             data-title="'.str_replace('"','&quot;',$row['title']).'"
             data-image="http://'.HTTP_HOST.$image.'">
        </div>';

        $item .= '    </div>';
        $item .= '</li>';

        $parts[$x] = $item;
        $x += 2;

        $ids[$row['id']] = $row['id'];
    }
}

$w = " (`pubdate`<='".time()."') ".(isset($ids)?"and (`id` not in (".join(',',$ids)."))":'')." and (`partner`=0)";
$n->setOptions(Array('where'=> $w, 'order' => " `pubdate` desc "));
$news = $n-> getAllRows();
if ($news) {
    $allcounts += count($news);
    $x = 1;
    foreach ($news as $key => $row) {

        $image = getCuteImage($n, 'news', $row['id']);
        if (!$image) { $image = '/userfiles/moduls/news/image/' . $row['image']; }

        $materialType = $row['partner'] == 1 ? 'Партнёрский материал' : 'Новости';
        $item = '';
        $item .= '<li style="width:100%; display: inline-block; vertical-align:top;">';
        $item .= '    <div style="padding: 10px; background-color: white; margin-bottom: 20px;" class="clearfix">';
        $item .= '
        <a href="/event-blog-news/'.$row['folder'].'" style="color: #4a4a4a;">
        <div style="width:150px;height:150px;float:left;border-radius:50%;overflow:hidden;margin-right: 20px;
                background-image:url(\'' . $image . '\');
                background-size: cover;
                background-position: center center">
        </div>
        </a>
        ';
        $item .= '        <div class="text-muted">'.date('d.m.Y',$row['pubdate']).'</div>';
        $item .= '        <div style="font-size: 16px; font-weight: bold; color: #4a4a4a; margin-top: 13px;">';
        $item .= '        <a href="/event-blog-news/'.$row['folder'].'" style="color: #4a4a4a;">' . $row['title'] . '</a>';
        $item .= '        </div>';
        $item .= '        <div style="font-size: 13px; color: #4a4a4a; margin-top: 7px;">' . $row['anonce'] . '</div>';

        $item .= '<br>
        <div class="share42init" style="padding-top: 5px;"
             data-url="http://'.HTTP_HOST.'/event-blog-news/'.$row['folder'].'"
             data-title="'.str_replace('"','&quot;',$row['title']).'"
             data-image="http://'.HTTP_HOST.$image.'">
        </div>';

        $item .= '    </div>';
        $item .= '</li>';

        $parts[$x] = $item;
        $x += 2;

    }
}

if (isset($parts)) {

    $counts = 0;
    $_telo .= '<ul style="list-style: none; padding: 0; margin: 0;">';
    foreach ($parts as $key => $row) {
        $_telo .= $row;
    }
    $_telo .= '</ul>';

} else {
    $_telo .= '<p>Нет данных</p>';
}

$_telo .= '<script type="text/javascript" src="/users/tpl/js/share42.js"></script>';