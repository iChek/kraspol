<?php
error_reporting(E_ALL);

$h = new Table('hotels');
$cl = new Table('clients');

if (isset($ses['hotels_order'])) {
    $ords = $ses['hotels_order'];
    $field = $ords['field'];
    $dir = $ords['dir'];
    $order = " `h`.`$field` $dir ";
} else {
    $order = ' `h`.`title` ';
}

unset($ahwere);
//if (isset($ses['hotels_search'])) {
if (isset($_POST['submit_search'])) {

    //##echo '<pre>'; print_r($ses['hotels_search']); exit;

    //$srch = $ses['hotels_search'];
    $srch = $_POST;

    //##echo $helper->aprn($srch); exit;

    if (isset($srch['name'])) $nameForm = $srch['name']; else $nameForm = '';
    if (isset($srch['territory']) && $srch['territory']!=0) $selectedTerritory = $srch['territory']; else $selectedTerritory = 'none';
    if (isset($srch['hoteltype']) && $srch['hoteltype']!=0) $selectedHoteltype = $srch['hoteltype']; else $selectedHoteltype = 'none';
    if (isset($srch['dist_cblcar']) && $srch['dist_cblcar']!=0) $selectedDist_cblcar = $srch['dist_cblcar']; else $selectedDist_cblcar = 'none';

    if (isset($srch['price_from2'])) $price_from2 = $srch['price_from2']; else $price_from2 = 0;
    if (isset($srch['price_until2'])) $price_until2 = $srch['price_until2']; else $price_until2 = 0;
    $pwhere = getWherePriceHotels('hotels', $price_from2, $price_until2);
    if ($pwhere != null) {
        $awhere[] = $pwhere;
    }

    if ($nameForm != '') $awhere[] = " (`h`.`title` like '%$nameForm%') ";
    if ($selectedTerritory != 'none') $awhere[] = " (`h`.`territory_id` = '$selectedTerritory') ";
    if ($selectedHoteltype != 'none') $awhere[] = " (`h`.`hoteltype_id` = '$selectedHoteltype') ";
    if ($selectedDist_cblcar != 'none') $awhere[] = " (`h`.`distancecabcar_id` = '$selectedDist_cblcar') ";

    if (isset($srch['isCheck'])) {
        $isCheck = $srch['isCheck'];
        foreach ($isCheck as $key => $val) {
            if ($val == 1) $awhere[] = " (`h`.`$key` = 1) ";
        }
    }

}
$where = ' (1 = 1) ';
if (isset($awhere)) $where = join(' and ', $awhere);

$hs = getObjAllItems($h, 'hotels', $where, $order, $startItems, $numItems);
if ($hs) {

    //echo $helper->aprn($hs); exit;
    //echo count($hs); exit;

    $territory = getSpravArray('sprav_territory');
    $d_cabcar = getSpravArray('sprav_distance_cabcar');
    $hoteltype = getSpravArray('sprav_hoteltype');
    $price = getSpravArray('sprav_price');

    $rownum = 0;  /// ТИРАЖИРОВАТЬ
    unset($coords);
    $cnt = count($hs);
    foreach ($hs as $k => $row) {
        $object_id = $row['object_id'];
        $id = $row['id'];
        $image = getImagePic($h, $object_id);

        list($contacts1, $contacts2, $crds) = getObjectContacts($object_id, $row['title']);
        if ($crds != null) { if (!isset($coords)) { $coords = Array(); } $coords = array_merge($coords, $crds); }
        $workS = ''; $workW = '';
        if ($row['worksummer'] == 1) $workS = '<i class="fa fa-sun-o" data-toggle="popover" data-placement="right" data-content="Работает летом"></i>';
        if ($row['workwinter'] == 1) $workW = '<i class="fa fa-snowflake-o" data-toggle="popover" data-placement="right" data-content="Работает зимой"></i>';

        $client = $cl->getRowByID($row['client_id']);
        $noorder = false;
        if ($row['noorder'] == 1 || !$client || ($client && $client['klemail']=='')) {
            $noorder = true;
        }

        $formname = 'hotels';
        $selname = 'gostinicy';
        include '_zayavka.inc';

        include '__copy_1.inc'; /// ТИРАЖИРОВАТЬ
        $data .= <<<html
                <td class="ti_p10">{$hoteltype[$row['hoteltype_id']]->title}</td>
                <td class="ti_p10">{$territory[$row['territory_id']]->title}</td>
                <td class="ti_p10">{$row['booking']}</td>
                <td class="ti_p10">{$d_cabcar[$row['distancecabcar_id']]->title}</td>
                <td class="ti_p10">{$row['summerprice_id']}</td>
                <td class="ti_p10">{$row['winterprice_id']}</td>
                <td class="ti_p10">
                    <div class="ti_site_cute">
                        <a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a>
                    </div>
                    {$contacts1}
                </td>
                {$zayavka}
            </tr>
html;
        $rownum++; /// ТИРАЖИРОВАТЬ

        $data2 .= '';

//        $data2 .= <<<data2
//            <tr class="level1">
//                <td><div style="margin:10px;width:180px;height:180px;background-image:url({$image});background-size:cover;">&nbsp;</div></td>
//                <td style="width:100%;vertical-align:top;padding-top:10px;">
//
//                    <table style="width:100%;">
//                        <tr>
//                            <td class="title2"><a href="/{$linktypes[$row['linktype']]}/{$row['folder']}">{$row['title']}</a></td>
//                            <td class="add-info">{$hoteltype[$row['hoteltype_id']]->title}</td>
//                            <td class="add-info">{$d_cabcar[$row['distancecabcar_id']]->title}</td>
//                            <td class="add-info rating2">рейтинг: {$row['booking']}</td>
//                        </tr>
//                        <tr>
//                            {$contacts2}
//                            <td class="add-info">{$workS} {$workW}</td>
//                        </tr>
//                        <tr>
//                            <td colspan="2"><div class="site2"><a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a></div></td>
//                        </tr>
//                        <tr>
//                            <td class="short2" colspan="4"><div class="txt">{$row['short']}</div></td>
//                        </tr>
//                    </table>
//
//                </td>
//                <td class="prices2">
//                    <p>
//                    Цена зима, руб./сут.<br>
//                    <span>{$price[$row['winterprice_id']]->title}</span>
//                    </p>
//
//                    <p>
//                    Цена лето, руб./сут.<br>
//                    <span>{$price[$row['summerprice_id']]->title}</span>
//                    </p>
//                    {$zayavka2}
//                </td>
//            </tr>
//data2;

    }

    $data3 = isset($coords) ? getMapScript($coords) : 'no';

} else {
    $cnt = 0;
    $data = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data2 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data3 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
}
