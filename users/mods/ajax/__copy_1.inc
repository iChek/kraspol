<?php
$detail_link = '/'.$linktypes[$row['linktype']].'/'.$row['folder'];
$short = '';
if ($row['short'] != '') {
    $arr = explode(' ', $row['short']);
    $short = '';
    foreach($arr as $k => $word) {
        $short .= $word.' ';
        if (mb_strlen($short) >= 150)
            break;
    }
    $short = $short .' ... <a href="'.$detail_link.'">подробнее</a>';
}
$numrow = $rownum % 2;
$rowclass = $numrow == 0 ? 'even_class' : 'odd_class';

$data .= <<<head
    <tr class="{$rowclass}">
        <td class="ti_cover">
            <a href="{$detail_link}">
                <div class="ti_image_bg" style="background-image:url({$image});">&nbsp;</div>
            </a>
        </td>
        <td style="height: 1%; padding: 0 10px 10px 10px;">
            {$workS} {$workW}
            <div class="ti_title_div"><a href="{$detail_link}" class="ti_title">{$row['title']}</a></div>
            <div class="recom">рекомендуем</div>
            <div class="ti_short">{$short}</div>
        </td>
head;
