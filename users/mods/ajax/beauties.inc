<?php
$h = new Table('beauties');
$cl = new Table('clients');

if (isset($ses['beauties_order'])) {
    $ords = $ses['beauties_order'];
    $field = $ords['field'];
    $dir = $ords['dir'];
    $order = " `h`.`$field` $dir ";
} else {
    $order = ' `h`.`title` ';
}

unset($awhere);
//if (isset($ses['beauties_search'])) {
//    $srch = $ses['beauties_search'];
if (isset($_POST['submit_search'])) {
    $srch = $_POST;

    if (isset($srch['name'])) $nameForm = $srch['name']; else $nameForm = '';
    if (isset($srch['territory'])) $selectedTerritory = $srch['territory']; else $selectedTerritory = 'none';
    if (isset($srch['beautytype'])) $selectedBeautytype = $srch['beautytype']; else $selectedBeautytype = 'none';

    if ($nameForm != '') $awhere[] = " (`h`.`title` like '%$nameForm%') ";
    if ($selectedTerritory != 'none') $awhere[] = " (`h`.`territory_id` = '$selectedTerritory') ";
    if ($selectedBeautytype != 'none') $awhere[] = " (`h`.`beautytype_id` = '$selectedBeautytype') ";
}
$where = ' (1 = 1) ';
if (isset($awhere)) $where = join(' and ', $awhere);

$hs = getObjAllItems($h, 'beauties', $where, $order, $startItems, $numItems);
if ($hs) {

    $beautytype = getSpravArray('sprav_beautytype');
    $territory = getSpravArray('sprav_territory');

    $rownum = 0;  /// ТИРАЖИРОВАТЬ
    unset($coords);
    $cnt = count($hs);
    foreach ($hs as $k => $row) {
        $object_id = $row['object_id'];
        $id = $row['id'];
        $image = getImagePic($h, $object_id);

        list($contacts1, $contacts2, $crds) = getObjectContacts($object_id, $row['title']);
        if ($crds != null) { if (!isset($coords)) { $coords = Array(); } $coords = array_merge($coords, $crds); }
        $workS = ''; $workW = '';
        if ($row['worksummer'] == 1) $workS = '<i class="fa fa-sun-o" data-toggle="popover" data-placement="left" data-content="Работает летом"></i>';
        if ($row['workwinter'] == 1) $workW = '<i class="fa fa-snowflake-o" data-toggle="popover" data-placement="left" data-content="Работает зимой"></i>';

        $client = $cl->getRowByID($row['client_id']);
        $noorder = false;
        if ($row['noorder'] == 1 || !$client || ($client && $client['klemail']=='')) {
            $noorder = true;
        }

        $formname = 'beauties';
        $selname = 'krasota-i-zdorove';
        include '_zayavka.inc';

        include '__copy_1.inc'; /// ТИРАЖИРОВАТЬ
        $data .= <<<html
                <td class="ti_p10">{$territory[$row['territory_id']]->title}</td>
                <td class="ti_p10">{$beautytype[$row['beautytype_id']]->title}</td>
                <td class="ti_p10">
                    <div class="ti_site_cute">
                        <a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a>
                    </div>
                    {$contacts1}
                </td>
                {$zayavka}
            </tr>
html;
        $rownum++; /// ТИРАЖИРОВАТЬ

//        $data2 .= <<<data2
//            <tr class="level1">
//                <td><div style="margin:10px;width:180px;height:180px;background-image:url({$image});background-size:cover;">&nbsp;</div></td>
//                <td style="width:100%;vertical-align:top;padding-top:10px;">
//
//                    <table style="width:100%;">
//                        <tr>
//                            <td class="title2"><a href="/{$linktypes[$row['linktype']]}/{$row['folder']}">{$row['title']}</a></td>
//                            <td></td>
//                            <td class="add-info">{$territory[$row['territory_id']]->title}</td>
//                            <td class="add-info">{$beautytype[$row['beautytype_id']]->title}</td>
//                        </tr>
//                        <tr>
//                            {$contacts2}
//                            <td class="add-info">{$workS} {$workW}</td>
//                        </tr>
//                        <tr>
//                            <td colspan="4"><div class="site2"><a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a></div></td>
//                        </tr>
//                        <tr>
//                            <td class="short2" colspan="4""><div class="txt">{$row['short']}</div></td>
//                        </tr>
//                    </table>
//
//                </td>
//                <td class="prices2">
//                    {$zayavka2}
//                </td>
//            </tr>
//data2;
    }

    $data3 = isset($coords) ? getMapScript($coords) : 'no';

} else {
    $cnt = 0;
    $data = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data2 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data3 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
}
