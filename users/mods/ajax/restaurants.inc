<?php
$h = new Table('restaurants');
$cl = new Table('clients');

if (isset($ses['restaurants_order'])) {
    $ords = $ses['restaurants_order'];
    $field = $ords['field'];
    $dir = $ords['dir'];
    $order = " `h`.`$field` $dir ";
} else {
    $order = ' `h`.`title` ';
}

unset($ahwere);
//if (isset($ses['restaurants_search'])) {
//    $srch = $ses['restaurants_search'];
if (isset($_POST['submit_search'])) {
    $srch = $_POST;

    if (isset($srch['name'])) $nameForm = $srch['name']; else $nameForm = '';
    if (isset($srch['territory'])) $selectedTerritory = $srch['territory']; else $selectedTerritory = 'none';
    if (isset($srch['resttype'])) $selectedResttype = $srch['resttype']; else $selectedResttype = 'none';
    if (isset($srch['kitchen'])) $selectedKitchen = $srch['kitchen']; else $selectedKitchen = 'none';
//    if (isset($srch['middlecheck'])) $selectedMiddlecheck = $srch['middlecheck']; else $selectedMiddlecheck = 'none';

    if (isset($srch['price_from2'])) $price_from2 = $srch['price_from2']; else $price_from2 = 0;
    if (isset($srch['price_until2'])) $price_until2 = $srch['price_until2']; else $price_until2 = 0;
    $pwhere = getWherePriceRestaraunts('hotels', $price_from2, $price_until2);
    if ($pwhere != null) {
        $awhere[] = $pwhere;
    }
    
    if ($nameForm != '') $awhere[] = " (`h`.`title` like '%$nameForm%') ";
    if ($selectedTerritory != 'none') $awhere[] = " (`h`.`territory_id` = '$selectedTerritory') ";
    if ($selectedResttype != 'none') $awhere[] = " (`h`.`resttype_id` = '$selectedResttype') ";
//    if ($selectedMiddlecheck != 'none') $awhere[] = " (`h`.`middlecheck_id` = '$selectedMiddlecheck') ";
    if ($selectedKitchen != 'none') $awhere[] = " ((`h`.`sprav_kitchen` like '$selectedKitchen') or (`h`.`sprav_kitchen` like '$selectedKitchen,%') or (`h`.`sprav_kitchen` like '%,$selectedKitchen') or (`h`.`sprav_kitchen` like '%,$selectedKitchen,%')) ";

    if (isset($srch['isCheck'])) {
        $isCheck = $srch['isCheck'];
        foreach ($isCheck as $key => $val) {
            if ($val == 1) $awhere[] = " (`h`.`$key` = 1) ";
        }
    }
}
$where = ' (1 = 1) ';
if (isset($awhere)) $where = join(' and ', $awhere);

$hs = getObjAllItems($h, 'restaurants', $where, $order, $startItems, $numItems);
if ($hs) {

    $kitchen = getSpravArray('sprav_kitchen');
    $resttype = getSpravArray('sprav_resttype');
//    $middlecheck = getSpravArray('sprav_middle_check');
    $territory = getSpravArray('sprav_territory');

    $rownum = 0; /// ТИРАЖИРОВАТЬ
    unset($coords);
    $cnt = count($hs);
    foreach ($hs as $k => $row) {
        $object_id = $row['object_id'];
        $id = $row['id'];
        $image = getImagePic($h, $object_id);

        list($contacts1, $contacts2, $crds) = getObjectContacts($object_id, $row['title']);
        if ($crds != null) {
            if (!isset($coords)) {
                $coords = Array();
            }
            $coords = array_merge($coords, $crds);
        }
        $workS = '';
        $workW = '';
        if ($row['worksummer'] == 1) $workS = '<i class="fa fa-sun-o" data-toggle="popover" data-placement="right" data-content="Работает летом"></i>';
        if ($row['workwinter'] == 1) $workW = '<i class="fa fa-snowflake-o" data-toggle="popover" data-placement="right" data-content="Работает зимой"></i>';

        $k = explode(',', $row['sprav_kitchen']);
        unset($kitchens);
        foreach ($k as $k_id) {
            if (trim($k_id) != '') $kitchens[] = '&nbsp;' . $kitchen[$k_id]->title . '&nbsp;';
        }
        if (isset($kitchens)) {
            $kitchens2 = '<i class="fa fa-cutlery"></i> ' . join(', ', $kitchens);
            $kitchens = join(',<br>', $kitchens);
        } else {
            $kitchens = '';
            $kitchens2 = '';
        }

        $client = $cl->getRowByID($row['client_id']);
        $noorder = false;
        if ($row['noorder'] == 1 || !$client || ($client && $client['klemail']=='')) {
            $noorder = true;
        }

        $formname = 'restaurants';
        $selname = 'restorany-kafe-bary';
        include '_zayavka.inc';

        include '__copy_1.inc'; /// ТИРАЖИРОВАТЬ
        $data .= <<<html
                <td class="ti_p10">{$territory[$row['territory_id']]->title}</td>
                <td class="ti_p10">{$kitchens}</td>
                <td class="ti_p10">{$resttype[$row['resttype_id']]->title}</td>
                <td class="ti_p10">{$row['middlecheck_id']}</td>
                <td class="ti_p10">
                    <div class="ti_site_cute">
                        <a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a>
                    </div>
                    {$contacts1}
                </td>
                {$zayavka}
            </tr>
html;
        $rownum++; /// ТИРАЖИРОВАТЬ

//        $data2 .= <<<data2
//        <tr class="level1">
//            <td><div style="margin:10px;width:180px;height:180px;background-image:url({$image});background-size:cover;">&nbsp;</div></td>
//            <td style="width:100%;vertical-align:top;padding-top:10px;">
//
//                <table style="width:100%;">
//                    <tr>
//                        <td class="title2"><a href="/{$linktypes[$row['linktype']]}/{$row['folder']}">{$row['title']}</a>&nbsp;</td>
//                        <td class="add-info">{$resttype[$row['resttype_id']]->title}</td>
//                        <td class="add-info">{$territory[$row['territory_id']]->title}</td>
//                        <td class="add-info">{$middlecheck[$row['middlecheck_id']]->title}</td>
//                    </tr>
//                    <tr>
//                        <td>{$kitchens2}</td>
//                    </tr>
//                    <tr>
//                        {$contacts2}
//                        <td class="add-info">{$workS} {$workW}</td>
//                    </tr>
//                    <tr>
//                        <td colspan="4"><div class="site2"><a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a></div></td>
//                    </tr>
//                    <tr>
//                        <td class="short2" colspan="4""><div class="txt">{$row['short']}</div></td>
//                    </tr>
//                </table>
//
//            </td>
//            <td class="prices2">
//                <p>
//                Средний чек, руб<br>
//                <span>{$middlecheck[$row['middlecheck_id']]->title}</span>
//                </p>
//                {$zayavka2}
//            </td>
//        </tr>
//data2;

    }

    $data3 = isset($coords) ? getMapScript($coords) : 'no';

} else {
    $cnt = 0;
    $data = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data2 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
    $data3 = '<tr><td colspan="10">Ничего не найдено</td></tr>';
}
