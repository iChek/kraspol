<?php
include '_sprav.inc';
$cl = new Table('clients');
$objc = new Table('objects_contacts');
$linktypes = Array( "резерв" => 0, "premium-1" => 1, "premium-2" => 2, "premium-3" => 3, "premium-4" => 4, "besplatno" => 5);

$tpl->indexPage('kp');
$_telo .= '
<style>
    #slider { height:170px; }
</style>
';

$linktype = 0;
if (isset($linktypes[$router->getAction(0)])) {
    $linktype = $linktypes[$router->getAction(0)];
}
$folder = $router->getAction();

$good = true;

$o = new Table('objects');
$o->setOptions(Array('where' => " (`linktype` = '$linktype') and (`folder` = '$folder') "));
$item = $o->getRow();
if ($item) {

    $object_id = $item['id'];
    $isGallery = $item['nogellery'] == 1 ? false : true;

    #############################################################
    $sql = "select * from `".cfg::$prfx."objects_names` where `object_id` = '$object_id' limit 0,1 ";
    $sth = $o->_q($sql);
    $row = $sth->fetch();
    $tobj = $row['name'];
    //$t = new Table($tobj);

    $sliderCategory = $sliderCategories['/kategoriya-spravochnika/'.$tobj];
    $_sliders = '';
    $sl = new Table('slider');
    $sl->setOptions(Array('where' => " `category` = '$sliderCategory' "));
    $sls = $sl->getAllRows();
    if ($sls) {
        unset($t);
        foreach ($sls as $key => $val) {
            $t[] = $val['image'];
        }
        $_sliders = join(',', $t);
    } else {
        // По умолчанию
        $sl->setOptions(Array('where' => " `category` = '1' "));
        $sls = $sl->getAllRows();
        if ($sls) {
            unset($t);
            foreach ($sls as $key => $val) {
                $t[] = $val['image'];
            }
            $_sliders = join(',', $t);
        }
    }
    $tpl->add('_sliders', $_sliders);
    #############################################################

    $contacts = '<div style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis">
                <a href="#" data-href="'.$item['site'].'" class="url-href">'.$item['site'].'</a></div><br>';

    $objc->setOptions(Array('where' => " `object_id`='$object_id' "));
    $objcs = $objc->getAllRows();
    if ($objcs) {

        foreach($objcs as $oc){
            $contacts .= '<div style="margin-bottom: 7px;">';
            if (trim($oc['address']) != '') $contacts .= '<i class="fa fa-globe"></i> '.$oc['address'].'<br>';
            if (trim($oc['phone']) != '') $contacts .= '<i class="fa fa-phone"></i> '.$oc['phone'].'<br>';
            if (trim($oc['email']) != '') $contacts .= '<i class="fa fa-at"></i> '.$oc['email'].'<br>';
            $contacts .= '</div>';
        }
    }

    $meta = Array(
        'page_title' => $item['title'],
        '_title' => $item['title'] . ' | Единый туристический справочник Красной Поляны',
        '_keywords' => $item['title'],
        '_description' => $item['short']
    );

    $sliderShow = false;

    $_telo .= '<div style="background-color:white;padding: 1px 15px 25px 15px; margin-bottom: 20px;">';

    $sql = "SELECT image FROM `krasp_objects_gallery` WHERE (`parent_id`='$object_id') and (`gallery`=0)";
    $sth = $o->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $imgRows = $sth->fetchAll();
    if ($imgRows) {
        $_telo .= '<div style="background-image: url(/galleries/objects/' . $object_id . '/' . $imgRows[0]['image'] . ');" id="detail-info">';

        $_telo .= '<h1 class="detail-title">' . $item['title'] . '</h1>';
        $sliderShow = true;

//        $_telo .= '
//            <p class="detail-rating">
//                <i class="fa fa-heart"></i> <span>0</span>
//                <i class="fa fa-commenting"></i> <span>0</span>
//            </p>
//            ';

        $_telo .= '<a href="#" class="slide_nav_prev"><img src="/users/tpl/images/slide_nav_prev.png"></a>';
        $_telo .= '<a href="#" class="slide_nav_next"><img src="/users/tpl/images/slide_nav_next.png"></a>';

        $_telo .= '</div>';

        $indx = 0;
        $_telo .= '<div class="clearfix" id="detail-thumbs" style="margin: 0 -3px; display: none;">';
        foreach ($imgRows as $k => $img) {
            $_telo .= '<div style="max-width: 456px;width: 100%;display: inline-block;padding:3px;">';
            $_telo .= '<a href="#" class="detail-show-img-gallery" data-indx="' . $indx . '">';
            $_telo .= '<img src="/galleries/objects/' . $object_id . '/' . $img['image'] . '" style="width:100%">';
            $_telo .= '</a>';
            $_telo .= '</div>';
            $indx++;
        }
        $_telo .= '</div>';
    }

    $sql = "SELECT image FROM `krasp_objects_gallery` WHERE (`parent_id`='$object_id') and (`gallery`=1)";
    $sth = $o->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $imgRows = $sth->fetchAll();
    if ($imgRows) {
        $indx = 0;
        $_telo .= '<div class="clearfix" id="detail-gallery" style="'.($sliderShow ? '':'margin-top: 13px;').'">';
        foreach ($imgRows as $k => $img) {
            $_telo .= '<div style="max-width: 456px;width: 100%;display: inline-block;padding:3px;">';
            $_telo .= '    <a href="/galleries/objects/' . $object_id . '/' . $img['image'] . '" data-typez="image" data-fancyboxz="gallery">';
            $_telo .= '        <img src="/galleries/objects/' . $object_id . '/' . $img['image'] . '" style="width:100%">';
            $_telo .= '    </a>';
            $_telo .= '</div>';
            $indx++;
        }
        $_telo .= '</div>';
    }

    $_telo .= '<script src="/users/tpl/js/detgal.js?ts=10001"></script>';

    $_telo .= '<div class="clearfix" style="margin: 15px 0;">';
    if (trim($item['logo']) != '') {
        $image = getCuteImage($o, 'objects', $item['id']);
        if (!$image) { $image = '/userfiles/moduls/objects/logo/' . $item['logo']; }

        $_telo .= '<img src="'.$image.'" width="200" alt="'.$item['title'].'" style="float: left; margin-right:20px;">';
    }
    if (!$sliderShow) $_telo .= '<h2>' . $item['title'] . '</h2><hr>';

//    $client = $cl->getRowByID($item['client_id']);
//    $noorder = false;
//    if ($item['noorder'] == 1 || !$client || ($client && $client['klemail']=='') || isset($ses['smo_'.$object_id])) {
//        $noorder = true;
//    }
//
//    if (!$noorder) {
//        $_telo .= '
//        <a href="#" class="sel_obj_form" data-formname="' . $formname . '" data-selname="' . $selname . '" data-id="' . $object_id . '">
//            <img src="/users/tpl/images/basket.png">
//        </a><p>&nbsp;</p>
//        ';
//    }
//
//    if (isset($ses['smo_'.$object_id])) {
//        $_telo .= 'Заявка отправлена<p>&nbsp;</p>';
//    }

    $_telo .= $contacts;
    $_telo .= '</div>';


    /* СПРАВОЧНИКИ */
    $territory = getSpravArray('sprav_territory');
    $d_cabcar = getSpravArray('sprav_distance_cabcar');
    $hoteltype = getSpravArray('sprav_hoteltype');
    $price = getSpravArray('sprav_price');
    $kitchen = getSpravArray('sprav_kitchen');
    $resttype = getSpravArray('sprav_resttype');
    $middlecheck = getSpravArray('sprav_middle_check');
    $transporttype = getSpravArray('sprav_transport_type');
    $beautytype = getSpravArray('sprav_beautytype');
    $servicetype = getSpravArray('sprav_servicetype');
    $shoptype = getSpravArray('sprav_shoptype');
    /* СПРАВОЧНИКИ */


    //*********************************************************************************************************
    $check_array = Array(
        'Отель' => Array('hotels', $ahotels),
        'Ресторан' => Array('restaurants', $arestaurants),
        'Транспорт' => Array('transports', $atransports),
        'Летний отдых' => Array('activitysummer',$activity_summer),
        'Зимний отдых' => Array('activitywinter',$activity_winter),
        'Отдых с детьми' => Array('activitychildren',$achildren),
        'beauties' => Array('beauties',Array()),
        'services' => Array('services',Array()),
        'shops' => Array('shops',Array())
    );

    $add_info = '<table class="table table-condensed" style="width:50%;">';
    if (isset($item['territory_id'])) {
        $add_info .= <<<info
            <tr><td><strong>Расположение: </strong></td><td>{$territory[$item['territory_id']]->title}</td></tr>
info;
    }

    foreach($check_array as $name => $value) {
        $hh = new Table($value[0]);
        $hh->setOptions(Array('where' => " `object_id` = '$object_id' "));
        $hhrow = $hh->getRow();
//        $_telo .= $value[0].'<br>';
        if ($hhrow) {
            
            if(in_array($name, array('Летний отдых', 'Зимний отдых'))){
                $_telo .= '<div class="row"><div class="col-sm-12"><strong>' . $name . '</strong></div>';
                foreach ($value[1] as $key => $chk) {
                    
                    $a_arr = json_decode($hhrow[$key]);
                    if(!empty($a_arr) && $a_arr->opened == 1){
                        
                        $_telo .= '<div class="col-sm-3 col-sm-offset-1">';
                        $_telo .= '<h5><strong>' . (($key == 'activity') ? 'Активности' : 'Прокат') . '</strong></h5>';
                        
                        foreach($chk as $k_a => $v_a){
                            if(isset($a_arr->values) && in_array($k_a, $a_arr->values)){
                            $_telo .= '<div class="activity-line"><i class="fa fa-' . ((isset($a_arr->values) && in_array($k_a, $a_arr->values)) ? 'check-' : '') . 'square-o"></i> ' . $v_a . '</div>';
                            }
                        }
                        $_telo .= '</div>';
                    }
                }
                $_telo .= '</div><br>';
            }elseif (!in_array($name, Array('beauties','services','shops'))) {
                $_telo .= '<div class="row"><div class="col-sm-12"><strong>' . $name . '</strong></div>';
                foreach ($value[1] as $key => $chk) {
                    $_telo .= '<div class="col-sm-3"><i class="fa fa-' . ($hhrow[$key] == 1 ? 'check-' : '') . 'square-o"></i> ' . $chk . '</div>';
                }
                $_telo .= '</div><br>';
            }

            if ($value[0] == 'hotels') {
                $add_info .= <<<info
                    <tr><td><strong>Тип отеля: </strong></td><td>{$hoteltype[$hhrow['hoteltype_id']]->title}</td></tr>
                    <tr><td><strong>Рейтинг Booking.com: </strong></td><td>{$hhrow['booking']}</td></tr>
                    <tr><td><strong>До канатки: </strong></td><td>{$d_cabcar[$hhrow['distancecabcar_id']]->title}</td></tr>
                    <tr><td><strong>Цена, зима, руб/сут: </strong></td><td>{$hhrow['summerprice_id']}</td></tr>
                    <tr><td><strong>Цена, лето, руб/сут: </strong></td><td>{$hhrow['winterprice_id']}</td></tr>
info;
            }
            elseif ($value[0] == 'restaurants') {

                $k = explode(',', $hhrow['sprav_kitchen']);
                unset($kitchens);
                foreach ($k as $k_id) {
                    if (trim($k_id) != '') $kitchens[] = '&nbsp;' . $kitchen[$k_id]->title . '&nbsp;';
                }
                if (isset($kitchens)) {
                    $kitchens2 = '<i class="fa fa-cutlery"></i> ' . join(', ', $kitchens);
                    $kitchens = join(',<br>', $kitchens);
                } else {
                    $kitchens = '';
                    $kitchens2 = '';
                }

                $add_info .= <<<info
                    <tr><td><strong>Тип ресторана: </strong></td><td>{$resttype[$hhrow['resttype_id']]->title}</td></tr>
                    <tr><td><strong>Кухня: </strong></td><td>{$kitchens}</td></tr>
                    <tr><td><strong>Средний чек, руб: </strong></td><td>{$hhrow['middlecheck_id']}</td></tr>
info;
            }
            elseif ($value[0] == 'transports') {

                $add_info .= <<<info
                    <tr><td><strong>Тип: </strong></td><td>{$transporttype[$hhrow['transporttype_id']]->title}</td></tr>
info;
            }
            elseif ($value[0] == 'beauties') {
                $add_info .= <<<info
                    <tr><td><strong>Тип: </strong></td><td>{$beautytype[$hhrow['beautytype_id']]->title}</td></tr>
info;
            }
            elseif ($value[0] == 'services') {
                $add_info .= <<<info
                    <tr><td><strong>Тип: </strong></td><td>{$servicetype[$hhrow['servicetype_id']]->title}</td></tr>
info;
            }
            elseif ($value[0] == 'shops') {
                $add_info .= <<<info
                    <tr><td><strong>Тип: </strong></td><td>{$shoptype[$hhrow['shoptype_id']]->title}</td></tr>
info;
            }
        }
    }
    $add_info .= '</table>';
    //*********************************************************************************************************

    $_telo .= 'Работает: <i class="fa fa-' . ($item['worksummer'] == 1 ? 'check-' : '') . 'square-o"></i> летом &nbsp; ';
    $_telo .= '<i class="fa fa-' . ($item['workwinter'] == 1 ? 'check-' : '') . 'square-o"></i> зимой';

    $_telo .= '<p>&nbsp;</p>' . $add_info;

    // Информация относящаяся к конкретному разделу

    $_telo .= '<hr>' . $item['descr'] . '<hr>';

//    $cl = new Table('clients');
//    $client = $cl->getRowByID($item['client_id']);
//    if ($client) {
//        if (trim($client['klfio']) != '') $_telo .= '<p><strong>' . $client['klfio'] . '</strong></p>';
//        if (trim($client['klphone']) != '') $_telo .= '<p><i class="fa fa-phone"></i> ' . $client['klphone'] . '</p>';
//        if (trim($client['klemail']) != '') $_telo .= '<p><i class="fa fa-envelope"></i> ' . $client['klemail'] . '</p>';
//        if (trim($client['ur_lico']) != '') $_telo .= '<p><i class="fa fa-user"></i> ' . $client['ur_lico'] . '</p>';
//    }

    //$_telo .= $helper->aprn($_SERVER);

    $c = new Table('objects_contacts');
    $c->setOptions(Array('where' => " (`object_id` = '$object_id') "));
    $contacts = $c->getAllRows();

    if ($contacts) {

        $l = 0;
        unset($coords);
        foreach ($contacts as $contact) {
            $titl = str_replace('"','&quot;',$item['title']);
            $addr = str_replace('"','&quot;',$contact['address']);
            $contentString = '<div><h3>' . $titl . '</h3><br>Адрес: ' . $addr . '<br>Телефон: ' . $contact['phone'] . '<br>E-mail: ' . $contact['email'] . '</div>';
            $lat = $contact['latitude'];
            $lng = $contact['longitude'];
            if ($lat != 0 && $lng != 0) {
                $coords[$l] = '["'.$contentString.'", '.$lat.', '.$lng.']';
            }

            $l++;
        }

        if (isset($coords)) {

            $_telo .= '
                <style>
                    #map {
                        height: 400px;
                        width: 100%;
                    }
                </style>
                <div id="map"></div>
                <script>
                    var loc = ['.join(',',$coords).'];
                    function initMap() {
                        var map = new google.maps.Map(document.getElementById("map"), {
                            zoom: 17,
                            center: new google.maps.LatLng(loc[0][1], loc[0][2])
                        });
                        
                        var infowindow = new google.maps.InfoWindow();
                        var marker, i;
                        
                        for (i = 0; i < loc.length; i++) {  
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(loc[i][1], loc[i][2]),
                                map: map
                            });
                        
                            google.maps.event.addListener(marker, "click", (function(marker, i) {
                                return function() {
                                    infowindow.setContent(loc[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                    }
                </script>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDViRzlYb0tx2sK3yDFq39kfMmtxGTjrhk&callback=initMap"></script>
                ';
        }
        /**/
    }

    $_telo .= '</div>';

} else {
    $good = false;
}

if (!$good) {

    $meta = Array(
        'page_title' => '404',
        '_title' => '404 | Единый туристический справочник Красной Поляны',
        '_keywords' => '404',
        '_description' => ''
    );
    $_telo .= '404';
}

$tpl->addArray($meta);