<?php

$tpl->add('resttypeSelect2', getResttypeSelect('... Всё ...', true, isset($selectedResttype)?$selectedResttype : 0));
$tpl->add('territorySelect2', getTerritorySelect('Расположение ...', true, isset($selectedTerritory)?$selectedTerritory:0));
$tpl->add('hoteltypeSelect2', getHoteltypeSelect('... Всё ...', true, isset($selectedHoteltype)?$selectedHoteltype:0));
$tpl->add('distCblcarSelect2', getCblcarSelect('... Всё ...', true, isset($selectedDist_cblcar)?$selectedDist_cblcar:0));
$tpl->add('middleCheckSelect2', getMiddleCheckSelect('... Всё ...', true, isset($selectedMiddleCheck)?$selectedMiddleCheck:0));
$tpl->add('kitchenSelect2', getKitchenSelect('... Всё ...', true, isset($selectedKitchen)?$selectedKitchen:0));
$tpl->add('transporttypeSelect2', getTransporttypeSelect('... Всё ...', true, isset($selectedTransporttype)?$selectedTransporttype:0));
$tpl->add('beautytypeSelect2', getBeautytypeSelect('... Всё ...', true, isset($selectedBeautytype)?$selectedBeautytype:0));
$tpl->add('servicetypeSelect2', getServicetypeSelect('... Всё ...', true, isset($selectedServicetype)?$selectedServicetype:0));
$tpl->add('shoptypeSelect2', getShoptypeSelect('... Всё ...', true, isset($selectedShoptype)?$selectedShoptype:0));

/***************************/

$tpl->add('territorySelect', getSpravList('territory','sprav_territory', 'Расположение', isset($selectedTerritory)?$selectedTerritory:'none'));
$tpl->add('hoteltypeSelect', getSpravList('hoteltype','sprav_hoteltype', 'Тип отеля', isset($selectedHoteltype)?$selectedHoteltype:'none'));
$tpl->add('distCblcarSelect', getSpravList('dist_cblcar','sprav_distance_cabcar', 'До подъёмника', isset($selectedDist_cblcar)?$selectedDist_cblcar:'none'));

$tpl->add('kitchenSelect', getSpravList('kitchen','sprav_kitchen', 'Кухня', isset($selectedKitchen)?$selectedKitchen:'none'));
$tpl->add('resttypeSelect', getSpravList('resttype','sprav_resttype', 'Тип ресторана', isset($selectedResttype)?$selectedResttype:'none'));
$tpl->add('middleCheckSelect', getSpravList('middlecheck','sprav_middle_check', 'Средний чек', isset($selectedMiddleCheck)?$selectedMiddleCheck:'none'));

$tpl->add('transporttypeSelect', getSpravList('transporttype','sprav_transport_type', 'Тип', isset($selectedTransporttype)?$selectedTransporttype:'none'));

$tpl->add('beautytypeSelect', getSpravList('beautytype','sprav_beautytype', 'Тип', isset($selectedBeautytype)?$selectedBeautytype:'none'));
$tpl->add('servicetypeSelect', getSpravList('servicetype','sprav_servicetype', 'Тип', isset($selectedServicetype)?$selectedServicetype:'none'));
$tpl->add('shoptypeSelect', getSpravList('shoptype','sprav_shoptype', 'Тип', isset($selectedShoptype)?$selectedShoptype:'none'));




$price_from = isset($price_from) ? $price_from : 0;
$price_until = isset($price_until) ? $price_until : 0;
$price = getSpravArray('sprav_price', ' `id` ');
//unset($price[0]);
$priceFromSelect = '<option value="none">... Любая ...</option>';
//foreach ($price as $k => $row) {
//    $priceFromSelect .= '<option value="'.$row->id.'" '.($row->id == $price_from?'selected':'').'>' . $row->title . '</option>';
//}
$priceUntilSelect = '<option value="none">... Любая ...</option>';
//foreach ($price as $k => $row) {
//    $priceUntilSelect .= '<option value="'.$row->id.'" '.($row->id == $price_until?'selected':'').'>' . $row->title . '</option>';
//}
//$tpl->add('priceFromSelect', $priceFromSelect);
//$tpl->add('priceUntilSelect', $priceUntilSelect);

//unset($price[0]);
//unset($price['none']);
////echo $helper->aprn($price); exit;
//$price_from2 = isset($price_from2) ? $price_from2 : 0;
//$price_until2 = isset($price_until2) ? $price_until2 : 0;
//$priceFrom2Select = '<option value="none">... Любая ...</option>';
//$priceUntil2Select = '<option value="none">... Любая ...</option>';
//foreach ($price as $k => $row) {
//    if ($row->insearch == 1) {
//        if (trim($row->from) != '') $priceFrom2Select .= '<option value="' . $row->id . '" ' . ($row->id == $price_from2 ? 'selected' : '') . '>' . $row->from . '</option>';
//        if (trim($row->until) != '') $priceUntil2Select .= '<option value="' . $row->id . '" ' . ($row->id == $price_until2 ? 'selected' : '') . '>' . $row->until . '</option>';
//    }
//}
//$tpl->add('priceFrom2Select', $priceFrom2Select);
//$tpl->add('priceUntil2Select', $priceUntil2Select);

unset($price[0]);
unset($price['none']);
if(!isset($max_slider)){
    $max_slider = 10000;
}
//echo $helper->aprn($price); exit;
$price_from2 = isset($price_from2) ? $price_from2 : '0';
$price_until2 = isset($price_until2) ? $price_until2 : $max_slider;
$priceFrom2Select = '<input type="hidden" name="price_from2" id="price_from2" value="'.$price_from2.'">';
$priceUntil2Select = '<input type="hidden" name="price_until2" id="price_until2" value="'.$price_until2.'">';
//$listFrom2 = '';
//$listUntil2 = '';
//$text_value_from = 'Цена от, руб';
//$text_value_until = 'Цена до, руб';
//foreach ($price as $k => $row) {
//    if ($row->insearch == 1) {
//        if (trim($row->from) != '') {
//            $id = $row->id;
//            $title = $row->from;
//            $name = 'price_from2';
//            $active = '';
//            if ($id == $price_from2) {
//                $text_value_from = $title;
//                $active = 'list-item-select-active';
//            }
//            $listFrom2 .= '<li class="'.$active.'"><a href="#" data-itemid="'.$id.'" data-input="'.$name.'" class="item-click">'.$title.'</a></li>';
//            $priceFromSelect .= '<option value="'.$id.'">' . $title . '</option>';
//
//        }
//        if (trim($row->until) != '') {
//            $id = $row->id;
//            $title = $row->until;
//            $name = 'price_until2';
//            $active = '';
//            if ($id == $price_until2) {
//                $text_value_until = $title;
//                $active = 'list-item-select-active';
//            }
//            $listUntil2 .= '<li class="'.$active.'"><a href="#" data-itemid="'.$id.'" data-input="'.$name.'" class="item-click">'.$title.'</a></li>';
//            $priceUntilSelect .= '<option value="'.$id.'">' . $title . '</option>';
//
//        }
//    }
//}

$priceFrom2Select .= '<div class="text-value col-sm-2 price_from">'.$price_from2.'</div>';
//$priceFrom2Select .= '<div class="text-value"><div class="text-arrow">'.$text_value_from.'</div></div>';
//$priceFrom2Select .= '<div class="ul-value"><ul>'.$listFrom2.'</ul></div>';
//
$priceFrom2Select .= '<div class="col-sm-8"><div id="price_slider" style="margin-top: 15px; background: rgba(0,0,0,0.5);border-radius: 15px;border: 1px solid gray;"></div></div>';
$priceFrom2Select .= '<script>'
        . 'jQuery(document).ready(function($){
            $("#price_slider").slider({
                max: ' . $max_slider . ',
                min: 0,
                step: 100,
                values: [' . (int)$price_from2 . ', ' . (int)$price_until2 . '],
                slide: function(event, ui) {
                    $("#price_from2").val(ui.values[0]);
                    $("#price_until2").val(ui.values[1]);
                    $(".price_from").html(ui.values[0]);
                    $(".price_until").html(ui.values[1]);
                }
            });
        });'
        . '</script>';
$priceUntil2Select .= '<div class="text-value col-sm-2 price_until">'.$price_until2.'</div>';
//$priceUntil2Select .= '<div class="text-value"><div class="text-arrow">'.$text_value_until.'</div></div>';
//$priceUntil2Select .= '<div class="ul-value"><ul>'.$listUntil2.'</ul></div>';

$tpl->add('priceFrom2Select', $priceFrom2Select);
$tpl->add('priceUntil2Select', $priceUntil2Select);

$tpl->add('priceFromSelect', $priceFromSelect);
$tpl->add('priceUntilSelect', $priceUntilSelect);

// переделали чекбоксы активностей
$ttt = new Table('sprav_summer_rental');
$sr = $ttt->getAllRows();
$summer_rental = array_combine(array_column($sr, 'id'), array_column($sr, 'title'));

$ttt = new Table('sprav_summer_activity');
$sa = $ttt->getAllRows();
$summer_activity = array_combine(array_column($sa, 'id'), array_column($sa, 'title'));
$activity_summer = array(
    'rental' => $summer_rental,
    'activity' => $summer_activity
);

$ttt = new Table('sprav_winter_rental');
$wr = $ttt->getAllRows();
$winter_rental = array_combine(array_column($wr, 'id'), array_column($wr, 'title'));

$ttt = new Table('sprav_winter_activity');
$wa = $ttt->getAllRows();
$winter_activity = array_combine(array_column($wa, 'id'), array_column($wa, 'title'));
$activity_winter = array(
    'rental' => $winter_rental,
    'activity' => $winter_activity
);

// переделали чекбоксы активностей

$asummer = Array(
    'ekipir' => 'прокат экипировки и оборудования',
    'gora' => 'горные походы/треккинг',
    'splav' => 'сплавы/рафтинг/каньонинг',
    'moto' => 'мото/квадро/джиппинг',
    'lesopedy' => 'велосипеды',
    'konyaki' => 'конные походы',
    'skalolaz' => 'скалолазание',
    'yoga' => 'йога',
    'ekskurs' => 'экскурсии'
);
$form_order_summer = '';
foreach($asummer as $key => $val) {
    $form_order_summer .= '
    <div class="col-md-6">
        <label style="margin-bottom: 5px;">'.$val.'</label><br>
        <input type="hidden" name="activitysummer['.$key.']" value="'.$val.': нет">
        <input type="checkbox" name="activitysummer['.$key.']" id="'.$key.'" class="s-checkbox" value="'.$val.': да">
        <span class="s-label">
            <span class="s-off"><i class="fa fa-square-o"></i></span>
            <span class="s-on"><i class="fa fa-check-square"></i></span>
            <label for="'.$key.'">Да</label>
        </span>
    </div>
    ';
}
$tpl->add('form_order_summer', $form_order_summer);

$awinter = Array(
    'ekipir' => 'прокат экипировки и оборудования',
    'lyzhi' => 'инструктор лыжи',
    'snowboard' => 'инструктор сноуборд',
    'freeride' => 'гид фрирайд',
    'backcountry' => 'гид бэккантри',
    'healski' => 'хели-ски',
    'skitour' => 'скитур',
    'childschool' => 'детская школа',
    'yoga' => 'йога',
    'ekskurs' => 'экскурсии'
);
$form_order_winter = '';
foreach($awinter as $key => $val) {
    $form_order_winter .= '
    <div class="col-md-6">
        <label style="margin-bottom: 5px;">'.$val.'</label><br>
        <input type="hidden" name="activitywinter['.$key.']" value="'.$val.': нет">
        <input type="checkbox" name="activitywinter['.$key.']" id="'.$key.'" class="s-checkbox" value="'.$val.': да">
        <span class="s-label">
            <span class="s-off"><i class="fa fa-square-o"></i></span>
            <span class="s-on"><i class="fa fa-check-square"></i></span>
            <label for="'.$key.'">Да</label>
        </span>
    </div>
    ';
}
$tpl->add('form_order_winter', $form_order_winter);

$achildren = Array(
    'aktivnyi_otdyh' => 'активный отдых',
    'detskie_kluby' => 'детские клубы'
);
$form_order_children = '';
foreach($achildren as $key => $val) {
    $form_order_children .= '
    <div class="col-md-6">
        <label style="margin-bottom: 5px;">'.$val.'</label><br>
        <input type="hidden" name="activitychildren['.$key.']" value="'.$val.': нет">
        <input type="checkbox" name="activitychildren['.$key.']" id="srch_'.$key.'" class="s-checkbox" value="'.$val.': да">
        <span class="s-label">
            <span class="s-off"><i class="fa fa-square-o"></i></span>
            <span class="s-on"><i class="fa fa-check-square"></i></span>
            <label for="srch_'.$key.'">Да</label>
        </span>
    </div>
    ';
}
$tpl->add('form_order_children', $form_order_children);

$ahotels = Array(
    'breakfast' => 'Завтрак',
    'wifi' => 'Wi-Fi',
    'transfer' => 'Бесплатный трансфер до подъёмника',
    'parking' => 'Парковка',
    'sauna' => 'Баня / сауна'
);

$arestaurants = Array(
    'alltime' => 'Круглосуточно',
    'delivery' => 'Доставка',
    'childroom' => 'Детская комната',
    'terrace' => 'Терраса'
);

$atransports = Array(
    'transfer' => 'Трансфер'
);
