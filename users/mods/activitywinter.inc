<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'activitywinter';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Зимний отдых',
        '_title' => 'Зимний отдых | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Зимний отдых ',
        '_description' => 'Зимний отдых в Красной Поляне, расположение, прокат экипировки и оборудования, инструкторы, лыжи, сноуборд, фрирайд, бэкквнтри, хели-ски, скитур, экскурсии, контакты, найти',
        'searchForm' => '[%html/searches/activitywinter%]'
    );

    //$checkboxes_class = 'col-sm-1';
    $checkboxes_offset = '';
//    $checkboxes_array = Array(
//        Array(
//            'name' => 'ekipir',
//            'title' => 'прокат экипировки и оборудования',
//            'col' => 4,
//        ),
//        Array(
//            'name' => 'lyzhi',
//            'title' => 'инструктор лыжи',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'snowboard',
//            'title' => 'инструктор сноуборд',
//            'col' => 3,
//        ),
//        Array(
//            'name' => 'freeride',
//            'title' => 'гид фрирайд',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'yoga',
//            'title' => 'йога',
//            'col' => 1,
//        ),
//        Array(
//            'name' => 'backcountry',
//            'title' => 'гид бэккантри',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'healski',
//            'title' => 'хели-ски',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'skitour',
//            'title' => 'скитур',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'childschool',
//            'title' => 'детская школа',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'ekskurs',
//            'title' => 'экскурсии',
//            'col' => 2,
//        )
//    );
    
    
    $t = new Table('sprav_winter_activity');
    $summer_activity = $t->getAllRows();
    $activity_array = array();
    foreach($summer_activity as $activity){
        $activity_array[] = array(
            'name' => $activity['id'],
            'title' => $activity['title'],
            'col' => 1
        );
    }
    
    $t = new Table('sprav_winter_rental');
    $summer_rental = $t->getAllRows();
    $rental_array = array();
    foreach($summer_rental as $rental){
        $rental_array[] = array(
            'name' => $rental['id'],
            'title' => $rental['title'],
            'col' => 1
        );
    }
    

    $nameForm = '';
$_telo .= '<div class="row">
<div class="col-md-12" style="background: #f5f1f1;margin-top: -10px;padding: 0 10px;">
                        <h3>Хотите активно отдохнуть зимой на курортах Красной Поляны?</h3> 


<p>Здесь все, что вы ожидали найти на официальном сайте Красной Поляны 2018/19. Главное, чем мы хотели бы поделиться с вами – это контакты горных гидов и инструкторов Красной Поляны.<br> Большинство работает много лет, и однозначно заслуживает доверия. Тем не менее, крайне важно, найти своего инструктора. Особенно, для тех, кто делает первые шаги на лыжах или сноуборде.</p>

<p>Обратите внимание, часть инструкторов обладает сертификатом*, который подтверждает его квалификацию. <br>Это важно, если вы пользуетесь услугами инструктора для обучения. Это может быть критично важно, если вы намерены взять гида для фрирайда или ски-тура.</p>

                    </div>
</div>';
    $selectedTerritory = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
        
        if (isset($srch['rental'])) {
            $rental = $srch['rental'];
            foreach ($rental as $key => $val) {
                $tpl->add('rental_check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
        if (isset($srch['activity'])) {
            $rental = $srch['activity'];
            foreach ($rental as $key => $val) {
                $tpl->add('activity_check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК


    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);

//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th>Контакты</th>
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td>фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить запрос<br>выбранным</button></td>
        </tr>
    ';

    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
<div class="col-md-12" style="background: #f5f1f1;">

<p>Вы найдете здесь координаты всех прокатов горнолыжного снаряжения. К открытию зимнего сезона 2018/19 готовы более 30 прокатов горнолыжной экипировки. Выбор огромен: от знаменитой Базы 560 на Розе Хутор, где можно взять в аренду полный комплект снаряжения для фрирайда и скитура, до недорогих прокатов оборудования в поселках Красная Поляна и Эсте-Садок.</p>

<p>Красная Поляна – это не только горнолыжный отдых. Любители более спокойного отдыха, найдут себе развлечение по душе. Прогулки на беговых лыжах по сосновому лесу, катание на собачей упряжке, экскурсии в горы. Мы постарались отразить все варианты зимнего отдыха в этом разделе, и поделиться контактами тех, кто его организует.</p>
<p style="font-size: 13px;border-top: 1px solid #ccc;padding: 10px 0;color: #000;">
* Пользуясь услугами инструкторов и гидов, не имеющих сертификатов, вы берёте на себя ответственность за свою безопастность.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Уточняйте наличие у вашего инструктора лицензии или сертификата.<br>
* НЛИ - Национальная Лига инструкторов, www.isiarussia.ru<br>
* Ваги - Всероссийская ассоциация горнолыжных курортов, www.arasia.ru<br>
* RMGA - Ассоциация горных гидов России, www.rmga.ru<br>
* ФГССР - Федерация горнолыжного спорта и сноубординга России, www.fgssr.ru<br>
* УФК - Университет Физической Культуры</p>
                    </div>
                </div>';
    $_telo .= '<script> var blockName = "activitywinter"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}