<?php

if (isset($_GET['id'])) {

    unset($arr);

    $id = round($_GET['id']);
    $objs = new Table('objects');
    $obj = $objs->getRowByID($id);

    if ($obj) {

        unset($obj['descr']);
        $obj['object_type'] = 'null';

        if (isset($_GET['type'])) {
            $type = trim($_GET['type']);
            $obj['object_type'] = $type;
            $h = new Table($type);
            $h->setOptions(Array('where' => " `object_id` = '$id' "));
            $temp = $h->getRow();
            if ($temp) {
                $arr[$type] = $temp;
            }
        }
        $arr['object'] = $obj;

        echo json_encode($arr);

    } else {
        echo '{"result" : false, "message" : "Не найден объект по ID: '.$id.'" }';
    }

} else {
    http_response_code(400);
    echo '{"result" : false, "message" : "Не заданы параметры для получения данных объекта" }';
}

exit;