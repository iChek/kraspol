<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
$meta = Array(
    'page_title' => 'Поиск',
    '_title' => 'Поиск | Единый туристический справочник Красной Поляны',
    '_keywords' => 'Поиск',
    '_description' => '',
    'searchForm' => '[%html/searches/main%]'
);


$linktypes = Array(0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");

$post = $_POST;
$get = $_GET;
$serv_name = (isset($post['serv_name'])) ? $post['serv_name'] : $get['serv_name'];
//$serv_category = $post['serv_category'];
if (isset($post['serv_category']))
    $serv_category = $post['serv_category'];
else
    $serv_category = 'none';
if (isset($post['serv_territory']))
    $serv_territory = $post['serv_territory'];
else
    $serv_territory = 'none';

if ($serv_name == '' && $serv_category == 'none' && $serv_territory == 'none') {
    $ses->redirect('/');
} elseif ($serv_category != 'none') {

    $url = '/kategoriya-spravochnika/' . $serv_category;
    $blockName = $kpArray[$serv_category];
    unset($srch);
    if ($serv_territory != 'none') {
        $srch['territory'] = $serv_territory;
    }
    if ($serv_name != '') {
        $srch['name'] = $serv_name;
    }

    if (isset($srch)) {
        $ses[$blockName . '_search'] = $srch;
    }

    echo $url . '<br>';
    echo $blockName . '<bR>';
    if (isset($srch))
        echo $helper->aprn($srch);
    if (isset($ses[$blockName . '_search']))
        echo $helper->aprn($ses[$blockName . '_search']);
    echo $helper->aprn($_POST);
    $ses->redirect($url);
    exit;
} else {

    $tpl->indexPage('kp');
    //$_telo .= 'style>#slider { height:170px; }</style>';
    //$_telo .= '<style>#slider {height: 195px !important;} #search_form {display: none !important;}</style>';
    $_telo .= '<style>.main_search_form { margin-top: 65px; }@media (max-width: 768px) {.main_search_form {margin-top: 0px;}#slider {height:295px;}}</style>';

    unset($srch);
    if ($serv_territory != 'none') {
        $srch['territory'] = $serv_territory;
    }
    if ($serv_name != '') {
        // морфологический разбор
        // Разобрать искомую строку $search на отдельные слова
//        preg_match_all('/[[:alnum:]]{3,}/is', stripslashes($serv_name), $matches);
        $words = explode(' ', $serv_name);

//        $true_words = Array();
//        if (count($words) > 1) {
//            foreach ($words as $word) {
//                $word = mb_substr($word, 0, (mb_strlen($word) - 1));
//                // Обрабатывать только слова длиннее 3 символов
//                if (mb_strlen($word) > 3) {
//                    // От слов длиннее 7 символов отрезать 2 последних буквы
//                    if (mb_strlen($word) > 7) {
//                        $word = mb_substr($word, 0, (mb_strlen($word) - 2));
//                    }
//                    // От слов длиннее 5 символов отрезать последнюю букву
//                    elseif (mb_strlen($word) > 5) {
//                        $word = mb_substr($word, 0, (mb_strlen($word) - 1));
//                    }
//                    $true_words[] = addcslashes(addslashes($word), '%_');
//                }
//            }
//        }
//// Список уникальных поисковых слов
//        $true_words = array_unique($true_words);


//        $serv_name = mb_substr($serv_name, 0, (mb_strlen($serv_name) - 1));
        $srch['name'] = $serv_name;
    }

    $tpl->add('serv_name', $serv_name);

    if (isset($srch)) {
        foreach ($kpArray as $k => $blockName) {
            $ses[$blockName . '_search'] = $srch;
        }
    }

    $awhere = array();

    if(count($words) < 2){
        if(mb_strlen($serv_name) > 7){
            $serv_name = mb_substr($serv_name, 0, (mb_strlen($serv_name) - 2));
        }elseif(mb_strlen($serv_name) > 5){
            $serv_name = mb_substr($serv_name, 0, (mb_strlen($serv_name) - 1));
        }
    }
    
    if (isset($srch['name']))
        $nameForm = $serv_name;
    else
        $nameForm = '';
    if (isset($srch['territory']))
        $selectedTerritory = $srch['territory'];
    else
        $selectedTerritory = 'none';
    
    if ($nameForm != '') {
        $query = '';
        // Условие выборки - вхождение фразы или отдельных слов в заголовок или текст
        $query .= " (";
        $query .= " h.`title` LIKE '%" . $nameForm . "%' OR h.`descr` LIKE '%" . $nameForm . "%' OR h.`short` LIKE '%" . $nameForm . "%'";
// Условия для каждого из слов
//        if (count($true_words)) {
//            foreach ($true_words as $word) {
//                $query .= " OR h.`title` LIKE '%" . $word . "%'";
//                $query .= " OR h.`descr` LIKE '%" . $word . "%'";
//            }
//        }
        $query .= ")";
        $awhere[] = $query;
    }
//    $awhere[] = " (`h`.`title` like '%$nameForm%') ";
    if ($selectedTerritory != 'none')
        $awhere[] = " (`h`.`territory_id` = '$selectedTerritory') ";

    $where = ' (1 = 1) ';
    if (isset($awhere) && !empty($awhere))
        $where = join(' and ', $awhere);
    $order = ' `h`.`title` ';
    $startItems = 0;
    $numItems = 50;
    $data2 = '';
    $data3 = '';

    $h = new Table('hotels');

/////////////////////////////////////////////////////////////////////////////////////////////////////////
    $hs = getAllItems($h, $where, $order, $startItems, 10000);
    if ($hs) {

        $allResults = count($hs);

        $hs = getAllItems($h, $where, $order, $startItems, $numItems);
        $territory = getSpravArray('sprav_territory');

        unset($coords);
        $cnt = count($hs);
        foreach ($hs as $k => $row) {
            $object_id = $row['id'];

            $image = getImagePic($h, $object_id);

            list($contacts1, $contacts2, $crds) = getObjectContacts($object_id, $row['title']);
            if ($crds != null) {
                if (!isset($coords)) {
                    $coords = Array();
                } $coords = array_merge($coords, $crds);
            }
            $workS = '';
            $workW = '';
            if ($row['worksummer'] == 1)
                $workS = '<i class="fa fa-sun-o" data-toggle="popover" data-placement="right" data-content="Работает летом"></i>';
            if ($row['workwinter'] == 1)
                $workW = '<i class="fa fa-snowflake-o" data-toggle="popover" data-placement="right" data-content="Работает зимой"></i>';

            $categories = '';
            $sql = "select * from `" . cfg::$prfx . "objects_names` where `object_id`='$object_id';";

            $sth = $h->_q($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            unset($cats);
            while ($cat = $sth->fetch()) {
                $cats[] = $cat;
            }
            if (isset($cats)) {
                unset($temp);
                foreach ($cats as $kcat => $vcat) {
                    $temp[] = '<a href="/kategoriya-spravochnika/' . $vcat['name'] . '" target="_blank">' . $vcat['title'] . '</a>';
                }
                $categories = join(', ', $temp);
            }

            $data2 .= <<<data2
            <tr class="level1">
                <td><div class="ti_image_bg" style="/*margin:10px;width:180px;height:180px;*/background-image:url({$image});background-size:cover;">&nbsp;</div></td>
                <td style="width:100%;vertical-align:top;padding-top:10px;">
                
                    <table style="width:100%;">
                        <tr>
                            <td colspan="3">{$categories}</td>
                            <td style="/*width: 1%; text-align: right;*/ white-space: nowrap">{$territory[$row['territory_id']]->title}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="title2"><a href="/{$linktypes[$row['linktype']]}/{$row['folder']}" target="_blank">{$row['title']}</a></td>
                        </tr>
                        <tr>
                            {$contacts2}
                            <td class="add-info">{$workS} {$workW}</td>
                        </tr>
                        <tr>
                            <td colspan="4"><div class="site2"><a href="#" data-href="{$row['site']}" class="url-href">{$row['site']}</a></div></td>
                        </tr>
                        <tr>
                            <td class="short2" colspan="4"><div class="txt">{$row['short']}</div></td>            
                        </tr>
                    </table>

                </td>
                <!--<td class="prices2">
                    <a href="#" class="sel_obj_form" data-formname="hotels" data-selname="gostinicy" data-id="{$object_id}"><img src="/users/tpl/images/basket.png" style="margin: 20px 80px;"></a>
                </td>-->
            </tr>
data2;
        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////

        $data3 = isset($coords) ? getMapScript($coords) : 'no';

        $s = new Table('structs');
        $s->setOptions(Array('where' => " `folder` = 'search_txt' AND `status`='1' "));
        $ss = $s->getRow();
        if ($ss) {
            $_telo .= $ss['text'];
        }

        if ($allResults > $numItems) {
            $_telo .= '<div class="alert">Всего результатов ' . $allResults . '. Показаны первые ' . $numItems . '. Для уточнения поиска перейдите, пожалуйста, в нужный раздел.</div>';
        }

        $_telo .= '<div id="scontrol">';
        $_telo .= '<a href="#" class="objtab2"><i class="fa fa-bars"></i> Список</a>';
        if ($data3 != 'no')
            $_telo .= '<a href="#" class="objtab3"><i class="fa fa-map-marker"></i> На карте</a>';
        $_telo .= '</div>';

        $_telo .= '
        <div style="/*border: 1px solid silver;*/ border-radius: 5px; box-shadow: 2px 2px 2px rgba(100,100,100,.5);">
            <div id="objTable3" style="display:none;">' . $data3 . '</div>
            <section id="no-more-tables"><table class="table" id="objTable2" style="display:block;">' . $data2 . '</table></section>
        </div>
        ';

        $_telo .= '<script> var noGetBlockItems = true; </script>';
        $_telo .= '<script src="/users/tpl/js/objs.js?ts=10018"></script>';
        if ($data3 != 'no')
            $_telo .= '<script> isMap = true; </script>';
        $_telo .= "
        <script>
            $(function(){
                $('[data-toggle=popover]').popover({trigger:'hover'});
                urlHrefClick();
            });
        </script>";
        $_telo .= '
            <p>&nbsp;</p>
            <form action="" method="post" id="order-form">
                <input type="hidden" name="order_submit" value="1">
                <input type="hidden" name="order_field" id="order-field" value="">
            </form>
        ';
    } else {

        $_telo .= '<div id="scontrol">';
        $_telo .= '<a href="#" class="objtab2"><i class="fa fa-bars"></i> Список</a>';
        $_telo .= '</div>';

        $_telo .= '
        <div style="/*border: 1px solid silver;*/ border-radius: 5px; box-shadow: 2px 2px 2px rgba(100,100,100,.5);">
            <section id="no-more-tables"><table class="table" id="objTable2" style="display: block">
                <tr><td colspan="10">Ничего не найдено</td></tr>
            </table></section>
        </div>
        ';
    }
}


$tpl->addArray($meta);
$tpl->add('nameForm', $nameForm);
include '_sprav.inc';
