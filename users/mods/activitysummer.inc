<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'activitysummer';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Летний отдых',
        '_title' => 'Летний отдых | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Летний отдых ',
        '_description' => 'Летний отдых в Красной Поляне, расположение, прокат экипировки и оборудования, горные походы, треккинг, сплавы, рафтинг, каньонинг, велосипеды, мото, квадро, джиппинг, конные походы, скалолазание, йога, экскурсии, контакты, найти ',
        'searchForm' => '[%html/searches/activitysummer%]'
    );

    //$checkboxes_class = 'col-sm-1';
    $checkboxes_offset = '';
//    $checkboxes_array = Array(
//        Array(
//            'name' => 'ekipir',
//            'title' => 'прокат экипировки и оборудования',
//            'col' => 4,
//        ),
//        Array(
//            'name' => 'gora',
//            'title' => 'горные походы/треккинг',
//            'col' => 3,
//        ),
//        Array(
//            'name' => 'splav',
//            'title' => 'сплавы/рафтинг/каньонинг',
//            'col' => 3,
//        ),
//        Array(
//            'name' => 'lesopedy',
//            'title' => 'велосипеды',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'moto',
//            'title' => 'мото/квадро/джиппинг',
//            'col' => 3,
//        ),
//        Array(
//            'name' => 'konyaki',
//            'title' => 'конные походы',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'skalolaz',
//            'title' => 'скалолазание',
//            'col' => 2,
//        ),
//        Array(
//            'name' => 'yoga',
//            'title' => 'йога',
//            'col' => 1,
//        ),
//        Array(
//            'name' => 'ekskurs',
//            'title' => 'экскурсии',
//            'col' => 2,
//        ),
//    );
    
    $t = new Table('sprav_summer_activity');
    $summer_activity = $t->getAllRows();
    $activity_array = array();
    foreach($summer_activity as $activity){
        $activity_array[] = array(
            'name' => $activity['id'],
            'title' => $activity['title'],
            'col' => 1
        );
    }
    
    $t = new Table('sprav_summer_rental');
    $summer_rental = $t->getAllRows();
    $rental_array = array();
    foreach($summer_rental as $rental){
        $rental_array[] = array(
            'name' => $rental['id'],
            'title' => $rental['title'],
            'col' => 1
        );
    }

    $nameForm = '';
    $selectedTerritory = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
        if (isset($srch['rental'])) {
            $rental = $srch['rental'];
            foreach ($rental as $key => $val) {
                $tpl->add('rental_check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
        if (isset($srch['activity'])) {
            $rental = $srch['activity'];
            foreach ($rental as $key => $val) {
                $tpl->add('activity_check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);

//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th>Контакты</th>
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td>фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить запрос<br>выбранным</button></td>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12">
                        <h3>Если Вы любите активный отдых летом</h3> 
                        Унас Вы найдете, чем заняться на курортах Красной Поляны - Роза Хутор, Горки Город, ГТЦ "Газпром", Альпика. Катание на велосипедах и квадроциклах, джиппинг, конные прогулки и горные походы, сплавы и рафтинг, скалолазание, йога и экскурсии. Удобный поиск по фильтрам.
                    </div>
                </div>';
    
    $_telo .= '<script> var blockName = "activitysummer"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}