<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'hotels';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Отели',
        '_title' => 'Отели | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Отели',
        '_description' => 'Отели в Красной Поляне, расположение, тип отеля, цена от, доступность до подъёмника, завтрак, wi-fi, парковка, подробнее',
        'searchForm' => '[%html/searches/hotels%]'
    );

    $checkboxes_offset = 'col-md-offset-1';
    $checkboxes_array = Array(
        Array(
            'name' => 'breakfast',
            'title' => 'Завтрак',
            'col' => 1
        ),
        Array(
            'name' => 'wifi',
            'title' => 'Wi-Fi',
            'col' => 1
        ),
        Array(
            'name' => 'transfer',
            'title' => 'Бесплатный трансфер до подъёмника',
            'col' => 3
        ),
        Array(
            'name' => 'parking',
            'title' => 'Парковка',
            'col' => 1
        ),
        Array(
            'name' => 'sauna',
            'title' => 'Есть баня / сауна',
            'col' => 2
        )
    );

    $max_slider = 100000;
    
    $price_from2 = 0;
    $price_until2 = $max_slider;
    $nameForm = '';
    
        $_telo .= '<div class="row">
                    <div class="col-md-12" style="background: #f5f1f1;margin-top: -10px;padding: 0 10px;">
                           <h3>Отели, гостевые дома, апартаменты, хостелы на курортах Красной Поляны -</h3> 
                           <p>Здесь всё, что вы ожидали найти на официальном сайте Красной Поляны 2018/19. Справочная информация обо всех отеля, гостевых домах, апартаментах и  хостелах, расположенных в районах Роза Хутор, Альпика Сервис, Эсто-Садок, Горки Город, ГТЦ "Газпром" и, собственно, деревне Красная Поляна. Для гостей курорта, мы указали прямые контакты всех гостиниц, апартаментов и хостелов, которые будут открыты для прямого бронирования в зимнем сезоне 2018 и 2019 года.</p> 

<p>Бронируйте напрямую у собственников, звоните, общайтесь, получайте скидки. Найдите вариант размещения напрямую, когда, кажется, что все отели курорта заполнены до предела. Найдите самое дешевое размещение, там где, кажется, закончились все доступные варианты.</p> 

<p>Мы постарались сделать удобную навигацию и поиск, чтобы вы могли легко найти свой вариант размещения и связаться с отелем напрямую. </p> 

                        </div>
                    </div>';
    
    $selectedTerritory = 0;
    $selectedHoteltype = 0;
    $selectedDist_cblcar = 0;

    include '_common_block1.inc';

    //if (isset($ses[$blockName.'_search'])) {
    //    $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;

        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];
        if (isset($srch['hoteltype'])) $selectedHoteltype = $srch['hoteltype'];
        if (isset($srch['dist_cblcar'])) $selectedDist_cblcar = $srch['dist_cblcar'];
        if (isset($srch['price_from2'])) $price_from2 = round($srch['price_from2']);
        if (isset($srch['price_until2'])) $price_until2 = round($srch['price_until2']);

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_hoteltype_id = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;
    $sort_booking = $sort_item_pin;
    $sort_distancecabcar_id = $sort_item_pin;
    $sort_summerprice_id = $sort_item_pin;
    $sort_winterprice_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="hoteltype_id">Тип отеля'.(isset($sort_hoteltype_id)?$sort_hoteltype_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="booking">Рейтинг Booking.com'.(isset($sort_booking)?$sort_booking:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);

    // шапка таблицы
//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="hoteltype_id">Тип отеля'.(isset($sort_hoteltype_id)?$sort_hoteltype_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="booking">Рейтинг<br>Booking.com'.(isset($sort_booking)?$sort_booking:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="distancecabcar_id">До канатки'.(isset($sort_distancecabcar_id)?$sort_distancecabcar_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="summerprice_id">Цена,&nbsp;зима,<br>руб/сут'.(isset($sort_summerprice_id)?$sort_summerprice_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="winterprice_id">Цена,&nbsp;лето,<br>руб/сут'.(isset($sort_winterprice_id)?$sort_winterprice_id:'').'</a></th>
//            <th>Контакты</th>
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="gostinicy">отправить выбранным</button></th>
//        </tr>
//    ';

    // шапка таблицы
    $_telo .= '
        <tr class="thead">
            <td style="width:1%;">фото</td>
            <td style="width:20%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="hoteltype_id">Тип отеля'.(isset($sort_hoteltype_id)?$sort_hoteltype_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="booking">Рейтинг<br>Booking'.(isset($sort_booking)?$sort_booking:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="distancecabcar_id">До подъёмника'.(isset($sort_distancecabcar_id)?$sort_distancecabcar_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="summerprice_id">Цена,&nbsp;зима,<br>руб/сут'.(isset($sort_summerprice_id)?$sort_summerprice_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="winterprice_id">Цена,&nbsp;лето,<br>руб/сут'.(isset($sort_winterprice_id)?$sort_winterprice_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="gostinicy">отправить запрос<br>выбранным</button></td>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12" style="background: #f5f5f5;">

<p>Здесь только актуальная информация об отелях, гостевых домам, частных апартаментах и недорогих хостелах, открытых в Красной Поляне в 2019. Дважды в год, весной и осенью, как раз перед зимним сезоном, мы обзваниваем все гостиницы и частные гостевые дома, уточняем адреса, телефоны, сайты и примерные цены.</p> 

<p>Важно, что на сайте приведена проверенная информация, которую мы стараемся сделать максимально полной. Актуальные цены, вам, конечно, нужно будет уточнить у отеля. Мы приводим здесь ориентировочные расценки «от и до», чтобы вы могли составить представление о ценовой политике отеля или гостевого дома. Обратите внимание, на этом сайте нет функционала бронирования. Информация носит справочный характер, не является оффертой.</p> 

<p>Бронируйте напрямую! Не платите посредникам.</p> 
                        </div>
                    </div>';
    $_telo .= '<script> var blockName = "hotels"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}