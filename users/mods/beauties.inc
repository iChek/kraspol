<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'beauties';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Красота и здоровье',
        '_title' => 'Красота и здоровье | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Красота и здоровье',
        '_description' => 'Красота и здоровье в Красной Поляне, расположение, бани, спа, йога, массаж, медицина, салоны красоты, фитнес, контакты, найти',
        'searchForm' => '[%html/searches/beauties%]'
    );

    //$checkboxes_class = 'col-sm-1';
    $checkboxes_offset = '';
    $checkboxes_array = Array();

    $nameForm = '';
    $selectedTerritory = 0;
    $selectedTransporttype = 0;
    $selectedBeautytype = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];
        if (isset($srch['beautytype'])) $selectedBeautytype = $srch['beautytype'];
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;
    $sort_beautytype_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';
//
//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="beautytype_id">Тип'.(isset($sort_beautytype_id)?$sort_beautytype_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);
//
//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="beautytype_id">Тип'.(isset($sort_beautytype_id)?$sort_beautytype_id:'').'</a></th>
//            <th>Контакты</th>
//
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="krasota-i-zdorove">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td>фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="beautytype_id">Тип'.(isset($sort_beautytype_id)?$sort_beautytype_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="krasota-i-zdorove">отправить запрос<br>выбранным</button></td>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12">
                        Бани и СПА, салоны красоты, медицинские центры, аптеки, услуги массажа, а также где позаниматься йогой или фитнесом в Красной Поляне - всё это без труда можно найти в этом разделе сайта.
                    </div>
                </div>';
    $_telo .= '<script> var blockName = "beauties"; </script>';
    include '_common_block2.inc';


} else {

    $ses->redirect('/');

}