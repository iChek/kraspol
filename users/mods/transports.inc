<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'transports';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Транспорт',
        '_title' => 'Транспорт | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Транспорт ',
        '_description' => 'Как добраться из аэропорта в Красную Поляну? Где взять машину напрокат и как заказать такси на курорт Роза Хутор, Горки Город или ГТЦ Газпром - всё о транспорте, вид транспорта, контакты, найти',
        'searchForm' => '[%html/searches/transports%]'
    );

    $checkboxes_offset = 'col-md-offset-4';
    $checkboxes_array = Array(
        Array( 'name' => 'transfer', 'title' => 'Трансфер', 'col' => 1)
    );

    $nameForm = '';
    $selectedTerritory = 0;
    $selectedTransporttype = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];
        if (isset($srch['transporttype'])) $selectedTransporttype = $srch['transporttype'];

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;
    $sort_transporttype_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="transporttype_id">Вид&nbsp;транспорта'.(isset($sort_transporttype_id)?$sort_transporttype_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);
//
//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="transporttype_id">Вид&nbsp;транспорта'.(isset($sort_transporttype_id)?$sort_transporttype_id:'').'</a></th>
//            <th>Контакты</th>
//
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="transport">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td style="width:1%;">фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <!-- <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td> -->
            <td><a href="#" class="thsort" data-sort-field="transporttype_id">Вид&nbsp;транспорта'.(isset($sort_transporttype_id)?$sort_transporttype_id:'').'</a></td>
            <td>Контакты</td>
            
            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="transport">отправить запрос<br>выбранным</button></th>
        </tr>
    ';
    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12">
                        <h3>Как добраться из аэропорта в Красную Поляну?</h3> 
                        Где взять машину напрокат и как заказать такси на курорт Роза Хутор, Горки Город или ГТЦ "Газпром"- всё о транспорте можно найти в этом разделе сайта при помощи удобного фильтра.
                    </div>
                </div>';
    $_telo .= '<script> var blockName = "transports"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}