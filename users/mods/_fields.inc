<?php

$s = new Table('schools'); $s->setOptions(Array('where'=>'`status`=1')); $schools = $s->getAllRowsArraysID();
$h = new Table('hotels'); $h->setOptions(Array('where'=>'`status`=1')); $hotels = $h->getAllRowsArraysID();
$c = new Table('courses'); $c->setOptions(Array('where'=>'`status`=1')); $courses = $c->getAllRowsArraysID();
$p = new Table('paytypes'); $p->setOptions(Array('where'=>'`status`=1')); $paytypes = $p->getAllRowsArraysID();

$schools_select = '<select name="school_id" id="schoold_id" class="form-control"><option value="none">{#select_school#}</option>';
foreach($schools as $k => $v) {
    $schools_select .= '<option value="'.$v['id'].'" *school_id_'.$v['id'].'*>'.$v['code'].'</option>';
}
$schools_select .= '</select>';

$hotels_select = '<select name="hotel_id" id="hoteld_id" class="form-control"><option value="none">{#select_hotel#}</option>';
foreach($hotels as $k => $v) {
    $hotels_select .= '<option value="'.$v['id'].'" *hotel_id_'.$v['id'].'*>'.$v['code'].'</option>';
}
$hotels_select .= '</select>';

$courses_select = '<select name="course_id" id="coursed_id" class="form-control"><option value="none">{#select_course#}</option>';
foreach($courses as $k => $v) {
    $courses_select .= '<option value="'.$v['id'].'" *course_id_'.$v['id'].'*>'.$v['code'].'</option>';
}
$courses_select .= '</select>';

$paytypes_select = '<select name="paytype_id" id="paytyped_id" class="form-control"><option value="none">{#select_paytype#}</option>';
foreach($paytypes as $k => $v) {
    $paytypes_select .= '<option value="'.$v['id'].'" *paytype_id_'.$v['id'].'*>'.$v['code'].'</option>';
}
$paytypes_select .= '</select>';

$fields = Array(
    "school" => Array (
        "name" => "school_id",
        "html" => $schools_select,
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => false,  "edit" => false),
            'schooldir' =>   Array("visible" => false,  "edit" => false),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "title" => Array (
        "name" => "title",
        "html" => '<input type="text" name="title" id="title" class="form-control" value="*title*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "name" => Array (
        "name" => "name",
        "html" => '<input type="text" name="name" id="name" class="form-control" value="*name*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "sname" => Array (
        "name" => "sname",
        "html" => '<input type="text" name="sname" id="sname" class="form-control" value="*sname*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "age" => Array (
        "name" => "age",
        "html" => '<input type="text" name="age" id="age" class="form-control" value="*age*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "dtstart" => Array (
        "name" => "dtstart",
        "html" => '<input type="text" name="dtstart" id="dtstart" class="form-control datepicker" value="*dtstart*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "dtfinish" => Array (
        "name" => "dtfinish",
        "html" => '<input type="text" name="dtfinish" id="dtfinish" class="form-control datepicker" value="*dtfinish*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "hotel" => Array (
        "name" => "hotel_id",
        "html" => $hotels_select,
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => false,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "course" => Array (
        "name" => "course_id",
        "html" => $courses_select,
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "room" => Array (
        "name" => "room",
        "html" => '<input type="text" name="room" id="room" class="form-control" value="*room*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => true,  "edit" => false)
        )
    ),
    "weeknum" => Array (
        "name" => "weeknum",
        "html" => '<input type="text" name="weeknum" id="weeknum" class="form-control onlyDigit" value="*weeknum*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "course_cost" => Array (
        "name" => "course_cost",
        "html" => '<input type="text" name="course_cost" id="course_cost" class="form-control moneyInput" value="*course_cost*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "discount" => Array (
        "name" => "discount",
        "html" => '<input type="text" name="discount" id="discount" class="form-control moneyInput" value="*discount*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "total_cost" => Array (
        "name" => "total_cost",
        "html" => '<input type="text" name="total_cost" id="total_cost" class="form-control moneyInput" value="*total_cost*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => false,  "edit" => false),
            'manager' =>   Array("visible" => false,  "edit" => false),
            'director' =>   Array("visible" => false,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "paid" => Array(
        "name" => "paid",
        "html" => '<input type="text" name="paid" id="paid" class="form-control moneyInput" value="*paid*">',
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "pay_type" => Array(
        "name" => "paytype_id",
        "html" => $paytypes_select,
        "access" => Array(
            'sysadmin' =>  Array("visible" => true,  "edit" => true),
            'manager' =>   Array("visible" => true,  "edit" => true),
            'director' =>   Array("visible" => true,  "edit" => false),
            'hoteldir' =>   Array("visible" => true,  "edit" => false),
            'schooldir' =>   Array("visible" => true,  "edit" => true),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    ),
    "isdeleted" => Array(
        "name" => "isdeleted",
        "html" => '<input type="hidden" name="isdeleted" value="0"><input type="checkbox" name="isdeleted" id="isdeleted" value="1" *isdeleted*>',
        "access" => Array(
            'sysadmin' =>  Array("visible" => false ,  "edit" => true),
            'manager' =>   Array("visible" => false,  "edit" => false),
            'director' =>   Array("visible" => false,  "edit" => false),
            'hoteldir' =>   Array("visible" => false,  "edit" => false),
            'schooldir' =>   Array("visible" => false,  "edit" => false),
            'teacher' =>   Array("visible" => false,  "edit" => false)
        )
    )
);
