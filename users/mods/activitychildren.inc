<?php

if (isset($isKp) && $isKp) {

    $tpl->indexPage('kp');
    $blockName = 'activitychildren';
    // Шаблон мета-данных
    $meta = Array(
        'page_title' => 'Отдых с детьми',
        '_title' => 'Отдых с детьми | Единый туристический справочник Красной Поляны',
        '_keywords' => 'Отдых с детьми',
        '_description' => 'Отдых с детьми в Красной Поляне, расположение, активный отдых, детские клубы, контакты, найти',
        'searchForm' => '[%html/searches/activitychildren%]'
    );

    //$checkboxes_class = 'col-sm-2';
    $checkboxes_offset = 'col-sm-offset-4';
    $checkboxes_array = Array(
        Array(
            'name' => 'aktivnyi_otdyh',
            'title' => 'активный отдых',
            'col' => 3,
        ),
        Array(
            'name' => 'detskie_kluby',
            'title' => 'детские клубы',
            'col' => 3,
        ),
    );

    /**
    echo '<pre>';
    echo '$checkboxes_array = Array('."\n";
    foreach($checkboxes_array as $k => $v) {
    echo '    Array('."\n";
    echo "        'name' => '$k',"."\n";
    echo "        'title' => '$v',"."\n";
    echo "        'col' => 1,"."\n";
    echo '    ),'."\n";
    }
    echo ');';
    echo '<pre>';exit;/**/


    $nameForm = '';
    $selectedTerritory = 0;
    $selectedTransporttype = 0;
    $selectedChildrenactivity = 0;

    include '_common_block1.inc';

//    if (isset($ses[$blockName.'_search'])) {
//        $srch = $ses[$blockName.'_search'];
    if (isset($post)) {
        $srch = $post;
        if (isset($srch['name'])) $nameForm = $srch['name'];
        if (isset($srch['territory'])) $selectedTerritory = $srch['territory'];

        if (isset($srch['isCheck'])) {
            $isCheck = $srch['isCheck'];
            foreach ($isCheck as $key => $val) {
                $tpl->add('check_' . $key, $val == 1 ? 'checked' : '');
            }
        }
    }
    // ПОИСК

    $sort_title = $sort_item_pin;
    $sort_territory_id = $sort_item_pin;

    $sort_field = 'sort_'.$ords['field'];
    $$sort_field = '&nbsp;<i class="fa fa-sort-amount-'.($ords['dir'] == '' ? 'asc':'desc').'"></i>';

//    $head_list = '
//        <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Название отеля'.(isset($sort_title)?$sort_title:'').'</a></th>
//        <th>|</th>
//        <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//    ';
//    $tpl->add('head_list', $head_list);

//    $_telo .= '
//        <tr style="background-color: #f0f7e9;">
//            <th colspan="2" style="width:100%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></th>
//            <th><a href="#" class="thsort" data-sort-field="territory_id">Территория'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></th>
//            <th>Контакты</th>
//            <th colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить всем</button></th>
//        </tr>
//    ';

    $_telo .= '
        <tr class="thead">
            <td>фото</td>
            <td style="width:50%;"><a href="#" class="thsort" data-sort-field="title">Наименование'.(isset($sort_title)?$sort_title:'').'</a></td>
            <td><a href="#" class="thsort" data-sort-field="territory_id">Расположение'.(isset($sort_territory_id)?$sort_territory_id:'').'</a></td>
            <td>Контакты</td>
            <td colspan="2"><button class="btn btn_self sel-many-objs-send" data-selname="letniy-otdyh">отправить запрос<br>выбранным</button></td>
        </tr>
    ';

    $_telo .= '</table>';

    $_telo .= '</div>';
    $_telo .= '<div class="row">
                    <div class="col-md-12">
                        <h3>Если Вы отдыхаете на курортах Красной Поляны вместе с детьми</h3> 
                        То в этом разделе сайта можно найти адреса и телефоны детских клубов, горнолыжных школ, творческих центров.
                    </div>
                </div>';
    $_telo .= '<script> var blockName = "activitychildren"; </script>';
    include '_common_block2.inc';

} else {

    $ses->redirect('/');

}