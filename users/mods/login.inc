<?php

if ($ses->isUserAuth) $ses->redirect('/members/');

$tpl->indexPage('login');

$tpl->add('_title', '{#enter#}');

$_errortelo = '';

if (isset($_POST['loginsubmit']))
{
    $login = $_POST['login'];
    $password = $_POST['password'];

    $users = new Table('users');
    $users->setOptions(Array('where' => "(`login`='$login') and (`password`='".md5($password)."')"));
    $row = $users->getRow();

    if (!$row)
    {
        $_errortelo .= $helper->errorMsg('{#wrong_login_password#}');
    }
    else
    {
        if ($row['status'] == '0')
        {
            $_errortelo .= $helper->infoMsg('{#is_no_user_active#}!');
        }
        elseif ($row['status'] == '2')
        {
            $_errortelo .= $helper->warningMsg('{#is_block_user#}');
        }
        else
        {
            $ses['uinit'] = $row;
            $ses->isUserAuth = true;
            $ses->redirect('/members/');
        }
    }
    
}
$tpl->add('_errortelo',$_errortelo);
$_telo .= '[%forms/login/login_form%]';