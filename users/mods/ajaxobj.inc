<?php

$linktypes = Array( 0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");
$data = '';
$data2 = '';
$data3 = '';
$cnt = 0;

if (isset($_POST['type']) && isset($_POST['startItem']) && isset($_POST['numItems'])) {
    $type = $_POST['type'];
    $startItems = round($_POST['startItem']);
    $numItems = round($_POST['numItems']);
    include 'ajax/'.$type.'.inc';
} elseif (isset($_GET['type']) && isset($_GET['startItem']) && isset($_GET['numItems'])) {
    $type = $_GET['type'];
    $startItems = round($_GET['startItem']);
    $numItems = round($_GET['numItems']);
    include 'ajax/'.$type.'.inc';
}

$result = Array(
    'result' => true,
    'cnt' => $cnt,
    'data' => $data,
    'data2' => $data2,
    'data3' => $data3
);

$ready_data = json_encode($result);
if (!$ready_data) {
    switch (json_last_error())
    {
        case JSON_ERROR_NONE:
            $jsonerror =  ' - Ошибок нет';
            break;
        case JSON_ERROR_DEPTH:
            $jsonerror =  ' - Достигнута максимальная глубина стека';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $jsonerror =  ' - Некорректные разряды или не совпадение режимов';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $jsonerror =  ' - Некорректный управляющий символ';
            break;
        case JSON_ERROR_SYNTAX:
            $jsonerror =  ' - Синтаксическая ошибка, не корректный JSON';
            break;
        case JSON_ERROR_UTF8:
            $jsonerror =  ' - Некорректные символы UTF-8, возможно неверная кодировка';
            break;
        default:
            $jsonerror =  ' - Неизвестная ошибка';
            break;
    }
    die('Ошибка преобразования - '.$jsonerror);

} else {
    echo $ready_data;
}
exit;