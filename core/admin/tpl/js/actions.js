
function getChar(event) {
    if (event.which == null) { // IE
        if (event.keyCode < 32) return null; // спец. символ
        return String.fromCharCode(event.keyCode)
    }

    if (event.which != 0 && event.charCode != 0) { // все кроме IE
        if (event.which < 32) return null; // спец. символ
        return String.fromCharCode(event.which); // остальные
    }
    return null; // спец. символ
}

$(document).ready(function(){
    
    $("input.onlyDigit").each(function(){
    
        $(this).keydown(function(event) {
            // Разрешаем: backspace, delete, tab и escape
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
                 // Разрешаем: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                 // Разрешаем: home, end, влево, вправо
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // Ничего не делаем
                     return;
            }
            else {
                // Обеждаемся, что это цифра, и останавливаем событие keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        
    });

    $("input.digitFloat").on('keypress', function(event) {

        var sym = getChar(event),
            code = sym.charCodeAt(0);

        // 46 == .
        // 48 == 0 ..  57 == 9
        if ( code == 46 || (code >= 48 && code <= 57)) {
            return;
        } else {
            event.preventDefault();
        }

    });

    $("input.moneyInput").on('keypress', function(event) {

        var sym = getChar(event),
            code = sym.charCodeAt(0);

        // 46 == .
        // 48 == 0 ..  57 == 9
        if ( code == 46 || (code >= 48 && code <= 57)) {
            return;
        } else {
            event.preventDefault();
        }

    });

});