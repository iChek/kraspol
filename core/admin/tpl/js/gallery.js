$(function () {

    var filez = [],
        cntfilez = 0;

    $('#fileupload').fileupload({
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(jpe?g)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                filez.push(file.name);
                // $("div.load-doc").text("Загружен файл: " + file.name);
                // $("#load-document").val(file.name);
                // $('#progress .bar').css('width', '0%');
                // $("div.load-doc").show();
            });
            if (filez.length == cntfilez) {
                // загрузка завершена, надо сохранить загруженное!!
                $("#files_array").val(filez.join(','));
                $("#progress").hide();
                $("#upld_images").submit();
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        },
        add: function (e, data) {
            filez = [];
            $("#progress").show();
            $("#files_array").val("");
            cntfilez = data.originalFiles.length;
            var uploadErrors = [];
            var acceptFileTypes = /^image\/(jpe?g)$/i;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                 uploadErrors.push('Данный формат файла не поддерживается');
            }
            if (data.originalFiles[0]['size'] > 134217728) {
                uploadErrors.push('Размер файла должен превышать 130Мб');
            }
            if (uploadErrors.length > 0) {
                $("#progress").after(data.originalFiles[0]['name'] + '<div style="color:red;font-weight:bold;">' + uploadErrors.join("<br>") + '</div><hr>');
            } else {
                data.submit();
            }
        }
    });

});