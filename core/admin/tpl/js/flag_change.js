
$(function(){

    var spin = '<i class="fa fa-spin fa-spinner"></i>';

    $("a.flag-change").on('click', function(ev){
        var $this = $(this);
        $this.html(spin);

        var table = $(this).data("table"),
            field = $(this).data("field"),
            id = $(this).data("rowid"),
            str = $(this).data("str");

        var data = { "type":"flag", "table":table, "field":field, "id":id, "str":str };

        $.ajax({
            url: '/control/serv',
            type: 'post',
            data: data,
            dataType: "json",
            success: function(resp) { $this.html(resp.html); }
        });

        ev.preventDefault();
    });

});
