
function hotkey(event)
{
    var isCtrl = event.ctrlKey;
    var isAlt = event.altKey;
    var id = event.keyCode;
    //document.getElementById("key").innerHTML = (isAlt?'alt+':'') + (isCtrl?'ctrl+':'') + id;

    //if (isAlt && isCtrl && id = 83)
    //{
    // сработало нажатие Ctrl+Alt+S
    //}

}

$(document).ready(function () {
    $('[data-toggle="popover"]').popover({html: true, trigger: 'hover'});

    $("div.citiesSelect").hover(
            function () {
                $(this).css({"background-color": "#eeeeee"});
            },
            function () {
                $(this).css({"background-color": "white"});
            }
    );

    $('[data-toggle="tooltip"]').tooltip();

    $("th.sortcolumn").each(function () {

        $(this).click(function () {
            var href = window.location.href;
            var datahref = $(this).attr("data-href");
            console.log(href);
            console.log(datahref);
            window.location.href = datahref;
        });

    });


    $("input.onlyDigit").each(function () {

        $(this).keydown(function (event) {
            // Разрешаем: backspace, delete, tab и escape
            if (event.keyCode == 46 ||
                    event.keyCode == 8 ||
                    event.keyCode == 9 ||
                    event.keyCode == 27 ||
                    event.keyCode == 13 ||
                    // Разрешаем: Ctrl+A
                            (event.keyCode == 65 && event.ctrlKey === true) ||
                            // Разрешаем: home, end, влево, вправо
                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                        // Ничего не делаем
                        return;
                    } else {
                        // Обеждаемся, что это цифра, и останавливаем событие keypress
                        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                            event.preventDefault();
                        }
                    }
                });

    });

    $("input.onlySumma").each(function () {
        $(this).keydown(function (event) {
            var val = $(this).val();
            // Разрешаем: backspace, delete, tab, escape, точка
            if (event.keyCode == 46 ||
                    event.keyCode == 8 ||
                    event.keyCode == 9 ||
                    event.keyCode == 27 ||
                    event.keyCode == 13 ||
                    (event.keyCode == 110 && val.indexOf(".") == -1) ||
                    (event.keyCode == 190 && val.indexOf(".") == -1) ||
                    // Разрешаем: Ctrl+A
                            (event.keyCode == 65 && event.ctrlKey === true) ||
                            // Разрешаем: home, end, влево, вправо
                                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                        // Ничего не делаем
                        return;
                    } else {
                        // Обеждаемся, что это цифра, и останавливаем событие keypress
                        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                            event.preventDefault();
                        }
                    }
                });
    });

    $("input.setDatePicker").each(function () {
        $(this).datetimepicker({
            weekStart: 1,
            language: "ru",
            pickTime: false
        });
    });


    if (document.getElementById("tablez"))
    {
        var tableOffset = $("#tablez").offset().top - 50;
        var $header = $("#tablez > thead").clone();
        var $fixedHeader = $("#header-fixed").append($header);
        $("#header-fixed").width($("#tablez").width());

        $(window).bind("scroll", function () {

            var offsetTop = $(this).scrollTop();
            var offsetLeft = $(this).scrollLeft();

            $fixedHeader.css("margin-left", -1 * offsetLeft);

            if (offsetTop >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.css("margin-top", "51px");
                $fixedHeader.show();
                console.log('show');
            } else if (offsetTop < tableOffset) {
                $fixedHeader.hide();
                console.log('hide');
            }

            $("#header-fixed th").each(function (index) {
                var index2 = index;
                $(this).css("background-color", "white");
                //$(this).css("vertical-align","bottom");
                $(this).width(function (index2) {
                    return $("#tablez th").eq(index).width();
                });
            });

        });
    }

    $('.contact_obj').click(function () {
        console.log('edit_contact_obj');
        var $this = $(this);
        var save = $this.data('save');
        var object_id = $this.data('object_id');
        var id = $this.data('id');
        $.ajax({
            type: "POST",
            url: "/control/ajax",
            data: $this.data(),
            dataType: 'html',
            success: function (data) {
                $('body').prepend(data);
                $('body').find('#contact_obj').find('form').attr('action', '/control/ajax');
                $('body').find('#contact_obj').find('form').attr('onsubmit', 'sendFormObj(this);return false;');
                $('body').find('#contact_obj').find('input[name="objects_contacts_submit"]').val(save);
                $('body').find('#contact_obj').find('#object_id').val(object_id);
                $('body').find('#contact_obj').find('input[name="objects_contacts_submit"]').attr('name', 'method');
                $('body').find('#contact_obj').find('form').prepend('<input type="hidden" name="id" value="' + id + '">');
                $('body').find('#contact_obj').modal('show');
                $('body').on('hidden.bs.modal', '#contact_obj', function () {
                    $("#contact_obj").remove();
                });
            },
            error: function () {
                console.log('contact_obj error');
            }
        });
    });
    
    $('.remove_contact_obj').click(function () {
        var $this = $(this);
        if (confirm('Вы подтверждаете удаление контакта?')) { 
            $.ajax({
                type: "POST",
                url: "/control/ajax",
                data: $this.data(),
                dataType: 'json',
                success: function (data) {
                    if (!data.error) {
                        location.reload();
                    } else {
                        alert(data.mess);
                    }
                },
                error: function () {
                    console.log('remove_section_obj error');
                }
            });
        }
    });

    $('.add_section_obj').click(function () {
        console.log('add_section_obj');
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: "/control/ajax",
            data: $this.data(),
            dataType: 'html',
            success: function (data) {
                $('body').find('.object_block_form').html(data);
            },
            error: function () {
                console.log('add_section_obj error');
            }
        });
    });

    $('body').on('change', '#chose_section_obj', function () {
        var $this = $(this);
        var section = $this.val();
        $.ajax({
            type: "POST",
            url: "/control/ajax",
            data: {section: section, method: 'get_section_form'},
            dataType: 'html',
            success: function (data) {
                $this.parent().parent().parent().find('.section_form_block').html(data)
                        .find('form')
                        .attr('action', '/control/ajax')
                        .attr('onsubmit', 'sendFormObj(this);return false;')
                        .prepend('<input type="hidden" name="method" value="save_section_obj">')
                        .prepend('<input type="hidden" name="section" value="' + section + '">')
                        .find('#object_id').val($this.data('object_id'));
            },
            error: function () {
                console.log('chose_section_obj error');
            }
        });
    });

    $('.edit_section').click(function () {
        var $this = $(this);
        var $parent = $this.parent();
        $.ajax({
            type: "POST",
            url: "/control/ajax",
            data: $this.data(),
            dataType: 'html',
            success: function (data) {
                $parent.html(data);

                $parent
                        .find('form')
                        .attr('action', '/control/ajax')
                        .attr('onsubmit', 'sendFormObj(this);return false;')
                        .prepend('<input type="hidden" name="method" value="edit_section">')
                        .prepend('<input type="hidden" name="section" value="' + $this.data('section') + '">')
                        .prepend('<input type="hidden" name="id" value="' + $this.data('id') + '">');

//                $('body').find('#edit_section').modal('show');
//                $('body').on('hidden.bs.modal', '#edit_section', function () {
//                    $("#edit_section").remove();
//                });
            },
            error: function () {
                console.log('edit_section error');
            }
        });
    });

    $('.remove_section_obj').click(function () {
        var $this = $(this);
        if (confirm('Вы подтверждаете удаление?')) {
            $.ajax({
                type: "POST",
                url: "/control/ajax",
                data: $this.data(),
                dataType: 'json',
                success: function (data) {
                    if (!data.error) {
                        location.reload();
                    } else {
                        alert(data.mess);
                    }
                },
                error: function () {
                    console.log('remove_section_obj error');
                }
            });
        }
    });
    

});

function sendFormObj(form) {
    console.log('sendFormObj');
    jQuery.ajax({
        type: "POST",
        url: jQuery(form).attr('action'),
        data: jQuery(form).serialize(),
        dataType: 'json',
        success: function (data) {
            if (!data.error) {
                location.reload();
            } else {
                alert(data.mess);
            }
        },
        error: function () {
            console.log('sendFormObj error');
        }
    });

    return false;
}