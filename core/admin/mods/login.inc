<?php

$tpl->indexPage('login');

$_errortelo = '';

if (isset($_POST['loginsubmit'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];

    $admins = new Table('admins');
    $admins->setOptions(Array('where' => "(`login`='$login') and (`password`='" . md5($password) . "')"));
    $row = $admins->getRow();
    if ($row) {
        if ($row['status'] == '0') {
            $_errortelo .= $helper->infoMsg('Извините, но данный аккаунт ещё <b>не активирован</b>!');
        } elseif ($row['status'] == '2') {
            $_errortelo .= $helper->warningMsg('Извините, но данный аккаунт <b>заблокирован</b>!');
        } else {
            $_errortelo .= $helper->successMsg('Good');
            $ses['init'] = $row;
            $ses->redirect(SITE_ROOT . cfg::$cms);
        }
    } else {
        $_errortelo .= $helper->errorMsg('Не правильный логин или пароль');
    }
}

$tpl->add('_errortelo',$_errortelo);
