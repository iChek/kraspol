<?php $ses->adminAuth();

$id = round($router->getAction(2));

$_breadcrumbs .= '<li><a href="'.DS.cfg::$cms.DS.'admins'.DS.'">Администраторы</a></li>';
$_breadcrumbs .= '<li class="active">Настройка доступа</li>';
$_telo .= '<h2>Настройка доступа</h2><hr>';

$m = new Table('moduls'); $m->setOptions(Array('order'=> ' `title` ' ));

$ada = new Table('adminsaccess');

$a = new Table('admins');
$r = $a->getRowById($id);
if ($r)
{
    $_telo .= '<h4>'.$r['title'].' ('.$r['login'].')</h4>';
    $mr = $m->getAllRows();
    
    $ada->setOptions(Array('where' => " (`user_id` = '$id') "));
    if (isset($_POST['saveaccess']))
    {
        //##echo $helper->aprn($_POST); exit;

        $ada->deleteAllRows();
        
        $read = $_POST['read'];
        $edit = $_POST['edit'];
        foreach($read as $modul_id => $v)
        {
            $isRead = ($v==1)?true:false;
            $isEdit = ($edit[$modul_id]==1)?true:false;
            
            unset($_re);
            if ($isRead || $isEdit)
            {
                $_re['user_id'] = $id;
                $_re['modul_id'] = $modul_id;
                $_re['read'] = $v;
                $_re['edit'] = $edit[$modul_id];
                $ada->saveRow($_re,$e);
            }
        }
        //##$_telo .= $helper->aprn($_POST);
        $_telo .= $helper->successMsg('Данные сохранены!');
    }
    
    $access = Array();
    $adarows = $ada->getAllRows();
    if ($adarows)
    {
        foreach($adarows as $ak => $av)
        {
            $access[$av['modul_id']]['read'] = $av['read']==1?true:false;
            $access[$av['modul_id']]['edit'] = $av['edit']==1?true:false;
        }
    }

    //##echo $helper->aprn($mr); exit;
    foreach($mr as $mrk => $mrv)
    {
        $modul_id = $mrv['id'];
        $mr[$mrk]['readcheck'] = '';
        $mr[$mrk]['editcheck'] = '';
        
        if (isset($access[$modul_id]))
        {
            if ($access[$modul_id]['read']) $mr[$mrk]['readcheck'] = 'checked';
            if ($access[$modul_id]['edit']) $mr[$mrk]['editcheck'] = 'checked';
        }
    }
    
    $_telo .= $tpl->printTable('admins','access',$mr);
}
else
{
    $_telo .= $helper->errorMsg('Администратор с ID:'.$id.' не найден');
}