<?php $ses->adminAuth();

// Модуль управления настройками сайта

$meta = Array('_title' => 'Настройки сайта - (АП)',
              '_keywords' => 'Настройки сайта - (АП)',
              '_description' => 'Настройки сайта - (АП)'
                );
$tpl->addArray($meta);

$_breadcrumbs .= '<li class="active">Настройки сайта</li>';
$_telo .= '<h2>Настройки сайта</h2><hr>';

$options = new Table('options');

$action = ''; if (isset($router->path[2])) $action = $router->path[2];

$opts = $options->getAllRows();
//##$_telo .= $helper->aprn($opts);

if (isset($_POST['optionsubmit']) && !$onlyReadAccess)
{
    //##$_telo .= $helper->aprn($_POST);
    foreach($opts as $ok => $ov)
    {
        $key = $ov['key'];
        if (isset($_POST[$key])) $opts[$ok]['value'] = $_POST[$key];
    }
    
    foreach($opts as $ok => $ov)
    {
        $options->updateRow($ov);
    }
    
    $_errortelo .= $helper->successMsg('Данные сохранены');
}

//##$_telo .= $helper->aprn($opts);

$tabs = Array();
foreach($opts as $ok => $ov)
{
    $tabs[$helper->createFolderName($ov['tab'])][] = Array('name'=>$ov['tab'],'data'=>$ov);
}

$tabcontent = '';

$_telo .= '
<form action="" method="post" class="form-horizontal">
<input type="hidden" name="optionsubmit" value="1">
';

$l = 0;
$_telo .= '<div role="tabpanel">';
$_telo .= '<ul class="nav nav-tabs" role="tablist">';
foreach($tabs as $tk => $tv)
{
    $tval = $tv['0'];
    $_telo .= '<li role="presentation" '.(($l==0)?' class="active"':'').'>';
    $_telo .= '<a href="#'.$tk.'" aria-controls="'.$tk.'" role="tab" data-toggle="tab">'.$tval['name'].'</a>';
    $_telo .= '</li>';
    
    $tabcontent .= '<div role="tabpanel" class="tab-pane '.(($l==0)?' active':'').'" id="'.$tk.'">';
    foreach($tv as $tkey => $tval)
    {
        $data = $tval['data'];
        $tabcontent .= '
        <div class="form-group">
            <label for="'.$data['key'].'" class="col-sm-2 control-label">'.$data['title'].'</label>
            <div class="col-sm-10">
        ';
        
        if ($data['type'] == 0 || $data['type'] == '')
        {
            $tabcontent .= '<input type="text" class="form-control" id="'.$data['key'].'" name="'.$data['key'].'" placeholder="'.$data['title'].'" value="'.$data['value'].'">';
        }
        else
        {
            $tabcontent .= '<textarea rows="10" class="form-control" id="'.$data['key'].'" name="'.$data['key'].'" placeholder="'.$data['title'].'">'.$data['value'].'</textarea>';
        }
        
        $tabcontent .= '
            </div>
        </div>
        ';
    }
    $tabcontent .= '</div>';
    
    $l++;
}
$_telo .= '</ul>';

$_telo .= '<br>';

$_telo .= '<div class="tab-content">';
$_telo .= $tabcontent;
$_telo .= '</div>';

$_telo .= '</div>';

if (!$onlyReadAccess) $_telo .= '<button type="submit" class="btn btn-default">Сохранить</button>';

$_telo .= '</form>';
