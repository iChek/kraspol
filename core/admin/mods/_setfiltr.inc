<?php

if (isset($_POST['filtrsubmit']))
{
    $filtraction = $_POST['filtraction'];
    if ($filtraction == 'clear')
    {
        unset($ses['filtr_'.$tableName]);
    }
    else
    {
        unset($_POST['filtrsubmit']);
        unset($_POST['filtraction']);
        $ses['filtr_'.$tableName] = $_POST;
    }
}

if (isset($ses['filtr_'.$tableName]))
{
    $filtr = $ses['filtr_'.$tableName];
    //##$_telo .= $helper->aprn($filtr);
    
    /**/
    foreach($filtr as $fk => $fv)
    {
        if (($fv['type'] == 'string'
             || $fv['type'] == 'phone'
             || $fv['type'] == 'email')
            && trim($fv['value'])!='')
        {
            $warr[] = "(`$fk` like '".str_replace('*','%',$fv['value'])."')";
            $filtrdata[$fk] = $fv['value'];
        }
        elseif (($fv['type'] == 'subtable' 
                 || $fv['type'] == 'sselect')
            && trim($fv['value'])!='none')
        {
            $warr[] = "(`$fk` = '".$fv['value']."')";
            $filtrdata[$fk] = $fv['value'];
        }
        elseif ($fv['type'] == 'date' && (trim($fv['value1'])!='' || trim($fv['value2'])!=''))
        {
            $w = '';
            if (trim($fv['value1'])!='')
            {
                $dt = strtotime($fv['value1']);
                $w = "(`$fk`>='".$dt."')";
                $filtrdata[$fk.'1'] = $fv['value1'];
            }
            if (trim($fv['value2'])!='')
            {
                $dt = strtotime($fv['value2']) + 86399;
                $w .= ($w!=''?' and ':'')."(`$fk`<='".$dt."')";
                $filtrdata[$fk.'2'] = $fv['value2'];
            }
            $warr[] = $w;
        }
        elseif ($fv['type'] == 'integer' && (trim($fv['value1'])!='' || trim($fv['value2'])!=''))
        {
            $w = '';
            if (trim($fv['value1'])!='')
            {
                $w = "(`$fk`>='".$fv['value1']."')";
                $filtrdata[$fk.'1'] = $fv['value1'];
            }
            if (trim($fv['value2'])!='')
            {
                $w .= ($w!=''?' and ':'')."(`$fk`<='".$fv['value2']."')";
                $filtrdata[$fk.'2'] = $fv['value2'];
            }
            $warr[] = $w;
        }
    }
    
    if (isset($warr)) { $sqloptions['where'] = join(' and ',$warr); $showFiltrBlock = true; }
    
    //$_telo .= $sqloptions['where'];
}