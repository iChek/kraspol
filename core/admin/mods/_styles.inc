<?php
$linktypes = Array( 0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");

//632
$_breadcrumbs .= '<li class="active">Очистка стилей</li>';
$_telo .= '<h2>Очистка стилей</h2><hr>';

$action = $router->getAction(2);
$o = new Table('objects');

if ($action == '') {

    $o->setOptions(Array('order' => ' `title` '));
    $objs = $o->getAllRows();
    if ($objs) {

        $_telo .= '<table class="table table-striped table-hover">'; //table-condensed">';
        foreach ($objs as $key => $obj) {

            $_telo .= '<tr>';
            $_telo .= '<td><button class="btn btn-xs clear-style clear-' . $obj['id'] . '" data-id="' . $obj['id'] . '" ' . ($obj['descr_bkp'] != '' ? 'disabled' : '') . '>очистить стиль</button></td>';
            $_telo .= '<td><button class="btn btn-xs restore-bkp restore-' . $obj['id'] . '" data-id="' . $obj['id'] . '" ' . ($obj['descr_bkp'] == '' ? 'disabled' : '') . '>восстановить</button></td>';
            $_telo .= '<td><a href="/' . cfg::$cms . '/objects/' . $obj['id'] . '/edit/" target="_blank" class="btn btn-xs"><i class="fa fa-pencil"></i></a></td>';
            $_telo .= '<td style="width: 100%;"><a href="/' . $linktypes[$obj['linktype']] . '/' . $obj['folder'] . '" target="_blank">' . $obj['title'] . '</a></td>';
            $_telo .= '</tr>';

        }
        $_telo .= '</table>';

        $_telo .= '
    <script>
    $(function(){
    
        $(".clear-style").on("click", function(){
            
            var $this = $(this);
            var id = $(this).data("id");
            var url = "/'.cfg::$cms.'/_styles/clearstyle/?id=" + id;
            
            $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");
            $.getJSON(url, function(resp){
                if (resp.result) {
                    $(".clear-" + id).attr("disabled", "disabled");
                    $(".restore-" + id).removeAttr("disabled");
                } else {
                    $(".clear-" + id).removeAttr("disabled");
                    $(".restore-" + id).attr("disabled", "disabled");
                }
                $this.html("очистить стиль");
            });
            
        });
    
    
        $(".restore-bkp").on("click", function(){
            
            var $this = $(this);
            var id = $(this).data("id");
            var url = "/'.cfg::$cms.'/_styles/restore/?id=" + id;
            $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");
            $.getJSON(url, function(resp){
                if (resp.result) {
                    $(".restore-" + id).attr("disabled", "disabled");
                    $(".clear-" + id).removeAttr("disabled");
                } else {
                    $(".restore-" + id).removeAttr("disabled");
                    $(".clear-" + id).attr("disabled", "disabled");
                }
                $this.html("восстановить");
            });
            
        });
    
    });
    </script>
    ';

    }

} elseif ($action == 'clearstyle') {

    $id = round($_GET['id']);
    $obj = $o->getRowByID($id);
    if ($obj) {

        unset($_re);
        $_re['id'] = $obj['id'];
        $_re['descr_bkp'] = $obj['descr'];
        $o->updateRow($_re);

        unset($_re['descr_bkp']);
        $descr = $obj['descr'];
        $descr = preg_replace('~style="[^"]*"~i', '', $descr);
        $_re['descr'] = $descr;
        $o->updateRow($_re);

        echo '{"result":true}';
    } else {
        echo '{"result":false}';
    }
    exit;

} elseif ($action == 'restore') {
    $id = round($_GET['id']);
    $obj = $o->getRowByID($id);
    if ($obj) {

        unset($_re);
        $_re['id'] = $obj['id'];
        $_re['descr'] = $obj['descr_bkp'];
        $_re['descr_bkp'] = null;
        $o->updateRow($_re);

        echo '{"result":true}';
    } else {
        echo '{"result":false}';
    }
    exit;
}