<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$ses->adminAuth();

$object_id = filter_input(INPUT_POST, 'object_id', FILTER_VALIDATE_INT);
$method = filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING);


switch ($method) {
    
    case 'remove_contact_obj' :
        
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        
        $contacts_table = new Table('objects_contacts');
        
        $contacts_table->deleteRowByID($id);
    
        echo json_encode(array('error' => false));
        break;
    
    case 'get_edit_contact_form' :
        
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        
        $contacts_table = new Table('objects_contacts');
        $contacts_table->setOptions(array('where' => '`id` = "' . (int) $id . '"'));
        $contacts = $contacts_table->getRow();

        $html = '';

        if (!empty($contacts)) {
            $html .= '<div class="modal fade" id="contact_obj" tabindex="-1" role="dialog" aria-labelledby="contact_obj_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="contact_obj_label">Редактировать контакты</h5>
        
      </div>
      <div class="modal-body">';

            $html .= $tpl->printForm($contacts_table, $contacts, false);
            $html .= '</div>
    </div>
  </div>
</div>';
        }
        echo $html;
        exit();

        break;

    case 'edit_contact' :

        $contacts_table = new Table('objects_contacts');
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

        $_err = '';
        $result = $contacts_table->validForm();
        $values = $contacts_table->getFormValues();

        if ($result) {
            $values['id'] = $id;
            $contacts_table->updateRow($values);
            echo json_encode(array('error' => false));
            exit();
        }

        if (!$result) {
            echo json_encode(array('error' => true, 'mess' => 'Не верно заполнена форма!'));
            $contacts_table->unsetpost();
            exit();
        }

        break;

    case 'get_add_contact_form' :
        $contacts_table = new Table('objects_contacts');

        $html = '';
        $html .= '<div class="modal fade" id="contact_obj" tabindex="-1" role="dialog" aria-labelledby="contact_obj_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="contact_obj_label">Редактировать контакты</h5>
      </div>
      <div class="modal-body">';

        $html .= $tpl->printForm($contacts_table, array(), false);
        $html .= '</div>
    </div>
  </div>
</div>';
        echo $html;
        exit();

        break;

    case 'add_contact' :

        $contacts_table = new Table('objects_contacts');

        $result = $contacts_table->validForm();
        $values = $contacts_table->getFormValues();

        if ($result) {
            $contacts_table->saveRow($values, $code);
            echo json_encode(array('error' => false));
            exit();
        }

        if (!$result) {
            echo json_encode(array('error' => true, 'mess' => 'Не верно заполнена форма!'));
            $contacts_table->unsetpost();
            exit();
        }

        break;

    case 'get_add_section_obj_form' :

        $obj_tables = array();
        $contacts_table = new Table('objects_contacts');
        $sql = 'SELECT * FROM `' . cfg::$prfx . 'objects_names` GROUP BY `newname`';
        $sth = $contacts_table->_q($sql);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        while ($obj_tab = $sth->fetch()) {
            $obj_tables[] = $obj_tab;
        }

        $html = '';
        $html .= '
                <select name="section" id="chose_section_obj" class="form-control" data-object_id="' . $object_id . '">
<option value="0">Выбрать</option>';
        
        foreach ($obj_tables as $val){
            $html .= '<option value="' . $val['newname'] . '">' . $val['title'] . '</option>';
        }
        
        $html .= '</select><div class="section_form_block" style="margin-top: 20px"></div>';
        echo $html;
        exit();
        break;
        
    case 'get_section_form' :
        $table = filter_input(INPUT_POST, 'section', FILTER_SANITIZE_STRING);
        $t = new Table($table);
        $html = $tpl->printForm($t, array(), false);
        echo $html;
        exit();
        break;
    
    case 'save_section_obj' :
        $table = filter_input(INPUT_POST, 'section', FILTER_SANITIZE_STRING);
        
        $section_table = new Table($table);

        $result = $section_table->validForm();
        $values = $section_table->getFormValues();

        if ($result) {
            $section_table->saveRow($values, $code);
            echo json_encode(array('error' => false));
            exit();
        }

        if (!$result) {
            echo json_encode(array('error' => true, 'mess' => 'Не верно заполнена форма!'));
            $contacts_table->unsetpost();
            exit();
        }
        
        break;
        
    case 'get_edit_section_form' :
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $section = filter_input(INPUT_POST, 'section', FILTER_SANITIZE_STRING);
        
        $section_table = new Table($section);
        $section_obj = $section_table->getRowById($id);

        $html = '';

        if (!empty($section_obj)) {
//            $html .= '<div class="modal fade" id="edit_section" tabindex="-1" role="dialog" aria-labelledby="contact_obj_label" aria-hidden="true">
//  <div class="modal-dialog" role="document">
//    <div class="modal-content">
//      <div class="modal-header">
//        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//          <span aria-hidden="true">&times;</span>
//        </button>
//        <h5 class="modal-title" id="contact_obj_label">Редактировать объект</h5>
//        
//      </div>
//      <div class="modal-body">';
            $html .= '<div id="edit_section">';
            $html .= $tpl->printForm($section_table, $section_obj, false);
            $html .= '</div>';
//            $html .= '</div>
//    </div>
//  </div>
//</div>';
        }
        echo $html;
        exit();
        
        break;
        
    case 'edit_section' :
        
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $section = filter_input(INPUT_POST, 'section', FILTER_SANITIZE_STRING);
        
        $section_table = new Table($section);

        $_err = '';
        $result = $section_table->validForm();
        $values = $section_table->getFormValues();

        if ($result) {
            $values['id'] = $id;
            $section_table->updateRow($values);
            echo json_encode(array('error' => false));
            exit();
        }

        if (!$result) {
            echo json_encode(array('error' => true, 'mess' => 'Не верно заполнена форма!'));
            $contacts_table->unsetpost();
            exit();
        }
        
        break;
        
    case 'remove_section_obj' :
        
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $section = filter_input(INPUT_POST, 'section', FILTER_SANITIZE_STRING);
        
        $section_table = new Table($section);
        
        $section_table->deleteRowByID($id);
        
        echo json_encode(array('error' => false));
        break;
}

exit();

