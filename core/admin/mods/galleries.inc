<?php

function createTable($_db, $table_name) {

    $result = true;
    $sql = 'select * from `'.cfg::$prfx.$table_name.'` limit 0,1';
    try { $_db->_q($sql); }
    catch(PDOException $ex) { $result = false; }

    if (!$result) {
        // таблица не создана
        $sql  = '
                    CREATE TABLE `'.cfg::$prfx.$table_name.'`( 
                        `id` int(10) unsigned NOT NULL,
                        `created` int(11) NOT NULL,
                        `updated` int(11) NOT NULL,
                        `parent_id` int(11) NOT NULL,
                        `image` varchar(255) NOT NULL,
                        `title` varchar(255) NOT NULL,
                        `descr` varchar(255) NOT NULL,
                        `status` bit(1) NOT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
                    ';

        $sql1 = 'ALTER TABLE `'.cfg::$prfx.$table_name.'` ADD PRIMARY KEY (`id`);';
        $sql2 = 'ALTER TABLE `'.cfg::$prfx.$table_name.'` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;';

        $_db->_q($sql);
        $_db->_q($sql1);
        $_db->_q($sql2);
    }
}

$_db = new DBase();

$tmppath = 'galleries/_upld/files/';

$meta = Array(
    '_title' => 'Галерея - (АП)',
    '_keywords' => 'Галерея - (АП)',
    '_description' => 'Галерея - (АП)'
);
$tpl->addArray($meta);

$table = $router->getAction(2);
if ($table == 'checkb') {

    $id = $_POST['id'];
    $table = $_POST['table'] . '_gallery';
    $sql = "select * from `".cfg::$prfx.$table."` where `id`='$id' ";
    $sth = $_db->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    unset($rows);
    $row = $sth->fetch();

    $g = abs($row['gallery'] - 1);

    $sql = "update `".cfg::$prfx.$table."` set `gallery` = '$g' where `id`='$id' ";
    $sth = $_db->_q($sql);

    echo $g;
    exit;
} elseif ($table == 'cover') {

    $id = $_POST['id'];
    $parent_id = $_POST['parent_id'];
    $table = $_POST['table'] . '_gallery';

    $sql = "update `".cfg::$prfx.$table."` set `cover` = 0 where `parent_id` = $parent_id ";
    $_db->_q($sql);
    $sql = "update `".cfg::$prfx.$table."` set `cover` = 1 where `id` = $id ";
    $_db->_q($sql);

    echo 1;
    exit;
}

$table_name = $table . '_gallery';
createTable($_db, $table_name);

$_breadcrumbs .= '<li><a href="/'.cfg::$cms.'/'.$table.'/">'.$table.'</a></li>';
$_breadcrumbs .= '<li class="active">Галерея</li>';
$_telo .= '<h2>Галерея</h2><a href="/'.cfg::$cms.'/'.$table.'/">&laquo; вернуться</a><hr>';

if ($table != '') {
    $parent_id = round($router->getAction(3));
    if ($parent_id != 0) {

        $action = $router->getAction(4);
        if ($action == '') {

            @mkdir('galleries');
            @mkdir('galleries/' . $table);
            @mkdir('galleries/' . $table . '/' . $parent_id);
            $path = 'galleries/'.$table.'/'.$parent_id.'/';

            if (isset($_POST['galsbmt'])) {

                $files_array = explode(',',$_POST['files_array']);
                foreach($files_array as $key => $name) {
                    $filesarray[$key]['src'] = $name;
                    $filesarray[$key]['dist'] = $helper->createFolderName(str_replace('.jpg','',$name)).'.jpg';
                }
                foreach($filesarray as $img) {
                    copy($tmppath . $img['src'], $path . $img['dist']);
                }

                foreach($filesarray as $img) {

                    $_sql = "insert into `".cfg::$prfx.$table_name."` (`parent_id`,`title`,`image`) values (':parent_id',':title',':image');";

                    $sql = str_replace(':parent_id', $parent_id,
                           str_replace(':title', $img['dist'],
                           str_replace(':image', $img['dist'], $_sql)));

                    //echo $sql.'<br>';
                    $_db->_q($sql);

                }
                $ses->redirect('/'.cfg::$cms.'/galleries/'.$table.'/'.$parent_id.'/');
            }

            $sql = "select * from `".cfg::$prfx.$table_name."` where `parent_id`='$parent_id' ";
            $sth = $_db->_q($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            unset($rows);
            while($row = $sth->fetch()) $rows[] = $row;

            if (isset($rows)) {
                $_telo .= '<div class="row">';
                foreach($rows as $key => $img) {
                    $id = $img['id'];
                    $_telo .= '
                        <div class="col-sm-2" style="display: inline-block; margin-bottom: 20px;">
                            <a href="#" class="killimg" data-url="/'.cfg::$cms.'/galleries/'.$table.'/'.$parent_id.'/kill/'.$id.'">
                            <img src="/galleries/'.$table.'/'.$parent_id.'/'.$img['image'].'" alt="'.$img['title'].'" class="img-thumbnail">
                            </a>
                            <div id="checkbox_'.$id.'" style="height:20px;">
                                <input '.($img['gallery']==1?'checked':'').' type="checkbox" class="save-checkb" id="input-checkbox-'.$id.'" data-id="'.$id.'" data-table="'.$table.'" data-parent-id="'.$parent_id.'">&nbsp;
                                <label for="input-checkbox-'.$id.'" style="cursor:pointer;font-weight:normal;">в галерею</label>
                            </div>
                            <div id="spinner_'.$id.'" style="display:none;height:20px;">
                                <i class="fa fa-spin fa-spinner"></i>
                            </div>

                            <div id="cover_'.$id.'" style="height:20px;">
                                <input '.($img['cover']==1?'checked':'').'
                                        name="cover"
                                        type="radio"
                                        class="save-cover"
                                        id="input-cover-'.$id.'" data-id="'.$id.'" data-table="'.$table.'" data-parent-id="'.$parent_id.'">&nbsp;
                                <label for="input-cover-'.$id.'" style="cursor:pointer;font-weight:normal;">обложка</label>
                            </div>
                            <div id="cover_spinner_'.$id.'" style="display:none;height:20px;">
                                <i class="fa fa-spin fa-spinner"></i>
                            </div>
                        </div>
                    ';
                }
                $_telo .= '</div>';

                $_telo .= '
                <script>
                $(function(){
                    $("a.killimg").on("click", function(ev){
                        if (confirm("Удалить изображение?")) {
                            var url = $(this).data("url");
                            window.location.href = url;
                        }
                        ev.preventDefault();
                    });
                    
                    $(".save-checkb").on("click", function(){
                        var id = $(this).data("id"),
                            table = $(this).data("table"),
                            parent_id = $(this).data("parent-id"),
                            $this = $(this);
                        
                        $("#checkbox_" + id).hide();
                        $("#spinner_" + id).show();
                        
                        $.ajax({
                            url: "/'.cfg::$cms.'/galleries/checkb/",
                            type: "post",
                            dataType: "text",
                            data: { id:id, table:table, parent_id:parent_id },
                            success: function(data){
                                if (data == 1) {
                                    $this.prop("checked", true);
                                } else {
                                    $this.prop("checked", false);
                                }
                                $("#spinner_" + id).hide();
                                $("#checkbox_" + id).show();
                            }
                        });
                        
                    });
                    
                    $(".save-cover").on("click", function(){
                        var id = $(this).data("id"),
                            table = $(this).data("table"),
                            parent_id = $(this).data("parent-id"),
                            $this = $(this);
                        
                        $("#cover_" + id).hide();
                        $("#cover_spinner_" + id).show();
                        $.ajax({
                            url: "/'.cfg::$cms.'/galleries/cover/",
                            type: "post",
                            dataType: "text",
                            data: { id:id, table:table, parent_id:parent_id },
                            success: function(data){
                                console.log(data);
                                $("#cover_spinner_" + id).hide();
                                $("#cover_" + id).show();
                            }
                        });
                    });
                    
                });
                </script>
                ';
            } else {
                $_telo .= '<p>Нет данных</p>';
            }

            $_telo .= '
            <div class="row">
            <div class="col-md-2"><button class="btn btn-primary cover-option" data-parent-id="'.$parent_id.'">Настроить обложку</button></div>
            <div class="col-md-10" style="color:maroon;">Для удаления изображения кликните на него.<br><strong>Все изображения по умолчанию включены в слайдер</strong></div>
            </div>
            <hr>
                
            <script src="/core/plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
            <script src="/core/plugins/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
            <script src="/core/plugins/jQuery-File-Upload/js/jquery.fileupload.js"></script>
            <script src="/core/admin/tpl/js/gallery.js"></script>
    
            <p>Разрешается загружать jpg. Поддерживается множественная загрузка</p>
            <input id="fileupload" type="file" name="files[]" data-url="/galleries/_upld/" multiple accept="image/jpeg">
            <div id="progress">
                <i class="fa fa-spin fa-spinner"></i>
                <!-- div class="bar" style="width: 0%;"></div -->
            </div>
            <form action="" method="post" id="upld_images">
            <input type="hidden" name="galsbmt" value="1">
            <input type="hidden" id="files_array" name="files_array" value="" style="width: 100%;">
            </form>
            ';

        } elseif ($action == 'kill') {

            $id = round($router->getAction(5));
            $sql = "select * from `".cfg::$prfx.$table_name."` where (`parent_id`='$parent_id') and (`id`='$id') limit 0,1 ";
            $sth = $_db->_q($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $row = $sth->fetch();

            $sql = "delete from `".cfg::$prfx.$table_name."` where (`parent_id`='$parent_id') and (`id`='$id') ";
            $_db->_q($sql);
            $file = 'galleries/'.$table.'/'.$parent_id.'/'.$row['image'];
            @unlink($file);
            $ses->redirect('/'.cfg::$cms.'/galleries/'.$table.'/'.$parent_id.'/');

        }
    }
}

$_telo .= '
<div class="modal fade" id="coverOptions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <iframe src="" height="600" style="width: 100%;" id="lframe" frameborder="0"></iframe>
        </div>
    </div>
</div>
<script>
    $(function() {
        $(".cover-option").on("click", function(ev){
            var parent_id = $(this).data("parent-id");
            $("#coverOptions").modal();
            $("#lframe").attr("src","/'.cfg::$cms.'/lf?parent_id=" + parent_id);
            ev.preventDefault();
        });
    });
    
    function closeCoverOption() {
        $("#coverOptions").modal("hide");
    }
</script>
';