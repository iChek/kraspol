<?php
$tpl->indexPage('empty');
$data = Array();

$json = isset($_POST['json']);

if (isset($_POST['table']) && isset($_POST['field']))
{
    //echo $helper->aprn($_POST); exit;
    $table = $_POST['table'];
    $field = $_POST['field'];
    //##if (isset($_POST['fields'])) $fields = explode(',',$_POST['fields']);
    
    if (isset($_POST['vals'])) { $tvals = explode(',',$_POST['vals']); foreach($tvals as $vk => $vv) $vals[$vv] = $vv; }
    else $vals = Array();

    $t = new Table($table);

    $fs = $t->tableConfig->fields;

    foreach($fs as $k => $v)
    {
        if ($v->name == $field && $v->type == 'multi')
        {
            $f = $v;
            break;
        }
    }

    if (isset($f))
    {
        //$v;
        $tname = $v->name;
        $st = new Table($tname);
        $rs = $st->getAllRows();
        foreach($rs as $r => $v)
        {
            $data[] = Array('id'=>$v['id'],'name'=>$v['title']);
        }
        $result = 1;
        $message = "";
    }
    else
    {
        $result = 0;
        $message = "Таблица/поле не найдено";
    }
}
else
{
    $result = 0;
    $message = "Ошибка данных";
}

if ($json) $_telo = '{"result":'.$result.',"message":"'.$message.'","data":'.json_encode($data,JSON_UNESCAPED_UNICODE).'}';
else
{
    if ($result == 0)
    {
        $_telo .= $message;
    }
    else
    {
        foreach($data as $dk => $dv)
        {
            $id = 'checkbox_'.$table.'_'.$dv['id'];
            $_telo .= '<input type="checkbox" name="'.$dv['name'].'" id="'.$id.'" value="'.$dv['id'].'" class="checkbox_'.$table.'"';
            if (isset($vals[$dv['id']])) $_telo .= ' checked';
            $_telo .= '>';
            $_telo .= '<label for="'.$id.'" style="cursor:pointer;">&nbsp;'.$dv['name'].'</label><br>';
        }
    }
}