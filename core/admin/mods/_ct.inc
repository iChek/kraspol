<?php

$_telo .= '<a href="/'.cfg::$cms.'/_cs/">конструктор справочников</a><h1>Конструктор таблиц</h1>';

$action = $router->getAction(2);
if ($action == '') {

    // отображение всех таблиц
    unset($items);
    $dr = opendir('core/db');
    while($fl = readdir($dr)) {
        if ($fl != '.' && $fl != '..') {
            $bf = file_get_contents('core/db/' . $fl);
            $tt = json_decode($bf);
            $title = $tt->title;
            $name = $tt->name;
            $items[$title] = $tt;
        }
    }
    ksort($items);

    $num = 1;
    $_telo .= '
    <hr>
    <a href="/'.cfg::$cms.'/_ct/add/">Добавить таблицу</a>
    <hr><table class="table table-striped table-condensed table-hover">
        <tr>
            <th>#</th>
            <th style="width:100%;">Название (таблица)</th>
        </tr>
    ';
    foreach($items as $title => $table) {
        $_telo .= '<tr>';
        $_telo .= '<td>'.$num.'</td>';
        $_telo .= '<td>'.$title.' ('.$table->name.')</td>';
        $_telo .= '</tr>';
        $num++;
    }
    $_telo .= '</table>';

} elseif ($action == 'add') {

    $_telo .= '<h2>Добавление таблицы</h2>';

    if (isset($_POST['addsbmt'])) {
        echo $helper->aprn($_POST);
        exit;
    }

    $_telo .= '
    <form action="" method="post" class="form-horizontal">
        <input type="hidden" name="addsbmt" value="1">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Название</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title" name="title" placeholder="Читабельное название таблицы">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Имя таблицы</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="Имя таблицы">
            </div>
        </div>
        <div class="form-group">
            <label for="access" class="col-sm-2 control-label">Доступ к таблице</label>
            <div class="col-sm-10">
                <input type="radio" name="access" value="0" checked> нет
                <input type="radio" name="access" value="1"> да
            </div>
        </div>
        <div class="form-group">
            <label for="nonew" class="col-sm-2 control-label">Добавление новых записей</label>
            <div class="col-sm-10">
                <input type="radio" name="access" value="0"> нет
                <input type="radio" name="access" value="1" checked> да
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <label>
                    <input type="hidden" name="gosprav" value="0">
                    <input type="checkbox" name="gosprav" value="1"> Перейти в справчник
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Создать</button>
            </div>
        </div>
    </form>
    ';

}