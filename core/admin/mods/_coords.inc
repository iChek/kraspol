<?php

$linktypes = Array( 0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");

$c = new Table('objects_contacts');

if (isset($_POST['subm_linkas'])) {

    $id = $_POST['id'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];

    unset($_re);
    $_re['id'] = $id;
    $_re['latitude'] = $lat;
    $_re['longitude'] = $lng;
    $c->updateRow($_re);
    echo 'good';
    exit;
}

$o = new Table('objects');
//$o->setOptions(Array('where' => " (`title` like '%дом%') "));
$items = $o->getAllRows();
if ($items) {

    $_telo .= '<table class="table">';
    foreach ($items as $item) {

        $object_id = $item['id'];
        $c->setOptions(Array('where' => " (`object_id` = '$object_id') "));
        $contacts = $c->getAllRows();

        if ($contacts) {
            foreach($contacts as $contact) {
                $_telo .= '<tr>';
                $_telo .= '<td>'.$item['title'].'</td>';
                $_telo .= '<td>'.$contact['address'].'</td>';
                $_telo .= '<td><input type="text" name="latitude" id="latitude_'.$contact['id'].'" value="'.$contact['latitude'].'"></td>';
                $_telo .= '<td><input type="text" name="longitude" id="longitude'.$contact['id'].'" value="'.$contact['longitude'].'"></td>';
                $_telo .= '<td><button type="button" data-id="'.$contact['id'].'" class="btn btn-default btn-xs get-coords" data-address="'.$contact['address'].'">получить</button></td>';
                $_telo .= '<td><button type="button" data-id="'.$contact['id'].'" class="btn btn-default btn-xs save-row">сохранить</button></td>';
                $_telo .= '</tr>';
            }
        }
    }
    $_telo .= '</table>';
}

$_telo .= '
<script>
    $(function(){
        
        $(".get-coords").on("click", function(){
            var $this = $(this),
                address = $(this).data("address"),
                id = $(this).data("id"),
                url = "http://maps.google.com/maps/api/geocode/json?address=" + address;
            
            $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");

            $.ajax({
                url: url,
                type: "get",
                success: function(data) {
                    $("#latitude_" + id).val(data.results[0].geometry.location.lat);
                    $("#longitude" + id).val(data.results[0].geometry.location.lng);
                    $this.html("получено");
                }
            })
        });
        
        $(".save-row").on("click", function(){
            var id = $(this).data("id");
            var lat = $("#latitude_"+id).val();
            var lng = $("#longitude"+id).val();
            var $this = $(this);
            
            $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");
            
            $.ajax({
                url:  "/control/_coords",
                type: "post",
                data: { subm_linkas:1, id:id, lat:lat, lng:lng },
                dataType: "text",
                success: function(data) {
                    console.log(data);
                    if (data == "good") {
                        $this.html("сохранено");                        
                    }
                }
                
            });
        });
    })
</script>

';