<?php

$mr = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
    'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е',
    'Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ',
    'Ъ','Ы','Ь','Э','Ю','Я',' ','№',',');
$me = array(
    'a','b','v','g','d','e','yo','zh','z','i','i','k','l','m','n','o','p',
    'r','s','t','u','f','h','ts','ch','sh','sch','','y','','e','u','ya',
    'a','b','v','g','d','e','yo','zh','z','i','i','k','l','m','n','o','p',
    'r','s','t','u','f','h','c','ch','sh','sch','','y','','e','u','ya',
    '_','N','');
function iz_get_extention($fname)
{
    $ext = substr($fname,strrpos($fname,'.'),100);
    return $ext;
}
function iz_r2e($str)
{
    global $mr,$me;
    $str2=$str;
    for ($i=0;$i<count($mr);$i++) $str2 = str_replace($mr[$i],$me[$i],$str2);
    return strtolower($str2);
}
function iz_rus2folder($fstr)
{
    $tmp = iz_r2e($fstr);
    $tmp = preg_replace('/[^\w\d]/','',$tmp);
    return $tmp;
}
function iz_GetImgResize($mx,$my,$nx,$ny,&$gx,&$gy)
{
    $x = round(($mx*$ny)/$my);
    $y = round(($my*$nx)/$mx);
    if ($x<=$nx) { $gx=$x; $gy=$ny; }
    else { $gx=$nx; $gy=$y; }
}
function iz_ImageResize($img1,$img2,$x,$y,$ext)
{
    if ($ext=='.jpg' || $ext=='.jpeg')
    {
        $im1 = imagecreatefromjpeg($img1);
        $im2 = imagecreatetruecolor($x,$y);
        $ws = getimagesize($img1);
        imagecopyresampled($im2, $im1, 0, 0, 0, 0, $x, $y, $ws[0], $ws[1]);
        imagejpeg($im2,$img2,100);
        return true;
    }
    if ($ext=='.gif')
    {
        $im1 = imagecreatefromgif($img1);
        $im2 = imagecreatetruecolor($x,$y);
        $ws = getimagesize($img1);
        imagecopyresampled($im2, $im1, 0, 0, 0, 0, $x, $y, $ws[0], $ws[1]);
        imagegif($im2,$img2);
        return true;
    }
    elseif ($ext=='.png')
    {
        $im1 = imagecreatefrompng($img1);
        $im2 = imagecreatetruecolor($x,$y);
        $ws = getimagesize($img1);
        imagecopyresampled($im2, $im1, 0, 0, 0, 0, $x, $y, $ws[0], $ws[1]);
        imagepng($im2,$img2);
        return true;
    }
    else return false;
}

$data = Array(
    'news' => Array(
        'field' => 'image',
        'path' => 'userfiles/moduls/news/image/'
    ),
    'objects' => Array(
        'field' => 'logo',
        'path' => 'userfiles/moduls/objects/logo/'
    )
);

$_breadcrumbs .= '<li class="active">Кадрирование изображения</li>';
$_telo .= '<h2>Кадрирование изображения</h2><hr>';

$activity = $router->getAction(2);
$id = round($router->getAction(3));

if ($activity == '' || $id == 0) {
    $ses->redirect("/".cfg::$cms."/");
}

$t = new Table($activity);
$item = $t->getRowByID($id);
if ($item) {

    $_telo .= '<link rel="stylesheet" type="text/css" href="/core/plugins/jquery.imgareaselect/css/imgareaselect-default.css">';
    $_telo .= '<script type="text/javascript" src="/core/plugins/jquery.imgareaselect/scripts/jquery.imgareaselect.min.js"></script>';

    $imgpath = $data[$activity]['path'];
    $field = $data[$activity]['field'];
    $image = $item[$field];

    if ($image != '') {

        if (isset($_POST['resizefoto'])) {
            unset($_re);
            $_re['id'] = $id;
            $_re['x1'] = $_POST['x1'];
            $_re['x2'] = $_POST['x2'];
            $_re['y1'] = $_POST['y1'];
            $_re['y2'] = $_POST['y2'];

            $t->updateRow($_re);
            $_telo .= '<div class="alert alert-success">Сохранено!</div>';
        } else {
            $ax = 150;
            $ay = 150;
            $im = getimagesize($imgpath . $image);

            if ($im[0] > $im[1]) {
                $msize = $im[1];
                $tsize = 'height';
            } else {
                $msize = $im[0];
                $tsize = 'width';
            }

            $kaka = 100;
            if ($im[0] < $im[1]) {
                $kaka = 50;
            }

            $_telo .= '
            <script>
                    
                function preview(img, selection)
                {
                    if (!selection.width || !selection.height) return;
                    
                    var ss = "";
                    ss += "X1: " + selection.x1 + " x Y1: " + selection.y1 + "<br>";
                    ss += "W: " + selection.width + " x H: " + selection.height + "<br>";
                    
                    var scaleX = ' . $ax . ' / selection.width;
                    var scaleY = ' . $ay . ' / selection.height;
        
                    ss += "sx: " + scaleX + " x xy: " + scaleY + "<br>";
                    
                    $("#preview img").css({
                        width: Math.round(scaleX * ' . $im[0] . '),
                        height: Math.round(scaleY * ' . $im[1] . '),
                        marginLeft: -Math.round(scaleX * selection.x1),
                        marginTop: -Math.round(scaleY * selection.y1)
                    });
                    
                    $("#x1").val(selection.x1);
                    $("#y1").val(selection.y1);
                    $("#x2").val(selection.x2);
                    $("#y2").val(selection.y2);
                    $("#w").val(selection.width);
                    $("#h").val(selection.height);
                    
                    $("#debug").html(ss);
                }
                    
                $(function () {
                    $("#photo").imgAreaSelect({
                        x1: 0, y1: 0, x2: ' . $msize . ', y2: ' . $msize . ',
                        aspectRatio: "1:1",
                        handles: true,
                        onSelectChange: preview
                    });
                });
            </script>
            <div id="blockphoto" style="float:left;margin-right:10px;">
                <img id="photo" src="/' . $imgpath . $image . '" alt="" title="">
            </div>
            
            <div class="previewramka">
                <div id="preview" style="width:' . $ax . 'px; height:' . $ay . 'px; overflow:hidden; float:left; margin-right:15px;">
                    <img src="/' . $imgpath . $image . '" ' . $tsize . '="100%" />
                </div>
        
            
                <form action="" method="post">
                    <input type="hidden" name="resizefoto" value="1">
                    
                    <input type="hidden" name="x1" id="x1" value="0" size="3">
                    <input type="hidden" name="y1" id="y1" value="0" size="3">
                    <input type="hidden" name="x2" id="x2" value="' . $msize . '" size="3">
                    <input type="hidden" name="y2" id="y2" value="' . $msize . '" size="3">
                    <input type="hidden" name="w" id="w" value="' . $msize . '" size="3">
                    <input type="hidden" name="h" id="h" value="' . $msize . '" size="3">
                    
                    <div id="debug" style="display:none;">#</div>
                    
                    <button type="submit" class="btn-self">сохранить</button>
                    <p>Выделять область слева на фотографии и передвигать её можно с помощью мыши</p>
                </form>
            </div>
            ';
        }
    } else {
        $_telo .= '<p>Изображение пустое</p>';
    }

} else {
    $_telo .= 'no';
    //$ses->redirect("/".cfg::$cms."/");
}
