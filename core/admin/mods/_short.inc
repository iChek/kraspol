<?php
$linktypes = Array( 0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");

$_breadcrumbs .= '<li class="active">Формирование краткого описания</li>';
$_telo .= '<h2>Формирование краткого описания</h2><hr>';

$_telo .= '<p>Вырезание краткого описания из полного описания</p>';

$action = $router->getAction(2);
$o = new Table('objects');

if ($action == '') {

    $o->setOptions(Array('order' => ' `title` '));
    $objs = $o->getAllRows();
    if ($objs) {

        $_telo .= '
        <p><i class="fa fa-file-o"></i> - описание есть</p>
        <p><i class="fa fa-ban"></i> - описания нет</p>
        <table class="table table-striped table-hover">
        <tr>
            <th>вырезать</th>
            <th>символов</th>
            <th>краткое</th>
            <th>полное</th>
            <th>изменить</th>
            <th>название</th>
        </tr>
        '; //table-condensed">';
        foreach ($objs as $key => $obj) {

            $_telo .= '<tr>';
            $_telo .= '<td><button class="btn btn-xs cute" data-id="' . $obj['id'] . '">вырезать</button></td>';
            $_telo .= '<td><input type="text" value="600" id="symb_' . $obj['id'] . '" size="3"></td>';
            $_telo .= '<td><i class="fa fa-'.(trim($obj['short']) != '' ? 'file-o' : 'ban' ).'"></i></td>';
            $_telo .= '<td><i class="fa fa-'.(trim($obj['descr']) != '' ? 'file-o' : 'ban' ).'"></i></td>';
            $_telo .= '<td><a href="/' . cfg::$cms . '/objects/' . $obj['id'] . '/edit/" target="_blank" class="btn btn-xs"><i class="fa fa-pencil"></i></a></td>';
            //$_telo .= '<td style="width: 100%;"><a href="/' . $linktypes[$obj['linktype']] . '/' . $obj['folder'] . '" target="_blank">' . $obj['title'] . '</a></td>';
            $_telo .= '<td style="width: 100%;">' . $obj['title'] . '</td>';
            $_telo .= '</tr>';

        }
        $_telo .= '</table>';

        $_telo .= '
        <script>
        $(function(){
        
            $(".cute").on("click", function(){
                
                var $this = $(this);
                var id = $(this).data("id");
                var symb = $("#symb_" + id).val();
                var url = "/'.cfg::$cms.'/_short/cute/?id=" + id + "&symb=" + symb;
                console.log(url);
                $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");
                $.getJSON(url, function(resp){
                    $this.html("вырезано!");
                });
                
            });
        
        });
        </script>
        ';

    }

} elseif ($action == 'cute') {

    $id = round($_GET['id']);
    $symb = round($_GET['symb']);
    $obj = $o->getRowByID($id);
    if ($obj) {

        $descr = strip_tags($obj['descr']);
        $txt = '';
        $atxt = explode(' ', $descr);
        foreach($atxt as $word) {
            if (strlen($txt.' '.$word) <= $symb) {
                $txt .= ' '.$word;
            } else {
                break;
            }
        }

        if (strlen($descr) > $txt) {
            $txt .= ' ... ';
        }

        unset($_re);
        $_re['id'] = $obj['id'];
        $_re['short'] = trim($txt);
        $o->updateRow($_re);
        echo '{"result":true}';
    } else {
        echo '{"result":false}';
    }
    exit;

}