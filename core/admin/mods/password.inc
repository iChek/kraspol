<?php

$id = round($router->getAction(2));

$_breadcrumbs .= '<li><a href="'.DS.cfg::$cms.DS.'users'.DS.'">Пользователи</a></li>';
$_breadcrumbs .= '<li class="active">Генерация пароля</li>';
$_telo .= '<h2>Генерация пароля</h2><hr>';

$us = new Table('users');
$u = $us->getRowById($id);

if ($u)
{
    if (isset($_POST['passsubmit']))
    {
        $pass = $helper->genpassw(8);
        $u['password'] = md5($pass);
        $us->updateRow($u);
        
        $email = $u['email'];
        
        /*
        // SMS
        $telefon = preg_replace('/\W/','',$u['phone']);
        $smsmsg = 'Новые данные. Логин:'.$email.' Пароль:'.$pass;
        $smsmsg2 = 'Новые данные. Логин:'.$u['email'].' Пароль:********';
        $smsresult = send_sms($telefon, $smsmsg);
        saveTechSMS($telefon."\n".$smsmsg2."\n".print_r($smsresult,true));
        // SMS
        */
        
        $msg  = '';
        $msg .= '<h3>Добрый день!</h3>';
        $msg .= '<p>Ваши данные для входа в личный кабинет:</p>';
        $msg .= '<p>Логин: '.$email.'</p>';
        $msg .= '<p>Пароль: '.$pass.'</p>';
        $msg .= '<p>Вы можете поменять пароль в личном кабинете участника</p>';
        $msg .= '<p>Успешных продаж!</p>';
        $msg .= '<p>С уважением, ваш '.$_o_sitename.'</p>';
        
        $eml = new Email();
        $eml->to = $email;
        $eml->subj = 'Новый пароль на сайте "'.$_o_sitename.'"';
        $eml->body = $msg;
        $eml->send(true);
        
        $_telo .= $helper->successMsg('Новый пароль сгенерирован и отправлен пользователю');
    }
    
    $_telo .= '<h3>'.$u['title'].' '.$u['name'].' '.$u['sname'].'</h3>';
    $data['returnlink'] = DS.cfg::$cms.DS.'users'.DS;
    $_telo .= $tpl->loadHTML('users/password',$data);
}
else
{
    $_telo .= $helper->errorMsg('Пользователь не найден');
    $_telo .= '<p><a href="'.DS.cfg::$cms.DS.'users'.DS.'">&laquo; вернуться</a></p>';
}