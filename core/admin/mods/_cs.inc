<?php

$_tpl = '
{
    "title":"#title#",
    "name":"#name#",
    "options":{
        "access": 1,
        "sort": " `title` "
    },
    "fields":[
        {
            "title": "Название",
            "name": "title",
            "type": "string",
            "req": 1,
            "showtable": 1
        },
        {
            "title":"Папка",
            "name":"folder",
            "type":"folder",
            "showtable":1,
            "inputText":"Папка для создания ЧПУ. Можно не вводить, будет создано автоматически из наименования."
        }
    ]
}
';

$_telo .= '<a href="/'.cfg::$cms.'/_ct/">конструктор таблиц</a><h1>Конструктор справочников</h1>';

if (isset($_POST['spsubmt'])) {

    //##echo $helper->aprn($_POST);
    $title = $_POST['title'];
    $name = 'sprav_'.$_POST['name'];
    $fname = 'core/db/'.$name.'.cfg';
    $values = explode("\n",trim($_POST['values']));
    //##echo $helper->aprn($values);
    
    $_tpl = str_replace('#title#', $title, str_replace('#name#', $name, $_tpl));

    $fp = fopen($fname, 'w');
    fwrite($fp, $_tpl);
    fclose($fp);

    $sprav = new Table($name, false);
    foreach($values as $key => $val) {

        $val = trim($val);
        if ($val != '') {
            $folder = $helper->createFolderName($val);
            $sprav->setOptions(Array('where' => " `folder`='$folder' "));
            $row = $sprav->getRow();
            if (!$row) {
                unset($_re);
                $_re['title'] = $val;
                $_re['folder'] = $folder;
                $sprav->saveRow($_re, $e);
            }
        }
    }

    if ($_POST['gosprav'] == 1) {
        $url = '/'.cfg::$cms.'/'.$name.'/';
    } else {
        $url = '/'.cfg::$cms.'/_cs/';
    }

    $m = new Table('moduls');
    $folder = $helper->createFolderName($title);
    $m->setOptions(Array('where' => " `sysname`='$folder' "));
    $row = $m->getRow();
    if (!$row) {
        unset($_re);
        $_re['title'] = '/Справочник/ '.$title;
        $_re['sysname'] = $folder;
        $m->saveRow($_re, $e);
    }

    $ses->redirect($url);

    exit;

}

$_telo .= '
<form action="" method="post" class="form-horizontal">
    <input type="hidden" name="spsubmt" value="1">
    <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Название справочника</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" placeholder="Название справочника">
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Имя таблицы</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Имя таблицы">
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Значения построчно</label>
        <div class="col-sm-10">
            <textarea rows="15" cols="60" class="form-control" id="values" name="values" placeholder="Значения, каждое значение с новой строки"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <label>
                <input type="hidden" name="gosprav" value="0">
                <input type="checkbox" name="gosprav" value="1"> Перейти в справчник
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Создать</button>
        </div>
    </div>
</form>
';