<?php

$type = $_POST['type'];
if ($type == 'flag') {

    $array = Array(
        'yes' => "<i class='fa fa-check-square-o' style='color:green;'></i>",
        'no' => "<i class='fa fa-square-o' style='color:maroon;'></i>",
        'plus' => "<i class='fa fa-plus' style='color:green;'></i>",
        'minus' => "<i class='fa fa-minus' style='color:maroon;'></i>"
    );

    $table = $_POST['table'];
    $field = $_POST['field'];
    $id = round($_POST['id']);
    $str = $_POST['str'];

    $t = new Table($table);
    $row = $t->getRowByID($id);
    if ($row) {

        unset($upd);
        $upd['id'] = $id;
        $upd[$field] = abs($row[$field] - 1);
        $t->updateRow($upd);
        if ($upd[$field] == 1) {
            $result = $array['yes'];
        } elseif ($upd[$field] == 0) {
            $result = $array['no'];
        }

        echo '{"result":true, "html": "'.$result.'" }';
    } else {
        echo '{"result":false, "html": "'.$array[$str].'" }';
    }

}

exit;