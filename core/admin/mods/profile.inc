<?php $ses->adminAuth();
// Модуль управления администраторами

$meta = Array('_title' => 'Профиль - (АП)',
              '_keywords' => 'Профиль - (АП)',
              '_description' => 'Профиль - (АП)'
                );
$tpl->addArray($meta);

$_breadcrumbs .= '<li class="active">Профиль</li>';

$_telo .= '<h2>Профиль</h2><hr>';

$tabs = 'admins'; if ($security->isCurator) $tabs = 'curators';

$admins = new Table($tabs);

$action = ''; if (isset($router->path[2])) $action = $router->path[2];

if ($action == '')
{
    $row = $admins->getRowById($user_id);
    //##$_telo .= $user_id.$helper->aprn($row);
    
    if (isset($_POST['profilesubmit']))
    {
        //##$_telo .= $helper->aprn($_POST);
        //$title = $_POST['title'];
        //$email = $_POST['email'];
        //$login = $_POST['login'];
        
        $_err = '';
        //if ($title == '') $_err .= 'Заполните имя<br>';
        //if ($login == '') $_err .= 'Заполните логин<br>';
        //if ($email == '') $_err .= 'Заполните e-mail<br>';
        
        $password0 = $_POST['password0'];
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];
        
        $changePasswor = true;
        if ($password0 != '')
        {
            if ($password1 == '' || $password2 == '') $_err .= 'Не заполнен новый пароль<br>';
            elseif ($password1 != $password2) $_err .= 'Новые пароли не совпадают<br>';
            else
            {
                if (md5($password0) != $row['password']) $_err .= 'Текущий пароль неверный<br>';
            }
        }
        else
        {
            $changePasswor = false;
        }
        
        if (trim($_err)!='')
        {
            $_errortelo .= $helper->errorMsg($_err);
        }
        else
        {
            //$row['title'] = $title;
            //$row['email'] = $email;
            //$row['login'] = $login;
            if ($changePasswor) $row['password'] = md5($password1); else unset($row['password']);
            $admins->updateRow($row);
            $_errortelo .= $helper->successMsg('Данные обновлены');
        }
        $tpl->addArray($_POST);
    }
    
    if (!isset($_POST['profilesubmit']))
    {
        $tpl->addArray($row);
    }
    $_telo .= $helper->printForm('profile', $row);
    
    //$_telo .= $helper->aprn($row);
}