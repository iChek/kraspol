<?php

$client = new Table('clients');
$advert = new Table('adv');

//$client->_q('truncate table `'.cfg::$prfx.'clients` ');
//$advert->_q('truncate table `'.cfg::$prfx.'adv` ');

function getClientId($row, $c) {

    global $client;
    $client_id = 0;
    // получаем клиент ID
    unset($cli);
    $fio = trim($row[$c->fio]);
    $phone = trim($row[$c->phone]);
    $email = trim($row[$c->email]);
    if ($fio != '' || $phone != '' || $email != '') {

        $client->setOptions(Array('where' => " (`fio`='$fio') and (`phone`='$phone') and (`email` = '$email') "));
        $tmp = $client->getRow();
        if (!$tmp) {
            $cli['title'] = trim($fio) != '' ? $fio : $phone . ' '. $email;
            $cli['fio'] = $fio;
            $cli['phone'] = $phone;
            $cli['email'] = $email;
            $cli['klfio'] = $fio;
            $cli['klphone'] = $phone;
            $cli['klemail'] = $email;
            $client->saveRow($cli, $_e);
            $client_id = $client->lastInsertId;
        } else {
            $client_id = $tmp['id'];
        }
    }
    return $client_id;

}

function getSpravId($sprav_name, $val) {

    $val = trim($val);
    if ($val == '') return 0;

    $t = new Table($sprav_name);
    $sprav_id = 0;

    $t->setOptions(Array('where' => " `title` = '$val' "));
    $row = $t->getRow();
    if ($row) {
        return $row['id'];
    } else {
        die( '<p style="color:red;">'.$sprav_name.' - '.$val.'</p>' );
    }

}

function getPubType($pubtype) {

    if ($pubtype == 'платное') {
        return 1;
    } elseif ($pubtype == 'бесплатное') {
        return 2;
    } elseif ($pubtype == '') {
        return 0;
    } else {
        die ('Pub type: '. $pubtype);
    }

}

$tnames = Array(
//    'hotels' => 1,
//    'restaurants' => 2,
//    'transports' => 3,
//    'activitysummer' => 5,
    'activitywinter' => 6,
//    'activitychildren' => 10,
//    'beauties' => 4,
//    'services' => 8,
//    'enterts' => 7,
//    'shops' => 9
);

$titles = Array(
    'ОТЕЛИ' => 'hotels',
    'РЕСТОРАНЫ' => 'restaurants',
    'ТРАНСПОРТ' => 'transports',
    'АКТИВНЫЙ ОТДЫХ ЛЕТО' => 'activitysummer',
    'АКТИВНЫЙ ОТДЫХ ЗИМА' => 'activitywinter',
    'ОТДЫХ С ДЕТЬМИ' => 'activitychildren',
    'КРАСОТА И ЗДОРОВЬЕ' => 'beauties',
    'УСЛУГИ' => 'services',
    'РАЗВЛЕЧЕНИЯ' => 'enterts',
    'МАГАЗИНЫ' => 'shops'
);

$clients = Array(
    'hotels'           => Array( 'fio' => 18, 'phone' => 19, 'email' => 20 ),
    'restaurants'      => Array( 'fio' => 17, 'phone' => 18, 'email' => 19 ),
    'transports'       => Array( 'fio' => 10, 'phone' => 11, 'email' => 12 ),
    'activitysummer'   => Array( 'fio' => 19, 'phone' => 20, 'email' => 21 ),
    'activitywinter'   => Array( 'fio' => 20, 'phone' => 21, 'email' => 22 ),
    'activitychildren' => Array( 'fio' => 11, 'phone' => 12, 'email' => 13 ),
    'beauties'         => Array( 'fio' => 10, 'phone' => 11, 'email' => 12 ),
    'services'         => Array( 'fio' =>  9, 'phone' => 10, 'email' => 11 ),
    'enterts'          => Array( 'fio' => 10, 'phone' => 11, 'email' => 12 ),
    'shops'            => Array( 'fio' => 11, 'phone' => 12, 'email' => 13 ),
);

$adv = Array(
    'hotels'           => Array('pubtype' => 16, 'pubcategory' => 17),
    'restaurants'      => Array('pubtype' => 15, 'pubcategory' => 16),
    'transports'       => Array('pubtype' =>  8, 'pubcategory' =>  9),
    'activitysummer'   => Array('pubtype' => 17, 'pubcategory' => 18),
    'activitywinter'   => Array('pubtype' => 18, 'pubcategory' => 19),
    'activitychildren' => Array('pubtype' => 16, 'pubcategory' => 17),
    'beauties'         => Array('pubtype' => 16, 'pubcategory' => 17),
    'services'         => Array('pubtype' => 16, 'pubcategory' => 17),
    'enterts'          => Array('pubtype' => 16, 'pubcategory' => 17),
    'shops'            => Array('pubtype' => 16, 'pubcategory' => 17)
);

$fields = Array(
    'hotels' => Array(
        'title' => 0,
        'sprav_hoteltype^hoteltype_id' => 2,
        'sprav_territory^territory_id' => 1,
        'sprav_distance_cabcar^distancecabcar_id' => 6,
        'sprav_price^summerprice_id' => 8,
        'sprav_price^winterprice_id' => 9,
        'booking' => 3,
        'address' => 4,
        'phone' => 5,
        'site' => 7,
        'is^brekfast' => 10,
        'is^wifi' => 11,
        'is^transfer' => 12,
        'is^parking' => 13,
        'is^sauna' => 14,
        'descr' => 24
    ),
    'restaurants' => Array(
        'title' => 0,
        'sprav_resttype^resttype_id' => 1,
        'sprav_territory^territory_id' => 2,
        'sprav_middle_check^middlecheck_id' => 8,
        'sprav_kitchen^sprav_kitchen' => 10,
        'address' => 3,
        'phone' => 4,
        'site' => 5,
        'is^alltime' => 0,
        'is^delivery' => 6,
        'is^childroom' => 11,
        'is^terrace' => 13,
        'descr' => 23
    ),
    'transports' => Array(
        'title' => 0,
        'sprav_trnasport_type^trnasporttype_id' => 1,
        'sprav_territory^territory_id' => 2,
        'is^transfer' => 3,
        'address' => 4,
        'phone' => 5,
        'site' => 6
    ),
    'activitysummer' => Array(
        'title' => 0,
        'sprav_territory^territory_id' => 2,
        'address' => 3,
        'phone' => 1,
        'site' => 4,
        'is^ekipir' => 5,
        'is^gora' => 6,
        'is^splav' => 7,
        'is^moto' => 8,
        'is^lesopedy' => 9,
        'is^konyaki' => 10,
        'is^skalolaz' => 11,
        'is^yoga' => 12,
        'is^ekskurs' => 13,
        'descr' => 26
    ),
    'activitywinter' => Array(
        'title' => 0,
        'sprav_territory^territory_id' => 2,
        'address' => 3,
        'phone' => 1,
        'site' => 4,
        'is^ekipir' => 5,
        'is^lyzhi' => 6,
        'is^snowboard' => 7,
        'is^freeride' => 8,
        'is^backcountry' => 9,
        'is^healski' => 10,
        'is^skitour' => 11,
        'is^childschool' => 12,
        'is^yoga' => 13,
        'is^ekskurs' => 14,
        'descr' => 27
    ),
    'activitychildren' => Array(
        'title' => 0,
        'sprav_territory^territory_id' => 2,
        'sprav_children_activity^activity_id' => 5,
        'address' => 3,
        'phone' => 1,
        'site' => 4,
        'descr' => 17
    ),
    'beauties' => Array(
        'title' => 0,
        'sprav_territory^territory_id' => 2,
        'sprav_beautytype^beautytype_id' => 1,
        'address' => 3,
        'phone' => 4,
        'site' => 5,
        'descr' => 17
    ),
    'services' => Array(
        'title' => 0,
        'sprav_servicetype^servicetype_id' => 1,
        'sprav_territory^territory_id' => 2,
        'address' => 3,
        'phone' => 4,
        'site' => 5,
        'descr' => 15
    ),
    'enterts' => Array(
        'title' => 0,
        'sprav_enterttype^enterttype_id' => 1,
        'sprav_territory^territory_id' => 2,
        'sprav_agetype^agetype_id' => 7,
        'address' => 3,
        'phone' => 4,
        'site' => 5,
        'descr' => 16
    ),
    'shops' => Array(
        'title' => 0,
        'sprav_shoptype^shoptype_id' => 1,
        'sprav_territory^territory_id' => 2,
        'address' => 3,
        'phone' => 4,
        'site' => 5,
        'descr' => 18
    )
);