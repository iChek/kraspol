<?php

$_breadcrumbs .= '<li class="active">Показать клиента</li>';
$_telo .= '<h2>Показать клиента</h2><hr>';

$id = round($router->getAction(2));

$o = new Table('objects');
$c = new Table('clients');

$obj = $o->getRowByID($id);
if ($obj) {

    $clinet_id = round($obj['client_id']);
    $client = $c->getRowByID($clinet_id);
    if ($client) {

        $ses->redirect('/'.cfg::$cms.'/clients/'.$clinet_id);

    } else {

        $_telo .= '<div class="alert alert-info">Не найден клиент с ID: '.$clinet_id.'</div>';

    }

} else {

    $_telo .= '<div class="alert alert-danger">Не верный ID объекта</div>';

}