<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$ses->adminAuth();

$modulname = $router->modpage;
if (!$security->isAccess($modulname)) {
    $_telo .= '<p><b style="color:red;">Доступ запрещён</b></p>';
} else {

    $_showRowsArray = Array(10 => 10, 20 => 20, 50 => 50, 100 => 100, 'all' => 'Все');

    $tableName = 'tables';
    if (isset($router->modpage))
        $tableName = $modulname;
    if (isset($staticTable))
        $tableName = $staticTable;

    $pth = 'userfiles';
    @mkdir($pth);
    $pth .= '/moduls';
    @mkdir($pth);
    $pth .= '/' . $tableName;
    @mkdir($pth);

    $meta = Array('_title' => 'Структура сайта - (АП)',
        '_keywords' => 'Структура сайта - (АП)',
        '_description' => 'Структура сайта - (АП)'
    );
    $tpl->addArray($meta);

    //##$debug .= $hel per->aprn($ses['subtable']);

    $newElement = true; // if adding new objects is allowed

    $table = new Table($tableName);

    if (!isset($table->tableConfig->options->access))
        die('<b style="color:red;">Доступ запрещён!</b> - <a href="../">на главную</a>');
    if (isset($table->tableConfig->options->nonew) && $table->tableConfig->options->nonew == 1) {
        $newElement = false;
    }

    $action = '';
    if (isset($router->path[2]))
        $action = $router->path[2];
    $exportData = false;
    if ($action == 'export') { //} && $userRole == 1) {
        $action = '';
        $exportData = true;
    }

    if (!in_array($action, Array('', 'new', 'import', 'clear', 'clearsort'))) {
        $id = round($action);
        $action = 'show';
    }

    $subt = $ses['subtable'];

    $_telo .= '<h2>' . $table->tableTitle . '</h2>';
    if (isset($subt[$tableName])) {
        $st = $subt[$tableName];
        $subtab = new Table($st['table']);
        $subval = $subtab->getRowById($st['id']);
        //$_telo .= '<pre>'.print_r($st,true).'</pre>';
        if (isset($st['showfields']) && trim($st['showfields']) != '') {
            $ff = explode(',', $st['showfields']);
            foreach ($ff as $ffk => $ffv)
                $arrval[] = $subval[$ffv];
            $subTitle = join(' ', $arrval);
        } else
            $subTitle = $subval['title'];
        $_telo .= '<h4>' . $subTitle . '</h4>';
    }
    $_telo .= '<hr>';

    if ($action == '' || $action == 'new' || $action == 'import') {
        $addtabs = '';
        $navfile = $router->filePath . 'tpl/html/modnav/' . $tableName . '.html';
        if (file_exists($navfile))
            $addtabs = file_get_contents($navfile);

        $tabdata = Array(
            'indexactive' => ($action == '' ? 'class="active"' : ''),
            'addactive' => ($action == 'new' ? 'class="active"' : '') . ((!$newElement || $onlyReadAccess) ? ' style="display:none;"' : ''),
            'addtabs' => $addtabs
        );
        if (!$onlyReadAccess) {
            $tpl->add('_exporttable', '<li role="presentation" ' . ($action == 'export' ? 'class="active"' : '') . '>
                                      <a href="' . $router->modulPath . 'export/">Экспорт таблицы</a></li>');
            if ($tableName == 'objects') {
                $tpl->add('_importtable', '<li role="presentation" ' . ($action == 'import' ? 'class="active"' : '') . '>
                                      <a href="' . $router->modulPath . 'import/">Импорт таблицы</a></li>');
            }
        }

        $_telo .= $tpl->loadHTML('tabletabs', $tabdata);
    }

    $sqloptions = $table->arrayOptions;


    //##$_telo .= $table->options;

    if (isset($subt[$tableName])) {
        //##echo $hel per->aprn($ses['subtable']); exit;
        $st = $subt[$tableName];
        $field = $st['field'];
        $value = $st['id'];
        $sqloptions['where'] = "(`$field`='$value')";

        $retP = str_replace($tableName, $st['table'], $router->modulPath);
        $tpl->add('modulReturnPath', $retP);
        $_lasbreadcrumbs = '<li><a href="' . $retP . '">' . $st['tableN'] . '</a></li>';

        while (isset($subt[$st['table']])) {
            $st = $subt[$st['table']];
            $retP = str_replace($tableName, $st['table'], $router->modulPath);
            $_breadcrumbs .= '<li><a href="' . $retP . '">' . $st['tableN'] . '</a></li>';
        }

        $_breadcrumbs .= $_lasbreadcrumbs;
        $_breadcrumbs .= '<li class="active">' . $table->tableTitle . '</li>';
    } else {
        $tpl->add('modulReturnStyle', 'style="display:none;"');
        if ($action == '')
            $_breadcrumbs .= '<li class="active">' . $table->tableTitle . '</li>';
        else
            $_breadcrumbs .= '<li><a href="' . $router->modulPath . '">' . $table->tableTitle . '</a></li>';
    }

    if ($action == '') { // list of objects
        $showFiltrBlock = false;
        // установка фильтра
        include '_setfiltr.inc';

        $table->setOptions($sqloptions);

        $countRows = $table->getMaxRows();
        $valSelect = -1;
        $val = '0';
        if (isset($_POST['changeShowRows'])) {
            $val = $_POST['changeShowRowsValue'];
            $valSelect = $val;
            $ses['tableShowRows_' . $table->tableName] = $val;
            $ses['tableShowRowsSelect_' . $table->tableName] = $valSelect;
            $ses->redirect($router->modulPath);
        }
        if (isset($ses['tableShowRows_' . $table->tableName])) {
            $val = $ses['tableShowRows_' . $table->tableName];
            $valSelect = $ses['tableShowRowsSelect_' . $table->tableName];
        }
        if ($val != 'all')
            $val = round($val);
        else
            $val = $countRows;
        if ($val == 0)
            $val = $table->defMaxRows;
        if ($valSelect == -1)
            $valSelect = $table->defMaxRows;

        $_showrows = '';
        foreach ($_showRowsArray as $sk => $sv) {
            $_showrows .= '<option value="' . $sk . '" ' . (($sk == $valSelect) ? 'selected' : '') . '>' . $sv . '</option>';
        }
        $tpl->add('_showrows', $_showrows);

        if (isset($_GET['sort'])) {
            $ses['sort_' . $tableName] = $_GET['sort'];
            unset($ses['sort_reverse_' . $tableName]);
        }
        if (isset($_GET['reverse']))
            $ses['sort_reverse_' . $tableName] = $_GET['reverse'];

        if (isset($ses['sort_' . $tableName])) {
            $desc = '';
            if (isset($ses['sort_reverse_' . $tableName])) {
                $desc = ' DESC';
                $setOrderDescField = 'desc';
            }
            $setOrderField = $ses['sort_' . $tableName];
            $sqloptions['ORDER'] = "`$setOrderField`$desc";
            $_telo .= '<style>.refreshBtn{ display: block !important; } </style>';
        }

        //##echo $hel per->aprn($_SERVER);
        //##echo '<pre>'; print_r($router); echo '</pre>';
        //exit;

        $countPages = ceil($countRows / $val);
        if ($countPages == 0)
            $countPages = 1;
        if (isset($_GET['page']))
            $page = round($_GET['page']);
        else
            $page = 1;
        if ($page == 0)
            $page = 1;
        if ($page > $countPages)
            $page = $countPages;

        $fromRows = ($page - 1) * $val;

        $sqloptions['limit'] = $fromRows . ',' . $val;

        $table->setOptions($sqloptions);
        //##$_telo .= '<p>['.$table->options.']</p>';

        include '_getfiltr.inc';
        $rows = $table->getAllRows();
        if ($rows) {
            //include '_getsearh.inc';
            // отображение формы фильтра
            //##$_telo .= $hel per->aprn($rows);
            //exportData
            $tableData = $tpl->printDefaultTable($table, $rows, $onlyReadAccess);

            if ($exportData) {
                $cachefile = 'cache/export/';
                @mkdir($cachefile);
                $cachefile = 'cache/export/' . $table->tableName . '.csv';
                file_put_contents($cachefile, iconv("utf-8", "windows-1251//IGNORE", $tableData[1]));
                $_telo .= '<iframe src="/' . $cachefile . '" style="display:none;"></iframe>';
            }

            //echo '<textarea rows="50" cols="150">'.$tableData[1].'</textarea>';
            $_telo .= $tableData[0];
            $_telo .= $tpl->printPagination($router->getCurrentHref(), $page, $countPages);
        } else {
            $_telo .= 'нет данных';
        }
        $tpl->add('countelements', $countRows);
        // end - list of objects
    } elseif ($action == 'clear') {
        //$_telo .= $hel per->aprn($ses['subtable']);
        $s = $ses['subtable'];
        unset($s[$tableName]);
        $ses['subtable'] = $s;
        //$_telo .= $hel per->aprn($ses['subtable']);
        $ses->redirect($router->modulPath);
    } elseif ($action == 'clearsort') {
        unset($ses['sort_' . $tableName]);
        unset($ses['sort_reverse_' . $tableName]);
        $ses->redirect($router->modulPath);
    } elseif ($action == 'new' && $newElement && !$onlyReadAccess) {
        $_breadcrumbs .= '<li class="active">Добавление элемента</li>';
        $values = Array();

        if ($table->post()) {
            $_err = '';
            $result = $table->validForm();
            $values = $table->getFormValues();

            //##echo $hel per->aprn($values); exit;
            if ($result) {
                if (isset($ses['subtable'][$tableName])) {
                    $st = $ses['subtable'][$tableName];
                    if (isset($st))
                        $values[$st['field']] = $st['id'];
                }

                $flds = $table->tableConfig->fields;
                foreach ($flds as $fk => $fv) {
                    if ($fv->type == 'file') {
                        if (isset($_FILES[$fv->name]) && $_FILES[$fv->name]['tmp_name'] != '') {
                            $imgpth = $pth . '/' . $fv->name . '/';
                            @mkdir($imgpth);
                            copy($_FILES[$fv->name]['tmp_name'], $imgpth . $_FILES[$fv->name]['name']);
                        }
                        ##echo $hel per->aprn($_FILES[$fv->name]);
                    }
                }

                if (!$table->saveRow($values, $code)) {
                    if ($code == 23000)
                        $_err .= 'Поле "папка" должно быть уникальным<br>';
                    $table->unsetpost();
                    $result = false;
                }

                if ($result) {
                    if ($router->path[1] == 'objects') {
                        $ses->redirect('/control/objects/' . $table->lastInsertId . '/');
                    } else {
                        $ses->redirect($router->modulPath);
                    }
                }
            }

            if (!$result) {
                $_err .= $table->errorMessage;
                $_errortelo .= $helper->errorMsg($_err);
                $table->unsetpost();
            }
        } else { // show Edit Object form
            $_telo .= $tpl->printForm($table, $values, false);
        }

    } elseif ($action == 'import' && $newElement && !$onlyReadAccess) {

        $_breadcrumbs .= '<li class="active">Импорт объектов</li>';
        $importResultsHtml = '';

        if (isset($_POST['importsubmit'])) {
            
            $validation = validateImportObjects();
            if ($validation['validated']) {
                $objects = new Objects();
                $results = $objects->importFromExcel($_FILES['file']['tmp_name'], $_POST['category_id']);
                $importResultsHtml = $tpl->printTable('objects', 'import_results', $results);
            } else {
                $_err = join('<br>', $validation['errors']);
                $_errortelo .= $helper->errorMsg($_err);
            }
        }
        
        if (isset($_POST['category_id']) && $_POST['category_id']) {
            $selectedCategoryId = intval($_POST['category_id']);
        } else {
            $selectedCategoryId = null;
        }
        $categories = [
            Objects::CAT_BEAUTY_HEALTH => 'Красота и здоровье'
        ];

        $categoryOptions = '';
        foreach ($categories as $categoryId => $categoryName) {
            if ($selectedCategoryId && $categoryId == $selectedCategoryId) {
                $selected = ' selected';
            } else {
                $selected = '';
            }
            $categoryOptions .= '<option value="' . $categoryId . '"' . $selected . '>' . $categoryName . '</option>';
        }
        $data = ['category_options' => $categoryOptions, 'import_results' => $importResultsHtml];
        $_telo .= $tpl->loadHTML('table/import_objects', $data);

    } elseif ($action == 'show') {

        $row = $table->getRowByID($id);
        if ($row) {
            $itemAction = '';
            if (isset($router->path[3]))
                $itemAction = $router->path[3];

            if ($itemAction == '')
                $_breadcrumbs .= '<li class="active">Просмотр элемента</li>';
            elseif ($itemAction == 'edit')
                $_breadcrumbs .= '<li class="active">Редактирование элемента</li>';
            elseif ($itemAction == 'delete')
                $_breadcrumbs .= '<li class="active">Удаление элемента</li>';

            $tpl->add('elemPath', $router->modulPath . $id . DS);

            if (isset($row['title']))
                $_telo .= '<h3>' . $row['title'] . '</h3><hr>';

            $itemstabs = Array(
                'indexactive' => ($itemAction == '' ? 'class="active"' : ''),
                'editactive' => ($itemAction == 'edit' ? 'class="active"' : ''),
                'deleteactive' => ($itemAction == 'delete' ? 'class="active"' : ''),
                'onlyReadAccess' => ($onlyReadAccess ? 'style="display:none;"' : ''),
            );
            $_telo .= $tpl->loadHTML('itemstabs', $itemstabs);

            if ($itemAction == '') {
                // емли модуль "объекты" - подключаем функционал единого модуля
                if ($router->path[1] == 'objects') {

                    // получаем контакты, если нету - кнопка создать
                    $contacts_table = new Table('objects_contacts');
                    $contacts_table->setOptions(array('where' => '`object_id` = "' . (int) $id . '"'));
                    $contacts = $contacts_table->getAllRows();
                    $cotacts_form = '<div class="row">';
                    $cotacts_btn = '';
                    if (!empty($contacts)) {
                        foreach ($contacts as $contact) {
                            $cotacts_form .= '<div class="col-md-6">';
                            $cotacts_form .= $tpl->printForm($contacts_table, $contact);
                            $cotacts_form .= '<input type="button" class="btn btn-default contact_obj" data-object_id="' . $id . '"  data-id="' . $contact['id'] . '" data-method="get_edit_contact_form" data-save="edit_contact" value="Редактировать">'
                                    . '<input type="button" class="btn btn-default remove_contact_obj" value="Удалить" data-id="' . $contact['id'] . '" data-method="remove_contact_obj">';
                            $cotacts_form .= '</div>';
                        }
                    } else {
                        
                    }
                    $cotacts_form .= '</div>';
                    $add_cotacts_btn = '<input type="button" class="btn btn-default contact_obj" data-object_id="' . $id . '" data-method="get_add_contact_form" data-save="add_contact" value="Создать">';
                    $contacts_html = '<h4>Контакты ' . $add_cotacts_btn . '</h4>' . $cotacts_form;

                    // получаем объект в разделах

                    $obj_sections_blocks = '';
                    unset($section_table);
                    $sql = 'SELECT * FROM `' . cfg::$prfx . 'objects_names` GROUP BY `newname`';
                    $sth = $contacts_table->_q($sql);
                    $sth->setFetchMode(PDO::FETCH_ASSOC);
                    while ($obj_tab = $sth->fetch()) {
                        $newname = '';
                        switch ($obj_tab['newname']) {
                            case 'transport' :
                                $newname = 'transports';
                                break;
                            default :
                                $newname = $obj_tab['newname'];
                                break;
                        }

                        $section_table = new Table($newname);
                        $section_table->setOptions(array('where' => '`object_id` = "' . (int) $id . '"'));
                        $section_object = $section_table->getRow();
                        if (!empty($section_object)) {
                            $obj_sections_blocks .= '<div class="col-md-6"><h4>' . $obj_tab['title'] . '</h4>' . $tpl->printForm($section_table, $section_object) . ''
                                    . '<input type="button" class="btn btn-default edit_section" value="Редактировать" data-id="' . $section_object['id'] . '" data-section="' . $newname . '" data-method="get_edit_section_form">'
                                    . '<input type="button" class="btn btn-default remove_section_obj" value="Удалить" data-id="' . $section_object['id'] . '" data-section="' . $newname . '" data-method="remove_section_obj">'
                                    . '</div>';
                        }
                    }

                    $obj_sections = '<h4>Объект в разделах <input type="button" class="btn btn-default add_section_obj" data-object_id="' . $id . '" data-method="get_add_section_obj_form" data-save="add_section_obj" value="Создать"></h4><div class="object_block_form" style="padding: 20px 40px 0 0"></div>';
                    $obj_sections .= $obj_sections_blocks;


                    $_telo .= '<div class="row"><div class="col-md-6">';
                    $_telo .= $tpl->printForm($table, $row);
                    $_telo .= '</div><div class="col-md-6">';

                    $_telo .= <<<html
                            <div class="row">
                                <div class=col-md-12>
                                    {$contacts_html}
                                </div>
                            </div>
                            <div class="row">
                                {$obj_sections}
                            </div>
html;
                    $_telo .= '</div></div>';
                } else {
                    $_telo .= $tpl->printForm($table, $row);
                }
            } elseif ($itemAction == 'edit' && !$onlyReadAccess) {
                if ($table->post()) {
                    $_err = '';
                    $result = $table->validForm();
                    $values = $table->getFormValues();

                    //##echo $hel per->aprn($values); exit;
                    if ($result) {
                        $values['id'] = $id;
                        $flds = $table->tableConfig->fields;
                        foreach ($flds as $fk => $fv) {
                            if ($fv->type == 'file' && isset($_FILES[$fv->name])) {
                                $ex = $_POST[$fv->name . '_exists'];
                                $de = isset($_POST[$fv->name . '_delete']) ? $_POST[$fv->name . '_delete'] : 0;
                                if ($ex == 1) {
                                    if (trim($_FILES[$fv->name]['tmp_name']) != '') {
                                        $imgpth = $pth . '/' . $fv->name . '/';
                                        @mkdir($imgpth);
                                        copy($_FILES[$fv->name]['tmp_name'], $imgpth . $_FILES[$fv->name]['name']);
                                    } else
                                        unset($values[$fv->name]);

                                    if ($de == 1) {
                                        $values[$fv->name] = '';
                                    }
                                }
                            }
                        }
                        $table->updateRow($values);
                        if ($result) {
                            if ($router->path[1] == 'objects') {
                                $ses->redirect('/control/objects/' . $id . '/');
                            } else {
                                $ses->redirect($router->modulPath);
                            }
                        }
                    }

                    if (!$result) {
                        $_err .= $table->errorMessage;
                        $_errortelo .= $helper->errorMsg($_err);
                        $table->unsetpost();
                    }
                }
                $_telo .= $tpl->printForm($table, $row, false);
            } elseif ($itemAction == 'delete' && !$onlyReadAccess) {
                if (isset($_POST['delsubmit'])) {
                    $table->deleteRowByID($id);
                    $ses->redirect($router->modulPath);
                }
                $_telo .= $tpl->loadHTML('table/delete');
            } elseif ($itemAction == 'subtable') {
                $_telo .= $id . '<br>';
                $subtable = $router->path[4];
                $subfield = $router->path[5];
                $virtual = $router->path[6];
                if (isset($ses['subtable']))
                    $st = $ses['subtable'];
                $st[$subtable]['tableN'] = $table->tableTitle;
                $st[$subtable]['table'] = $tableName;
                $st[$subtable]['field'] = $subfield;
                $st[$subtable]['id'] = $id;

                $showfields = '';
                $t = new Table($tableName);
                $f = $t->tableConfig->fields;
                foreach ($f as $fk => $fv) {
                    if ($fv->name == $virtual)
                        if (isset($fv->showfields))
                            $showfields = $fv->showfields;
                }
                $st[$subtable]['showfields'] = $showfields;

                $ses['subtable'] = $st;
                //#echo $hel per->aprn($ses['subtable']); exit;
                $ses->redirect(str_replace($router->modul, $subtable, $router->modulPath));
            } else
                $ses->redirect($router->modulPath);
        }
        else {
            $_telo .= '404';
        }
    } elseif ($action == 'delete' && !$onlyReadAccess) {
        $table->deleteRowByID($id);
        $ses->redirect($router->modulPath);
    }
    // elseif ($action == 'export' && $userRole == 1)
    // {
    //     $_telo .= '<h3>Экспорт данных</h3>';
    // }
    else {
        $ses->redirect($router->modulPath);
    }
}//##if (!$security->isAccess($modulname))


function validateImportObjects()
{
    $validated = true;
    $errors = [];
    if (!isset($_POST['category_id']) || !preg_match('/^\d+$/', $_POST['category_id'])) {
        $errors[] = 'Не выбрана категория объекта';
        $validated = false;
    }

    if (!isset($_FILES['file']['name']) || $_FILES['file']['name'] == '') {
        $errors[] = 'Не выбран файл для загрузки';
        $validated = false;
    } else {
        $fileExt = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        if (strtolower($fileExt) != 'xlsx') {
            $errors[] = 'Разрешенный формат файла - xlsx';
            $validated = false;
        }
    }

    return ['validated' => $validated, 'errors' => $errors];
}
