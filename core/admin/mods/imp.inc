<?php

include 'imp_fields.inc';

function loadFileToArray($inputFileName){

    global $titles;
    $inputFileType = 'Excel2007';
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
    $sheetCount = $objPHPExcel->getSheetCount();

    unset($table);
    for($i = 0; $i< $sheetCount; $i++) {
        $sheet = $objPHPExcel->getSheet($i);
        $title = $sheet->getTitle();
        $title = $titles[$title];
        $rowIndex = 0;
        foreach ($sheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $colIndex = 0;
            $row = [];
            foreach ($cellIterator as $cell) {
                $value = $cell->getValue();
                $row[$colIndex] = $value;
                $colIndex++;
            }

            $table[$title][$rowIndex] = $row;
            $rowIndex++;
        }
    }
    return $table;
}

function xlsToUnixDate($xlsDate) {
    $dt = explode('-',PHPExcel_Style_NumberFormat::toFormattedString($xlsDate,'DD-MM-YYYY'));
    return mktime(0,0,0,$dt[1],$dt[0],$dt[2]);
}

error_reporting(E_ALL);
set_time_limit(0);

date_default_timezone_set('Europe/Moscow');

/** Include path **/
set_include_path('core/plugins/PHPExcel/');

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';

$table = loadFileToArray('data.xlsx');

//echo $helper->aprn($table['activitywinter']); exit;

foreach($tnames as $tname => $pubcategory_id) {

    echo $tname.'<br>';

    $objects = new Table($tname);
    $objects->_q('truncate table `' . cfg::$prfx . $tname . '` ');
    $objects->_q('alter table `' . cfg::$prfx . $tname . '` AUTO_INCREMENT = '. ($pubcategory_id * 10000000) .';');
    $data = $table[$tname];
    unset($data[0]);
    foreach ($data as $row) {

        $f = (object)$fields[$tname];
        $client_id = getClientId($row, (object)$clients[$tname]);

        unset($obj);
        foreach ($f as $field => $num) {
            $arr = explode('^', $field);
            $value = $row[$f->$field];

            if (count($arr) == 1) {
                $obj[$field] = $value;
            } else {

                $val = $arr[0];
                $field = $arr[1];
                if ($val != 'is') {

                    if ($tname == 'restaurants' && $val == 'sprav_kitchen') {
                        $kitchens = explode(',', trim($value));
                        unset($kk);
                        foreach($kitchens as $kitchen) {
                            $kk[] = getSpravId($val, $kitchen).'<br>';
                        }
                        $obj[$field] = join(',', $kk);

                    } else {
                        $obj[$field] = getSpravId($val, $value);
                    }
                } else {
                    $obj[$field] = $value == 'да' ? 1 : 0;
                }

            }
        }
        $obj['folder'] = $helper->createFolderName($obj['title']);
        $obj['client_id'] = $client_id;
        $objects->saveRow($obj, $_e);
        echo $obj['title'].' - '.$_e.'<br>';
        $object_id = $objects->lastInsertId;

        // реклама
        $a = (object)$adv[$tname];
        unset($_ad);
        $_ad['object_id'] = $object_id;
        $_ad['pubtype_id'] = getPubType(trim($row[$a->pubtype]));
        $_ad['pubcategory_id'] = $pubcategory_id; // отели
        $_ad['client_id'] = $client_id; // отели
        $advert->saveRow($_ad, $_e);

    }

    echo ' - good<br>';

}

exit;