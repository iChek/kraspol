<?php if ($router->modul != 'login') $ses->adminAuth();

include 'users/mods/_func.inc';

$u = $ses['init'];
$userRole = $u['role'];
$user_id = $u['id'];

//$_telo .= $helper->aprn($ses);

$iscurator = false;
if (isset($ses['init']['kurator'])) $iscurator = true;

$security = new Security($user_id, $iscurator);

$menu = Array(
            'Модули' => Array(
                'access' => Array(1,2,3),
                'items' => Array(
                    Array ( 'title'=>'!Объекты',
                        'access'=>Array(1),
                        'link'=>'objects',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'!Контакты объектов',
                        'access'=>Array(1),
                        'link'=>'objects_contacts/clear/',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Реклама',
                        'access'=>Array(1),
                        'link'=>'adv',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Отели',
                        'access'=>Array(1),
                        'link'=>'hotels',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Рестораны',
                        'access'=>Array(1),
                        'link'=>'restaurants',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Транспорт',
                        'access'=>Array(1),
                        'link'=>'transports',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Активный отдых (лето)',
                        'access'=>Array(1),
                        'link'=>'activitysummer',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Активный отдых (зима)',
                        'access'=>Array(1),
                        'link'=>'activitywinter',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Отдых с детьми',
                        'access'=>Array(1),
                        'link'=>'activitychildren',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Красота и здоровье',
                        'access'=>Array(1),
                        'link'=>'beauties',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Услуги',
                        'access'=>Array(1),
                        'link'=>'services',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Развлечения',
                        'access'=>Array(1),
                        'link'=>'enterts',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Магазины',
                        'access'=>Array(1),
                        'link'=>'shops',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Структура',
                        'access'=>Array(1),
                        'link'=>'structs',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Клиенты',
                        'access'=>Array(1),
                        'link'=>'clients',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Новости',
                        'access'=>Array(1),
                        'link'=>'news',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Слайдер',
                        'access'=>Array(1),
                        'link'=>'slider',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Слайдер (моб)',
                        'access'=>Array(1),
                        'link'=>'slider_mobile',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Трассы',
                        'access'=>Array(1),
                        'link'=>'tracks',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Заявки',
                        'access'=>Array(1),
                        'link'=>'ff',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Подписчики',
                        'access'=>Array(1),
                        'link'=>'subscr',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Экспорт для печатного издания',
                        'access'=>Array(1),
                        'link'=>'exp',
                        'icon'=>'',
                    )
                )
            ),
            'Справочники' => Array(
                'access' => Array(1,3),
                'items' => Array(
                )
            ),
            'Администрирование' => Array(
                'access' => Array(1),
                'items' => Array(
                    Array ( 'title'=>'Коррекция стилей',
                        'access'=>Array(1),
                        'link'=>'_styles',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Обрезка описаний',
                        'access'=>Array(1),
                        'link'=>'_short',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Координаты',
                        'access'=>Array(1),
                        'link'=>'_coords',
                        'icon'=>'',
                    ),
                )
            ),
            'Управление' => Array(
                'access' => Array(1),
                'items' => Array(
                    Array ( 'title'=>'Настройки',
                        'access'=>Array(1),
                        'link'=>'options',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Администраторы',
                        'access'=>Array(1),
                        'link'=>'admins',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Очистить кэш',
                        'access'=>Array(1),
                        'link'=>'clearcache',
                        'icon'=>'',
                    ),
                    Array ( 'title'=>'Проверка почты',
                        'access'=>Array(1),
                        'link'=>'mailtest',
                        'icon'=>'',
                    ),
                )
            ),
        );

// Подгрузка справочников //
unset($items);
$dr = opendir('core/db');
while($fl = readdir($dr)) {
    if ($fl != '.' && $fl != '..' && '' . strpos($fl, 'sprav_') == '0') {
        $bf = file_get_contents('core/db/' . $fl);
        $tt = json_decode($bf);
        $title = $tt->title;
        $name = $tt->name;
        $items[$title] = Array(
            'title' => $title,
            'access' => Array(1),
            'link' => $name,
            'icon' => '',
        );
    }
}
ksort($items);

$items['Соц.сети'] = Array(
    'title' => 'Соц.сети',
    'access' => Array(1),
    'link' => 'social',
    'icon' => '',
);

$items['Трассы. Время года'] = Array(
    'title' => 'Трассы. Время года',
    'access' => Array(1),
    'link' => 'tracktime',
    'icon' => '',
);

$menu['Справочники']['items'] = $items;
// Подгрузка справочников //

$_topmenu  = '';
$_topmenu .= '<li><a href="{%path_to_rootsite%}" target="_blank">Открыть сайт</a></li>';

/**
$_telo .= "\n\n=========================================\n\n".'<div style="display:none">'."\n";
foreach($menu as $mkey => $mval)
{
    $_telo .= $mkey."\n";
    $items = $mval['items'];
    foreach($items as $ikey => $ival)
    {
        $_telo .= '->'.$ival['title']."\n";
        
    }
}
$_telo .= '</div>'."\n\n=========================================\n\n";
/**/

foreach($menu as $mkey => $mval)
{
    $items = $mval['items'];
    $subitems = '';
    foreach($items as $ikey => $ival)
    {
        $modulname = explode('/',$ival['link'])[0];
        if ($security->isAccess($modulname))
        {
            $subitems .= '<li><a href="{%path_to_site%}'.$ival['link'].'/" style="padding-left:10px;padding-right:20px;">';
            $subitems .= '<div style="width:20px;float:left">';
            if (isset($ival['icon']) && trim($ival['icon'])!='') $subitems .= '<i class="icon-'.$ival['icon'].'"></i>';
            $subitems .= '</div> ';
            $subitems .= $ival['title'];
            $subitems .= '</a>';
            $subitems .= '</li>';
        }
    }

    if (trim($subitems) != '')
    {
        $_topmenu .= '
        <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            '.$mkey.' <span class="caret"></span></a>
            <ul class="dropdown-menu" style="padding-right:10px;">
                '.$subitems.'
            </ul>
        </li>
        ';
    }
}

/**
if ($userRole == 1)
{
    $_topmenu .= '
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Управление <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="{%path_to_site%}options/">Настройки</a></a>
            <li><a href="{%path_to_site%}admins/">Администраторы</a></a>
        </ul>
    </li>
    ';
}
/**/

$_rightmenu  = '';
if ($security->isAccess('profile')) {
    $_rightmenu .= '<li><a href="{%path_to_site%}profile/">Профиль</a></li>';
}
$_rightmenu .= '<li><a rel="nofollow" href="{%path_to_site%}logout/"><span class="label label-danger"><i class="icon-power-off"></i>&nbsp;Выход</span></a></li>';

//print_r($router);

$_breadcrumbs = '';

if ($router->modul == 'main') $_breadcrumbs .= '<style>ol.breadcrumb { display: none; } </style>'; //'<li class="active">Главная</li>';
else $_breadcrumbs .= '<li><a href="{%path_to_site%}">Главная</a></li>';

$user = $ses['init'];
$user_id = $user['id'];

$modulname = $router->modul;
if (!$security->isAccess($modulname) && !in_array($modulname,Array('profile','main','moduls','login','logout','_getlist')))
{
    $tpl->indexPage('empty');
    echo '<p><b style="color:red;">Доступ запрещён</b> - <a href="/'.cfg::$cms.'/">перейти на главную</a></p>'; exit;
}

$onlyReadAccess = $security->isRead($modulname);
if ($security->isEdit($modulname)) $onlyReadAccess = false;

if ($security->isCurator)
{
    $u = $ses['init'];
    $cur_id = $u['id'];
    $regsales = ($u['regsales']==1?true:false);
    $orderprizes = ($u['orderprizes']==1?true:false);
    //##$_telo .= $helper->aprn($u);
    
    $_rightmenu = str_replace('Профиль',$u['title'],$_rightmenu);
}


$tpl->add('_rightmenu',$_rightmenu);
$tpl->add('_topmenu',$_topmenu);


// обновление данных
$t = new Table('hotels');
$sql = "
truncate table `".cfg::$prfx."objects_names`;
insert into `".cfg::$prfx."objects_names` (`object_id`, `title`, `name`, `newname`) select * from ( 
SELECT object_id, 'Отели' as title, 'gostinicy' as `name`, 'hotels' as `newname` from krasp_hotels union
SELECT object_id, 'Рестораны' as title, 'restorany-kafe-bary' as `name`, 'restaurants' as `newname` from krasp_restaurants union
SELECT object_id, 'Транспорт' as title, 'transport' as `name`, 'transport' as `newname` from krasp_transports union
SELECT object_id, 'Летний отдых' as title, 'letniy-otdyh' as `name`, 'activitysummer' as `newname` from krasp_activitysummer union
SELECT object_id, 'Зимний отдых' as title, 'zimniy-otdyh' as `name`, 'activitywinter' as `newname` from krasp_activitywinter union
SELECT object_id, 'Отдых с детьми' as title, 'otdyh-s-detmi' as `name`, 'activitychildren' as `newname` from krasp_activitychildren union
SELECT object_id, 'Развлечения' as title, 'razvlecheniya' as `name`, 'enterts' as `newname` from krasp_enterts union
SELECT object_id, 'Здоровье и красота' as title, 'krasota-i-zdorove' as `name`, 'beauties' as `newname` from krasp_beauties union
SELECT object_id, 'Услуги' as title, 'uslugi' as `name`, 'services' as `newname` from krasp_services union
SELECT object_id, 'Магазины' as title, 'magaziny' as `name`, 'shops' as `newname` from krasp_shops) t;
";
$t->_q($sql);
