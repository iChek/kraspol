<?php
$_telo .= '<div id="filtrblock" style="display:'.((!$showFiltrBlock)?'none':'').'">';

$fields = $table->tableConfig->fields;

foreach($fields as $fk => $fv)
{
    if ($fv->showtable == 0) unset($fields[$fk]);
}

//$_telo .= $helper->aprn($fields);

$_telo .= '
<form action="" method="post">
<input type="hidden" name="filtrsubmit" value="1">
<input type="hidden" name="filtraction" id="filtraction" value="clear">
';
$_telo .= '<div class="row">';

foreach($fields as $fk => $fv)
{
    if ($fv->type == 'date')
    {
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
                <input type="text" class="form-control setDatePicker" id="'.$fv->name.'1" name="'.$fv->name.'[value1]"
                       placeholder="с Даты"
                       value="'.(isset($filtrdata[$fv->name.'1'])?$filtrdata[$fv->name.'1']:'').'"
                       >
                <input type="text" class="form-control setDatePicker" id="'.$fv->name.'2" name="'.$fv->name.'[value2]"
                       placeholder="по Дату"
                       value="'.(isset($filtrdata[$fv->name.'2'])?$filtrdata[$fv->name.'2']:'').'"
                       >
            </div>
        </div>
        ';
    }
    elseif ($fv->type == 'string' || $fv->type == 'phone' || $fv->type == 'email')
    {
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
                <input type="text" class="form-control" id="'.$fv->name.'" name="'.$fv->name.'[value]"
                       placeholder="'.(isset($fv->inputText)?$fv->inputText:$fv->title).'"
                       value="'.(isset($filtrdata[$fv->name])?$filtrdata[$fv->name]:'').'"
                       >
            </div>
        </div>
        ';
    }
    elseif ($fv->type == 'integer')
    {
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
                <input type="text" class="form-control onlyDigit" id="'.$fv->name.'1" name="'.$fv->name.'[value1]"
                       placeholder="'.(isset($fv->inputText)?$fv->inputText:$fv->title).' (от)"
                       value="'.(isset($filtrdata[$fv->name.'1'])?$filtrdata[$fv->name.'1']:'').'"
                       >
                <input type="text" class="form-control onlyDigit" id="'.$fv->name.'1" name="'.$fv->name.'[value2]" 
                       placeholder="'.(isset($fv->inputText)?$fv->inputText:$fv->title).' (до)"
                       value="'.(isset($filtrdata[$fv->name.'2'])?$filtrdata[$fv->name.'2']:'').'"
                       >
            </div>
        </div>
        ';
    }
    elseif ($fv->type == 'float')
    {
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
                <input type="text" class="form-control onlySumma" id="'.$fv->name.'1" name="'.$fv->name.'[value1]"
                       placeholder="'.(isset($fv->inputText)?$fv->inputText:$fv->title).' (от)"
                       value="'.(isset($filtrdata[$fv->name.'1'])?$filtrdata[$fv->name.'1']:'').'"
                       >
                <input type="text" class="form-control onlySumma" id="'.$fv->name.'1" name="'.$fv->name.'[value2]" 
                       placeholder="'.(isset($fv->inputText)?$fv->inputText:$fv->title).' (до)"
                       value="'.(isset($filtrdata[$fv->name.'2'])?$filtrdata[$fv->name.'2']:'').'"
                       >
            </div>
        </div>
        ';
    }
    elseif ($fv->type == 'subtable')
    {
        $t = $fv->subtable;
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
        ';
        $sel = (isset($filtrdata[$fv->name])?$filtrdata[$fv->name]:'');
        
        $t = new Table($t);
        unset($showFields);
        if (isset($fv->datafields)) $showFields = explode(',', $fv->datafields); else $showFields[] = 'title';
        $t->setOptions(Array('order'=> join(',',$showFields)));
        $r = $t->getAllRows();
        if ($r)
        {
            $_telo .= '<select class="form-control" id="'.$fv->name.'" name="'.$fv->name.'[value]" placeholder="sdasdA">';
            $_telo .= '<option value="none">Выберите...</option>';
            foreach($r as $rk => $rv)
            {
                $_telo .= '<option value="'.$rv['id'].'" '.($sel==$rv['id']?'selected':'').'>';
                unset($temp); foreach($showFields as $skey => $sval) $temp[] = $rv[$sval];
                $_telo .= join(' ',$temp);
                $_telo .= '</option>';
            }
            $_telo .= '</select>';
        }
        $_telo .= '
            </div>
        </div>
        ';
    }
    elseif ($fv->type == 'sselect')
    {
        $_telo .= '
        <div class="col-sm-2">
            <div class="form-group">
                <label for="'.$fv->name.'">'.$fv->title.': </label>
                <input type="hidden" name="'.$fv->name.'[type]" value="'.$fv->type.'">
        ';
        $sel = (isset($filtrdata[$fv->name])?$filtrdata[$fv->name]:'');
        $items = $fv->values;
        /**/
        $_telo .= '<select class="form-control" id="'.$fv->name.'" name="'.$fv->name.'[value]" placeholder="sdasdA">';
        $_telo .= '<option value="none">Выберите...</option>';
        foreach($items as $rk => $rv)
        {
            $_telo .= '<option value="'.$rk.'" '.($sel==$rk?'selected':'').'>';
            $_telo .= $rv;
            $_telo .= '</option>';
        }
        $_telo .= '</select>';
        /**/
        $_telo .= '
            </div>
        </div>
        ';
    }
}
$_telo .= '</div>';


$_telo .= '
<div class="row">
    <div class="col-sm-12">
        <input type="submit" class="btn btn-default" onClick="$(\'#filtraction\').val(\'setfiltr\');" value="применить фильтр">
        <input type="submit" class="btn btn-default" value="сбросить фильтр">
        &nbsp;Для строковых данных разрешается использовать * для замены группы символов
    </div>
</div>

</form>

<hr>

</div>
';