<?php $ses->adminAuth();

$meta = Array('_title' => 'Экспорт данных - (АП)',
    '_keywords' => 'Экспорт данных - (АП)',
    '_description' => 'Экспорт данных - (АП)'
);
$tpl->addArray($meta);

$af = Array(
    'hotels' => Array(
        'breakfast' => 'Завтрак',
        'wifi' => 'Wi-Fi',
        'transfer' => 'Бесплатный трансфер',
        'parking' => 'Парковка',
        'sauna' => 'Баня/сайна'
    ),
    'restaurants' => Array(
        'alltime' => 'Круглосуточно',
        'delivery' => 'Доставка',
        'childroom' => 'Детская комната',
        'terrace' => 'Терраса'
    ),
    'transports' => Array(
        'transfer' => 'Трансфер'
    ),
    'activitysummer' => Array(
        'ekipir' => 'прокат экипировки и оборудования',
        'gora' => 'горные походы/треккинг',
        'splav' => 'сплавы/рафтинг/каньонинг',
        'moto' => 'мото/квадро/джиппинг',
        'lesopedy' => 'велосипеды',
        'konyaki' => 'конные походы',
        'skalolaz' => 'скалолазание',
        'yoga' => 'йога',
        'ekskurs' => 'экскурсии'
    ),
    'activitywinter' => Array(
        'ekipir' => 'прокат экипировки и оборудования',
        'lyzhi' => 'инструктор лыжи',
        'snowboard' => 'инструктор сноуборд',
        'freeride' => 'гид фрирайд',
        'backcountry' => 'гид бэккантри',
        'healski' => 'хели-ски',
        'skitour' => 'скитур',
        'childschool' => 'детская школа',
        'yoga' => 'йога',
        'ekskurs' => 'экскурсии'
    ),
    'activitychildren' => Array(
        'aktivnyi_otdyh' => 'активный отдых',
        'detskie_kluby' => 'детские клубы'
    )
);

$_breadcrumbs .= '<li class="active">Экспорт данных</li>';
$_telo .= '<h2>Экспорт данных</h2>';
$_telo .= '<p>Модуль экспорта данных для печатного справочника</p><hr>';

$action = $router->getAction(2);

$s = new Table('sprav_pubcategory');

if ($action == '') {

    $sp = $s->getAllRows();
    if ($sp) {

        $_telo .= '<h4>Выберите, пожалуйста, раздел для скачивания экспортируемых данных</h4>';

        foreach($sp as $k => $v) {
            $_telo .= '<a href="/'.cfg::$cms.'/exp/'.$v['intoname'].'/">'.$v['title'].'</a><br>';
        }

    } else {

        $_telo .= '<p>Данных нет</p>';

    }

} else {

    $s->setOptions(Array('where' => " `intoname` = '$action' "));
    $sp = $s->getRow();
    $razdel_name = str_replace(' ','_',$sp['title']);

    /* СПРАВОЧНИКИ */
    $territory = getSpravArray('sprav_territory');
    $d_cabcar = getSpravArray('sprav_distance_cabcar');
    $hoteltype = getSpravArray('sprav_hoteltype');
    $price = getSpravArray('sprav_price');
    $kitchen = getSpravArray('sprav_kitchen');
    $resttype = getSpravArray('sprav_resttype');
    $middlecheck = getSpravArray('sprav_middle_check');
    $transporttype = getSpravArray('sprav_transport_type');
    $beautytype = getSpravArray('sprav_beautytype');
    $servicetype = getSpravArray('sprav_servicetype');
    $shoptype = getSpravArray('sprav_shoptype');
    /* СПРАВОЧНИКИ */

    unset($xls);
    unset($rows);
    $cl = new Table('clients');
    $c = new Table('objects_contacts');
    $sql = 'select t.*, o.* from '.cfg::$prfx.$action.' t join '.cfg::$prfx.'objects as o on o.id = t.object_id;';
    $sth = $c->_q($sql);
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    while($row = $sth->fetch()) $rows[] = $row;
    if ($rows) {

        $rownum = 0;

        $xls[$rownum] = Array('Название','Адрес','Телефон','Территория','Сайт','Описание',
            'Название клиента',
            'ФИО клиента',
            'Телефон клиента',
            'E-mail клиента',
            'ФИО КЛ',
            'Телефон КЛ',
            'E-mail КЛ',
            'Юр.лицо'
            );

        if ($action == 'hotels') {
            $xls[$rownum][] = 'Тип отеля';
        } elseif ($action == 'restaurants') {
            $xls[$rownum][] = 'Тип ресторана';
        } elseif (in_array($action, Array('transports', 'beauties', 'services', 'shops'))) {
            $xls[$rownum][] = 'Тип';
        }

        if (isset($af[$action])) {
            foreach($af[$action] as $key => $val) {
                $xls[$rownum][] = $val;
            }
        }

        foreach($rows as $k => $row) {
            $rownum++;
            
            $object_id = $row['object_id'];
            $client_id = $row['client_id'];

            $xls[$rownum][] = $row['title'];

            unset($address);
            unset($phone);

            $c->setOptions(Array('where' => " `object_id` = '$object_id' "));
            $cs = $c->getAllRows();
            if ($cs) {
                foreach ($cs as $csk => $csv) {
                    if (trim($csv['address']) != '') $address[] = $csv['address'];
                    if (trim($csv['phone']) != '') $phone[] = $csv['phone'];
                }
            }

            $xls[$rownum][] = (isset($address) ? join(', ', $address) : '---');
            $xls[$rownum][] = (isset($phone) ? join(', ', $phone) : '---');

            $xls[$rownum][] = $territory[$row['territory_id']]->title;
            $xls[$rownum][] = $row['site'];
            $xls[$rownum][] = $row['descr'];

            // контакты клиента
            $client = $cl->getRowByID($client_id);
            if ($client) {
                $xls[$rownum][] = $client['title'];
                $xls[$rownum][] = $client['fio'];
                $xls[$rownum][] = $client['phone'];
                $xls[$rownum][] = $client['email'];
                $xls[$rownum][] = $client['klfio'];
                $xls[$rownum][] = $client['klphone'];
                $xls[$rownum][] = $client['klemail'];
                $xls[$rownum][] = $client['ur_lico'];
            } else {
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
                $xls[$rownum][] = '-';
            }
            // контакты клиента

            if ($action == 'hotels') {
                $xls[$rownum][] = $hoteltype[$row['hoteltype_id']]->title;
            } elseif ($action == 'restaurants') {
                $xls[$rownum][] = $resttype[$row['resttype_id']]->title;
            } elseif ($action == 'transports') {
                $xls[$rownum][] = $transporttype[$row['transporttype_id']]->title;
            } elseif ($action == 'beauties') {
                $xls[$rownum][] = $beautytype[$row['beautytype_id']]->title;
            } elseif ($action == 'services') {
                $xls[$rownum][] = $servicetype[$row['servicetype_id']]->title;
            } elseif ($action == 'shops') {
                $xls[$rownum][] = $shoptype[$row['shoptype_id']]->title;
            }

            if (isset($af[$action])) {
                foreach($af[$action] as $key => $val) {
                    $xls[$rownum][] = ($row[$key] == 1 ? 'Да':'Нет');
                }
            }
        }
    }

    if (isset($xls)) {

        error_reporting(E_ALL);
        set_time_limit(0);
        date_default_timezone_set('Europe/Moscow');
        set_include_path('core/plugins/PHPExcel/');
        include 'PHPExcel/IOFactory.php';

        $fname = $razdel_name.'_'.date('[Y_m_d]_[H_i]').'.xlsx';
        $pname = 'cache/'.$fname;

        $objPHPExcel = new PHPExcel();
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objSheet = $objPHPExcel->getActiveSheet();
        //$objSheet->getCellByColumnAndRow(колонка,строка)->setValue('***');

        $rowNum = 1;
        foreach($xls as $key1 => $row) {
            $colNum = 0;
            foreach($row as $key2 => $cell) {
                $objSheet->getCellByColumnAndRow($colNum,$rowNum)->setValue($cell);
                $colNum++;
            }
            $rowNum++;
        }

        $objWriter->save($pname);

        if (ob_get_level()) {
            ob_end_clean();
        }
        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . $fname);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($pname));
        readfile($pname);
        exit;
    }

}