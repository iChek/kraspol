<?php

$linktypes = Array( 0 => "резерв", 1 => "premium-1", 2 => "premium-2", 3 => "premium-3", 4 => "premium-4", 5 => "besplatno");

$obj = new Table('objects');

if (isset($_POST['subm_linkas'])) {

    $id = $_POST['id'];
    $linktype = $_POST['linktype'];
    $folder = $_POST['folder'];

    unset($_re);
    $_re['id'] = $id;
    $_re['linktype'] = $linktype;
    $_re['folder'] = $folder;
    $obj->updateRow($_re);
    echo 'good';
    exit;
}

$_telo .= '<form action="" method="post"><input type="hidden" name="subm_linkas" value="1">';

$l = 0;

$obj->setOptions(Array('order' => ' `title` '));
$tabs = $obj->getAllRows();
if ($tabs) {
    $cnt = 0;
    $_telo .= '<h3>'.$name.'</h3>';
    $_telo .= '<table class="table table-condensed table-hover table-striped "><tr><th>#</th><th>Имя</th><th>Тип ссылки</th><th style="width:100%;">Папка</th><th></th></tr>';
    foreach ($tabs as $key => $row) {
        $ltoptions = '';
        foreach ($linktypes as $k => $v) { $ltoptions .= '<option value="' . $k . '" ' . (($k == $row['linktype']) ? 'selected' : '') . '>' . $v . '</option>'; }

        $cnt++;
        $_telo .= '<tr>';
        $_telo .= '<td>' . $cnt . '</td>';
        $_telo .= '<td style="width: 30%;">' . $row['title'] . '</td>';
        $_telo .= '<td><select name="linktype_'.$row['id'].'" id="linktype_'.$row['id'].'"><option value="none">... тип ссылки ...</option>' . $ltoptions . '</select></td>';
        $_telo .= '<td><input type="text" name="folder_'.$row['id'].'" id="folder_'.$row['id'].'" value="'.$row['folder'].'" style="width:100%;"></td>';
        $_telo .= '<td colspan="4"><button type="button" class="btn btn-success btn-xs save-row" data-id="'.$row['id'].'">сохранить</button></td>';
        $_telo .= '</tr>';
    }
    $_telo .= '</table>';
}

$_telo .= '</form>

<script>
    $(function(){
        $(".save-row").on("click", function(){
            var id = $(this).data("id");
            var linktype = $("#linktype_"+id).val();
            var folder = $("#folder_"+id).val();
            var $this = $(this);
            
            $this.html("<i class=\'fa fa-spin fa-spinner\'></i>");
            
            $.ajax({
                url:  "/control/_linkas",
                type: "post",
                data: { subm_linkas:1, id:id, linktype:linktype, folder:folder },
                dataType: "text",
                success: function(data) {
                    console.log(data);
                    if (data == "good") {
                        $this.html("сохранить");                        
                    }
                }
                
            });
        });
    })
</script>

';