<?php

//## Класс для работы с базой

Class Dbase
{
    private static $isConnect = false;
    private static $db;
    private $typebase;
    
    var $lastInsertId;
    var $options = '';
    var $arrayOptions = Array();

    function __construct()
    {
        $this->typebase = 'mysql';
        if (!self::$isConnect) $this->connect();
    }
    
    function setOptions($opts)
    {
        if (is_array($opts))
        {
            $options = $opts; //array_merge($,$opts);
            $allKeys = array_keys($options);
            array_walk($allKeys, function(&$val){ $val = strtoupper($val); });
            
            $querySql = "";
            if (in_array("WHERE", $allKeys))
            {
                foreach($options as $key => $val)
                {
                    if(strtoupper($key) == "WHERE")
                    {
                        $querySql .= " WHERE " . $val;
                    }
                }
            }
             
            if (in_array("GROUP", $allKeys))
            {
                foreach($options as $key => $val)
                {
                    if(strtoupper($key) == "GROUP")
                    {
                        $querySql .= " GROUP BY " . $val;
                    }
                }
            }
             
            if (in_array("ORDER", $allKeys))
            {
                foreach($options as $key => $val)
                {
                    if(strtoupper($key) == "ORDER")
                    {
                        $querySql .= " ORDER BY " . $val;
                    }
                }
            }
             
            if (in_array("LIMIT", $allKeys))
            {
                foreach($options as $key => $val)
                {
                    if(strtoupper($key) == "LIMIT")
                    {
                        $querySql .= " LIMIT " . $val;
                    }
                }
            }

            $this->options = $querySql;
            return true;
        }
        return false;
    }
    
    function _q($sql)
    {
        $sth = self::$db->query($sql);
        return $sth;
    }
    
    function getMaxRows()
    {
        $sql = "SELECT `id` FROM `".$this->tableName."` ".$this->options.";";
        $sth = self::$db->query($sql);
        return count($sth->fetchAll());
    }
    
    function saveRow($arr, &$code)
    {
        if (!isset($arr['created'])) $arr['created'] = time();
        if (!isset($arr['updated'])) $arr['updated'] = time();

        foreach($arr as $ak => $av)
        {
            $fields[] = "`$ak`"; $prep[] = '?';
            $values[] = $av;
        }
        $sql = "INSERT INTO `".$this->tableName."` (".join(',',$fields).") values (".join(',',$prep).")";

        try
        {
            $sth = self::$db->prepare($sql);
            $sth->execute($values);
            $this->lastInsertId = self::$db->lastInsertId();
        }
        catch(PDOException $ex)
        {
            $code = $ex->getCode();
            //$this->errorMessage = $ex->getMessage();
            if ($code!=23000)
            {
                die('<b style="color:red;">'.$ex->getMessage().'</b>');
            }
            else return false;
        }
        $code = 0;
        return true;
    }
    
    function updateRow($arr)
    {
        $id = $arr['id']; unset($arr['id']);
        $arr['updated'] = time();
        foreach($arr as $ak => $av)
        {
            $values[] = $av;
            $filds[] = "`$ak`=?";
        }
        $sql = "UPDATE `".$this->tableName."` SET ".join(', ', $filds)." WHERE `id`=$id; ";        
        $sth = self::$db->prepare($sql);
        return $sth->execute($values);
    }


    function updateRowByCondition($arr, $conditions)
    {        
        $arr['updated'] = time();
        foreach ($arr as $ak => $av) {
            $values[] = $av;
            $fields[] = "`$ak`=?";
        }
        foreach ($conditions as $field => $value) {
            $conditionValues[] = $value;
            $conditionFields[] = "`$field`=?";
        }
        $sql = "UPDATE `" . $this->tableName . "` SET " . join(', ', $fields) . " WHERE " . join(' AND ', $conditionFields);
        $sth = self::$db->prepare($sql);
        return $sth->execute(array_merge($values, $conditionValues));
    }
    
    function deleteRowByID($id)
    {
        $sql = "DELETE FROM `".$this->tableName."` WHERE `id`=$id; ";
        $sth = self::$db->prepare($sql);
        return $sth->execute();
    }

    function deleteRowByConditions($conditions)
    {
        if (!is_array($conditions) || count($conditions) == 0) {
            return false;
        }
        $conditionFields = [];
        $conditionValues = [];
        foreach ($conditions as $field => $value) {
            $conditionValues[] = $value;
            $conditionFields[] = "`$field`=?";
        }        
        $sql = "DELETE FROM `" . $this->tableName . "` WHERE " . join(' AND ', $conditionFields) . ' LIMIT 1';
        $sth = self::$db->prepare($sql);
        return $sth->execute($conditionValues);
    }
    
    function deleteAllRows()
    {
        try
        {
            $sql = "delete from `".$this->tableName."` ".$this->options.";";
            $sth = self::$db->query($sql);
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b><br><pre>'.$sql.'</pre>');
        }
        return true;
    }
    
    function saveManyRows($arr)
    {
        $code = '';
        foreach($arr as $k => $row)
        {
            $this->saveRow($row,$code);
        }
    }
    
    function updateManyRows($arr)
    {
    }
    
    function getAllRows()
    {
        try
        {
            $sql = "select * from `".$this->tableName."` ".$this->options.";";
            $sth = self::$db->query($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            while($row = $sth->fetch()) $rows[] = $row;
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b><br><pre>'.$sql.'</pre>');
        }
        return (isset($rows)?$rows:false);
    }

    function getAllRowsArraysID() {
        $result = $this->getAllRows();
        if ($result) {
            $arr = Array();
            foreach($result as $kres => $vres) {
                $arr[$vres['id']] = $vres;
            }
            $result = $arr;
        }
        return $result;
    }
    
    function getRowByID($id)
    {
        $result = false;
        try
        {
            $sth = self::$db->prepare("SELECT * FROM `".$this->tableName."` WHERE `id`=? ");
            $sth->execute(Array($id));
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $result = $sth->fetch();
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b>');
        }
        
        return $result;
    }    
    
    function getRow()
    {
        $result = false;
        try
        {
            $sql = "SELECT * FROM `".$this->tableName."` ".$this->options.";";
            $sth = self::$db->query($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $result = $sth->fetch();
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b>');
        }
        
        return $result;
    }
    
    function clearDataTable()
    {
        $this->data = Array('lastindex'=>0,'data'=>Array());
        $this->saveTextTable();
    }
    
    function getArrayFields()
    {
        $result = Array();
        foreach($this->fieldsName as $ak => $av) $result[$av] = '';
        unset($result['id']);
        unset($result['created']);
        unset($result['updated']);
        return $result;
    }

    public static function getItemById($table, $id){
        
        $id = round($id);
        $result = false;
        try
        {
            $sql = "SELECT * FROM `".cfg::$prfx.$table."` where `id`='$id' limit 0,1 ;";
            $sth = self::$db->query($sql);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $result = $sth->fetch();
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b>');
        }
        
        return $result;
    }
    
    //######################################################################
    
    private function connect()
    {
        try
        {
            self::$db = new PDO('mysql:host='.cfg::$host.';dbname='.cfg::$base,cfg::$user,cfg::$pass);
            self::$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            self::$db->exec("set names utf8");
            self::$isConnect = true;
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b>');
        }
    }
    
    protected function isTableExists()
    {
        $result = true;
        $sql = 'select * from `'.$this->tableName.'` limit 0,1';
        try { self::$db->query($sql); }
        catch(PDOException $ex) { $result = false; }
        return $result;
    }
    
    protected function getTypeField($type)
    {
        $sql = '';
        if ($type == 'string' ||
            $type == 'email' ||
            $type == 'folder'
            ) $sql = 'varchar(255) DEFAULT NULL';
        if ($type == 'password') $sql = 'varchar(32) NOT NULL';
        elseif ($type == 'integer') $sql = 'int(11) NOT NULL';
        elseif ($type == 'float') $sql = 'float NOT NULL';
        elseif ($type == 'money') $sql = 'decimal(28,2) NOT NULL';
        elseif ($type == 'position') $sql = 'int(11) NOT NULL';
        elseif ($type == 'flag') $sql = 'tinyint(4) NOT NULL';
        elseif ($type == 'sselect') $sql = 'int(11) NOT NULL';
        elseif ($type == 'tselect') $sql = 'int(11) NOT NULL';
        elseif ($type == 'text') $sql = 'text DEFAULT NULL';
        elseif ($type == 'html') $sql = 'text DEFAULT NULL';
        elseif ($type == 'phone') $sql = 'varchar(20) NOT NULL';
        elseif ($type == 'subtable') $sql = 'int(11) NOT NULL';
        elseif ($type == 'date') $sql = 'int(11) DEFAULT NULL';
        elseif ($type == 'time') $sql = 'int(11) DEFAULT NULL';
        elseif ($type == 'datetime') $sql = 'int(11) DEFAULT NULL';
        elseif ($type == 'file') $sql = 'varchar(255) NOT NULL';
        elseif ($type == 'multi') $sql = 'varchar(255) NOT NULL';
        elseif ($type == 'subtable_checkboxes') $sql = 'text NOT NULL';
        return $sql;
    }
    
    protected function createTable()
    {
        global $helper;
        
        $cfgTab = $this->getTableConfig();
        
        $fields = $cfgTab->fields;
        //echo $helper->aprn($fields);

        $sql  = '
        CREATE TABLE `'.$this->tableName.'`( 
            `id` int(10) unsigned NOT NULL,
            `created` int(11) NOT NULL,
            `updated` int(11) NOT NULL,
            `title` varchar(150) NOT NULL,';
        foreach($fields as $fk => $fv)
        {
            if (!in_array($fv->name, Array('title','status')) &&
                !in_array($fv->type, Array('virtual','link','compile','gallery')))
            {
                $sql .= "\n".'            `'.$fv->name.'` '.$this->getTypeField($fv->type).', ';

                if ($fv->type == 'folder')
                {
                    $sqls[] = 'ALTER TABLE `'.$this->tableName.'` ADD UNIQUE KEY `folder` (`folder`);';
                }
            }
        }
        
        $sql .= '
            `status` bit(1) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
        ';
        
        $sql1 = 'ALTER TABLE `'.$this->tableName.'` ADD PRIMARY KEY (`id`);';
        $sql2 = 'ALTER TABLE `'.$this->tableName.'` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;';
        
        $this->q($sql);
        $this->q($sql1);
        $this->q($sql2);
        
        if (isset($sqls))
        {
            foreach($sqls as $k => $sql) $this->q($sql);
        }
        
        /*
        echo '<pre>'.$sql.'</pre>';
        echo '<pre>'.$sql1.'</pre>';
        echo '<pre>'.$sql2.'</pre>';
        */
        if ($this->isStopCreateTable) die('<b style="color:red;">Таблица '.$this->tableName.' создана</b>');
    }
    
    protected function createField($field,$after)
    {
        $name = $field->name;
        $type = $this->getTypeField($field->type);
        $sql = "ALTER TABLE `" . $this->tableName . "` ADD `$name` $type " . ((trim($after) != '') ? "AFTER `$after`" : '') . ";";
        $this->q($sql);
        return ' - <b style="color:green;">создано</b>';
    }
    
    protected function q($sql)
    {
        $r = false;
        try
        {
            $r = self::$db->query($sql);
        }
        catch(PDOException $ex)
        {
            die('<b style="color:red;">'.$ex->getMessage().'</b><br><pre>'.$sql.'</pre>');
        }
        return $r;
    }
    
    protected function initDBTable()
    {
        if ($this->typebase == 'text')
        {
            $tableFile = FULL_ROOT.'db'.DS.$this->tableName.'.tbl';
            $this->data = unserialize(file_get_contents($tableFile));
        }
    }
    
    private function saveTextTable()
    {
        $tableFile = FULL_ROOT.'db'.DS.$this->tableName.'.tbl';
        $fp = fopen($tableFile,'w');
        fwrite($fp,serialize($this->data));
        fclose($fp);
    }

    public function escape($text)
    {
        return self::$db->quote($text);
    }

}
