<?php

Class Sessions Implements ArrayAccess
{
    protected $vars = array();
    
    var $cms;
    var $fullPath;
    var $sessId;
    var $sessPath;
    var $sessDir1;
    var $sessDir2;
    var $isAdminAuth = false;
    var $isUserAuth = false;
    
    public function __construct()
    {
        session_start();
        $this->sessId = session_id();
        
        $this->sessDir1 = date('Ymd');
        $this->sessDir2 = date('Ymd',time()-86400);
        
        $this->sessPath = CACHE_ROOT.'sessions'.DS;
        
        if (isset($_SESSION['vari'])) {
            $this->vars = $_SESSION['vari'];
        }

        /*

        @mkdir($this->sessPath);
        @mkdir($this->sessPath.$this->sessDir1);
        
        $f1 = $this->sessPath.$this->sessDir1.DS.$this->sessId;
        $f2 = $this->sessPath.$this->sessDir2.DS.$this->sessId;
        if (file_exists($f1))
        {
            $this->vars = unserialize(join('',file($f1)));
        }
        elseif (file_exists($f2))
        {
            $this->vars = unserialize(join('',file($f2)));
        }

        // удаление старых директорий
        $dirHandle = opendir($this->sessPath);
        while ($file = readdir($dirHandle)) if ($file!='.' && $file!='..' && $file!=$this->sessDir1 && $file!=$this->sessDir2)
        {
            $path = $this->sessPath.'/'.$file;
            $dirHandle2 = opendir($path);
            while ($file2 = readdir($dirHandle2)) if ($file2!='.' && $file2!='..')
            {
                unlink($path.'/'.$file2);
            }
            rmdir($path);
        }
        */
        
        $this->isAdminAuth = isset($this->vars['init']);
        $this->isUserAuth = isset($this->vars['uinit']);
    }
    
    private function save_state()
    {
        /*
        $fp = fopen($this->sessPath.$this->sessDir1.'/'.$this->sessId,'w');
        fwrite($fp,serialize($this->vars));
        fclose($fp);
        */
        $_SESSION['vari'] = $this->vars;
    }
    
    public function __destruct()
    {
        $this->save_state();
    }

    public function set($key, $var)
    {
        $this->vars[$key] = $var;
        return true;
    }
    
    public function get($key)
    {
        if (!isset($this->vars[$key])) return null;
        return $this->vars[$key];
    }
    
    public function remove($key)
    {
        unset($this->vars[$key]);
    }
    
    function redirect($url)
    {
        $this->save_state();
        header('location: '.$url);
        exit;
    }

    function offsetExists($offset)
    {
        return isset($this->vars[$offset]);
    }

    function offsetGet($offset)
    {
        return $this->get($offset);
    }

    function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    function offsetUnset($offset)
    {
        $this->remove($offset);
    }
    
    function adminAuth()
    {
        if (!isset($this->vars['init'])) $this->redirect(SITE_ROOT.cfg::$cms.DS.'login'.DS);
    }
    
    function userAuth()
    {
        if (!isset($this->vars['uinit'])) $this->redirect(SITE_ROOT.'login'.DS);
    }
}

?>