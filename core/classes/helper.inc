<?php

/* Класс хелперов */

Class Helper
{
    public function __call($method, $args)
    {
        $helpfile = FULL_ROOT.DS.'core'.DS.'helpers'.DS.$method.'.inc';
        if (!file_exists($helpfile))
        {
            die('Невозможно найти хелпер - "'.$helpfile.'"');
        }
        else
        {
            if (!function_exists($method)) include $helpfile;
            return $method($args);
        }
    }
}
