<?php

Class Router
{
    public $path = Array();
    public $admin = false;
    public $modul = 'main';
    public $filePath;
    
    public $beginFile;
    public $modulFile;
    public $modulPath;
    
    public $request_params;
    
    private $tpl;
    
    function __construct($tpl)
    {
        $this->tpl = $tpl;
        $this->filePath = 'users'.DS;
    }
    
    public function start()
    {
        if (isset($_GET['route']))
        {
            $route = $_GET['route'];
            $route = preg_replace("#/$#",'',$route);
            $this->path = explode('/',$route);
        }
        
        if (count($this->path) > 0)
        {
            if ($this->path[0] == cfg::$cms)
            {
                // запрос на доступ в админку
                $this->admin = true;
                $this->filePath = 'core'.DS.'admin'.DS;
                $this->modulPath = SITE_ROOT.cfg::$cms.DS;
                if (isset($this->path[1]) && trim($this->path[0])!='') $this->modul = $this->path[1];
            }
            else
            {
                // запрос на доступ в пользовательскую часть
                $this->modul = $this->path[0];
                $this->modulPath = SITE_ROOT;
            }
            $this->modulPath .= $this->modul.DS;
        }
        
        $this->tpl->add('modulPath',$this->modulPath);
        $this->tpl->tplPath($this->filePath.'tpl'.DS);
        
        $this->beginFile = $this->filePath.'mods'.DS.'_begin.inc';
        if (in_array($this->modul,Array('резерв','premium-1','premium-2','premium-3','premium-4','besplatno'))) {
            $this->modul = 'detail';
        }
        $this->modulFile = $this->filePath.'mods'.DS.$this->modul.'.inc';
        
        if (!file_exists($this->modulFile))
        {
            if (!$this->admin)
            {
                $this->modulFile = $this->filePath.'mods'.DS.'structs.inc';
                $this->modpage = $this->modul;
            }
            else
            {
                global $ses;
                $this->modulFile = $this->filePath.'mods'.DS.'tables.inc';
                $this->modpage = $this->modul;
            }
        }
        
        $params = $_GET;
        unset($params['route']);
        unset($params['page']);
        $this->request_params = $params;
    }
    
    function getCurrentHref()
    {
        $href = $this->modulPath;

        $href_params = '';
        if (isset($this->request_params) && is_array($this->request_params))
        {
            foreach($this->request_params as $rk => $rv) $arr[] = "$rk=$rv";
            if (isset($arr)) $href_params = '?'.join('&',$arr);
        }
        $href .= $href_params;
        
        return $href;
    }
    
    function getAction($num = 1)
    {
        $action = '';
        if (isset($this->path[$num])) $action = trim($this->path[$num]);
        return $action;
    }
}