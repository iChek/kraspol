<?php

require(FULL_ROOT . '/core/plugins/PHPExcel/PHPExcel.php');

// Класс для работы с объектами

Class Objects extends Dbase
{
    const CAT_HOTELS = 1;
    const CAT_RESTAURANTS = 2;
    const CAT_TRANSPORT = 3;
    const CAT_BEAUTY_HEALTH = 4;
    const CAT_ACITIVITY_SUMMER = 5;
    const CAT_ACITIVITY_WINTER = 6;
    const CAT_ENTERTAINMENT = 7;
    const CAT_SERVICES = 8;
    const CAT_SHOPS = 9;
    const CAT_ACITIVITY_CHILDREN = 10;

    const STATUS_PAID = 1;
    const STATUS_FREE = 2;
    const STATUS_REMOVE = 3;

    private $territories;

    function __construct()
    {
        parent::__construct();
    }

   
    public function importFromExcel($filename, $categoryId)
    {
        $data = $this->loadFromExcel($filename);
        $results = [];

        // build territories reference
        $tblSpravTerritory = new Table('sprav_territory');
        $territoriesData = $tblSpravTerritory->getAllRows();
        $this->territories = [];
        foreach ($territoriesData as $territory) {
            $this->territories[$territory['title']] = $territory['id'];
        }
        
        foreach ($data['rows'] as $row) {

            if (!$row['id']) {
                
                $addResult = $this->addObject($row, $categoryId);
                if ($addResult['success']) {
                    $resultMessage = 'добавлен';
                    $results[] = ['id' => $addResult['id'], 'title' => $row['title'], 'result' => $resultMessage];
                } else {
                    foreach ($addResult['errors'] as $key => $error) {
                        $addResult['errors'][$key] = '<span class="import_object_error">' . $addResult['errors'][$key] . '</span>';
                    }
                    $resultMessage = join('<br>' , $addResult['errors']);
                    $results[] = ['id' => '', 'title' => $row['title'], 'result' => $resultMessage];
                }                 

            } else if ($row['status'] == self::STATUS_REMOVE) {

                // remove object
                $removeResult = $this->removeObject($row['id'], $categoryId);
                if ($removeResult['success']) {
                    $resultMessage = 'удален';
                } else {
                    foreach ($removeResult['errors'] as $key => $error) {
                        $removeResult['errors'][$key] = '<span class="import_object_error">' . $removeResult['errors'][$key] . '</span>';
                    }
                    $resultMessage = join('<br>' , $removeResult['errors']);
                }
                $results[] = ['id' => $row['id'], 'title' => $row['title'], 'result' => $resultMessage];

            } else {

                // update object
                $updateResult = $this->updateObject($row, $categoryId);
                if ($updateResult['success']) {
                    $resultMessage = 'обновлен';
                } else {
                    foreach ($updateResult['errors'] as $key => $error) {
                        $updateResult['errors'][$key] = '<span class="import_object_error">' . $updateResult['errors'][$key] . '</span>';
                    }
                    $resultMessage = join('<br>' , $updateResult['errors']);
                }
                $results[] = ['id' => $row['id'], 'title' => $row['title'], 'result' => $resultMessage];
            }
        }

        return $results;
    }

    
    private function loadFromExcel($filename)
    {
        $inputFileType = 'Excel2007';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($filename);
        $objSheet = $objPHPExcel->getActiveSheet();

        $rowNum = 1;
        $colNum = 0;
        $data = [];

        do {
            $value = $objSheet->getCellByColumnAndRow($colNum, $rowNum)->getValue();
            if (trim($value) != '') {
                $data['fields'][] = $this->trim($value);
                $colNum++;
            }
        } while (trim($value) != '');

        $highestColumnIndex = $colNum - 1;
        $highestRow = $objSheet->getHighestRow();
        $statusFieldIndex = $this->getStatusFieldIndex($data['fields']);

        $dataRowNum = 0;

        for ($rowNum = 3; $rowNum <= $highestRow; $rowNum++) {
            $objectName = $objSheet->getCellByColumnAndRow(1, $rowNum)->getValue();

            $id = $objSheet->getCellByColumnAndRow(0, $rowNum)->getValue();
            $status = $objSheet->getCellByColumnAndRow($statusFieldIndex, $rowNum)->getValue();

            // quit if no id
            if ($id == '-') {
                continue;
            }

            for ($colNum = 0; $colNum <= $highestColumnIndex; $colNum++) {
                $value = $this->trim($objSheet->getCellByColumnAndRow($colNum, $rowNum)->getValue());
                $key = $data['fields'][$colNum];
                $data['rows'][$dataRowNum][$key] = $value;
            }
            $dataRowNum++;
        }

        return $data;
    }


    private function updateObject($objectData, $categoryId)
    {
        $tblObjects = new Table('objects');
        $object = $tblObjects->getRowById($objectData['id']);
        if (!$object) {
            return ['success' => false, 'errors' => ['объект не найден']];
        }

        if ($objectData['title'] == '') {
            return ['success' => false, 'errors' => ['не указано название объекта']];
        }

        $tblObjectsContacts = new Table('objects_contacts');
        $errors = [];

        if ($categoryId == self::CAT_BEAUTY_HEALTH) {
            $tblBeauties = new Table('beauties');
            $tblSpravBeautyTypes = new Table('sprav_beautytype');

            // build beauty types reference
            $beautyTypesData = $tblSpravBeautyTypes->getAllRows();
            
            $beautyTypes = [];
            foreach ($beautyTypesData as $beautyType) {
                $beautyTypes[mb_strtolower($beautyType['title'])] = $beautyType['id'];
            }

            // update krasp_objects
            $object = [
                'id' => $objectData['id'],
                'title' => $objectData['title'],
                'site' => $objectData['site']
            ];
            $territoryName = $objectData['territory'];
            if (isset($this->territories[$territoryName])) {
                $object['territory_id'] = $this->territories[$territoryName];
            } else {
                // report error - unrecognized territory
                $errors[] = 'Не найдена территория: ' . $territoryName;
            }
            $tblObjects->updateRow($object);

            // update krasp_objects_contacts
            $objectContacts = [                
                'address' => $objectData['address'],
                'phone' => $objectData['phone']
            ];
            $tblObjectsContacts->updateRowByCondition($objectContacts, ['object_id' => $objectData['id']]);

            // update krasp_beauties
            $beautyTypeName = $objectData['type'];
            if (isset($beautyTypes[mb_strtolower($beautyTypeName)])) {
                $beauty = [
                    'beautytype_id' => $beautyTypes[mb_strtolower($beautyTypeName)]
                ];
                $tblBeauties->updateRowByCondition($beauty, ['object_id' => $objectData['id']]);
            } else {
                // report error - unrecognized beauty type
                if (trim($beautyTypeName) == '') {
                    $errors[] = 'Не указан тип объекта';
                } else {
                    $errors[] = 'Не найден тип: ' . $beautyTypeName;
                }
            }
        }

        if ($errors) {
            $result = ['success' => false, 'errors' => $errors];
        } else {
            $result = ['success' => true];
        }

        return $result;
    }


    // add object
    private function addObject($objectData, $categoryId)
    {
        $errors = [];

        if ($categoryId == self::CAT_BEAUTY_HEALTH) {
            $tblObjects = new Table('objects');
            $tblObjectsContacts = new Table('objects_contacts');
            $tblClients = new Table('clients');
            $tblBeauties = new Table('beauties');


            if ($objectData['title'] == '') {
                $errors[] = 'Не указано название объекта';
            }

            $territoryName = $objectData['territory'];
            $territoryId = null;
            if ($territoryName == '') {
                $errors[] = 'Не указана территория';
            } else {
                if (isset($this->territories[$territoryName])) {
                    $territoryId = $this->territories[$territoryName];
                } else {
                    // report error - unrecognized territory
                    $errors[] = 'Не найдена территория: ' . $territoryName;
                }
            }

            // get beauty type id
            if ($objectData['type'] == '') {
                $errors[] = 'Не указан тип объекта';
            } else {

                // build beauty types reference
                $tblSpravBeautyTypes = new Table('sprav_beautytype');
                $beautyTypesData = $tblSpravBeautyTypes->getAllRows();

                $beautyTypes = [];
                foreach ($beautyTypesData as $beautyType) {
                    $beautyTypes[mb_strtolower($beautyType['title'])] = $beautyType['id'];
                }

                $beautyTypeName = $objectData['type'];
                $beautytypeId = null;
                if (isset($beautyTypes[mb_strtolower($beautyTypeName)])) {
                    $beautytypeId = $beautyTypes[mb_strtolower($beautyTypeName)];
                } else {
                    // report error - unrecognized beauty type
                    $errors[] = 'Не найден тип: ' . $beautyTypeName;
                }
            }

            if ($errors) {
                return ['success' => false, 'errors' => $errors];
            }
            
            // search client by name
            $clientId = null;
            if ($objectData['client_title'] != '' && $objectData['client_title'] != '-') {
                $tblClients->setOptions(['WHERE' => 'title=' . $tblClients->escape($objectData['client_title'])]);
                $client = $tblClients->getRow();
                if ($client) {
                    $clientId = $client['id'];
                }
            }
            
            if (!$clientId) {
                // добавляем запись в krasp_clients
                $clientData = [
                    'title' => $objectData['client_title'],
                    'fio' => $objectData['client_name'],
                    'phone' => $objectData['client_phone'],
                    'email' => $objectData['client_email'],
                    'klfio' => $objectData['contact_name'],
                    'klphone' => $objectData['contact_phone'],
                    'klemail' => $objectData['contact_email'],
                    'ur_lico' => $objectData['legal_entity'],
                    'requisites' => ''                    
                ];
                $tblClients->saveRow($clientData, $code);
                $clientId = $tblClients->lastInsertId;
            }

            // добавляем запись в krasp_objects
            $originalFolderName = $this->generateFolderName($objectData['title']);
            $folderName = $originalFolderName;
            $j = 1;
            while ($this->existsFolder($folderName)) {
                $folderName = $originalFolderName . $j;
                $j++;
            }

            $dbObjectData = [
                'title' => $objectData['title'],
                'linktype' => 5, // besplatno
                'folder' => $this->generateFolderName($objectData['title']),
                'client_id' => $clientId,
                'territory_id' => $territoryId,
                'site' => $objectData['site'],
                'logo' => ''                
            ];
            $tblObjects->saveRow($dbObjectData, $code);
            $objectId = $tblObjects->lastInsertId;

            // добавляем запись в krasp_objects_contacts
            $objectContactData = [
                'title' => '',
                'object_id' => $objectId,
                'address' => $objectData['address'],
                'phone' => $objectData['phone'],
                'email' => ''                
            ];
            $tblObjectsContacts->saveRow($objectContactData, $code);

            // добавляем запись в krasp_beauties
            $beautyData = [
                'beautytype_id' => $beautytypeId,                
                'object_id' => $objectId
            ];
            $tblBeauties->saveRow($beautyData, $code);
        }

        return ['success' => true, 'id' => $objectId];
    }

    
    public function removeObject($objectId, $categoryId)
    {        
        $errors = [];
        // remove record from krasp_objects table
        $tblObjects = new Table('objects');
        $result = $tblObjects->deleteRowByID($objectId);        

        if (!$result) {
            $errors[] = 'ошибка удаления объекта';
        } else {
            // remove record from specific category table
            $tableName = $this->getCategoryTable($categoryId);
            if ($tableName) {
                $tblCategory = new Table($tableName);
                $result = $tblCategory->deleteRowByConditions(['object_id' => $objectId]);                
                if (!$result) {
                    $errors[] = 'ошибка удаления из таблицы ' . cfg::$prfx . $tableName;
                }
            }
        }

        if ($errors) {
            $result = ['success' => false, 'errors' => $errors];
        } else {
            $result = ['success' => true];
        }
        return $result;
    }


    private function getStatusFieldIndex($fields)
    {
        $flippedFields = array_flip($fields);
        return $flippedFields['status'];
    }

    private function getCategoryTable($categoryId)
    {
        switch ($categoryId) {
            case self::CAT_HOTELS:
                $categoryTable = 'hotels';
                break;
            case self::CAT_RESTAURANTS:
                $categoryTable = 'restaurants';
                break;
            case self::CAT_TRANSPORT:
                $categoryTable = 'transport';
                break;
            case self::CAT_BEAUTY_HEALTH:
                $categoryTable = 'beauties';
                break;
            case self::CAT_ACITIVITY_SUMMER:
                $categoryTable = 'activitysummer';
                break;
            case self::CAT_ACITIVITY_WINTER:
                $categoryTable = 'activitywinter';
                break;
            case self::CAT_ENTERTAINMENT:
                $categoryTable = 'enterts';
                break;
            case self::CAT_SERVICES:
                $categoryTable = 'services';
                break;
            case self::CAT_SHOPS:
                $categoryTable = 'shops';
                break;
            case self::CAT_ACITIVITY_CHILDREN:
                $categoryTable = 'activitychildren';
                break;
            default:
                $categoryTable = false;
        }
        return $categoryTable;
    }

    // this is to trim unicode non-breaking spaces
    private function trim($string)
    {
        return trim($string, " \t\n\r\0\x0B" . chr(0xC2) . chr(0xA0));
    }

 
    private function generateFolderName($objectTitle) {
        $cyr = ['ж',  'ч',  'щ',    'ш',  'ю',  'ё',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'ы', 'э'];
        $lat = ['zh', 'ch', 'shch', 'sh', 'yu', 'yo', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', '', '', 'ya', 'y', 'e'];
        $objectTitle = mb_strtolower($objectTitle);
        $folderName = str_replace(' ', '_', $objectTitle);
        $folderName = str_replace($cyr, $lat, $folderName);
        $folderName = preg_replace('/[^a-z0-9_\(\)\-]/', '', $folderName);

        return $folderName;
    }


    private function existsFolder($folderName) {
        $tblObjects = new Table('objects');
        $tblObjects->setOptions(['WHERE' => 'folder=' . $this->escape($folderName)]);
        $row = $tblObjects->getRow();
        return $row ? true : false;
    }
}