<?php

//## Класс для работы с таблицей

Class View extends Dbase
{
    var $table;
    var $tableName;
    
    function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
        $this->tableName = cfg::$prfx.$table;
    }
}