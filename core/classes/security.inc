<?php

class Security
{
    private $userid;
    private $aa = Array(); // admnaccess;
    private $m = Array(); // moduls;
    
    public $isAdmin;
    public $isUADS;
    public $isMark;
    public $isBuh;
    public $isCurator;
    
    function __construct($user_id, $iscurator = false)
    {
        $this->isAdmin = false;
        $this->isUADS = false;
        $this->isMark = false;
        $this->isBuh = false;
        $this->isCurator = $iscurator;
        if (!$this->isCurator)
        {
            $admins = new Table('admins'); $admin = $admins->getRowById($user_id);
            $adminRole = $admin['role'];
            
            if ($adminRole == 1) $this->isAdmin = true;
            elseif ($adminRole == 2) $this->isUADS = true;
            elseif ($adminRole == 3) $this->isMark = true;
            elseif ($adminRole == 5) $this->isBuh = true;
            
            $this->userid = $user_id;
            $ada = new Table('adminsaccess');
            $ada->setOptions(Array('where' => " (`user_id` = '$user_id') "));
            $adarows = $ada->getAllRows();
            if ($adarows)
            {
                foreach($adarows as $ak => $av)
                {
                    $this->aa[$av['modul_id']]['read'] = $av['read']==1?true:false;
                    $this->aa[$av['modul_id']]['edit'] = $av['edit']==1?true:false;
                }
            }
            
            $m = new Table('moduls');
            $mm = $m->getAllRows();
            if ($mm) foreach($mm as $k => $v)
            {
                $this->m[$v['sysname']] = $v['id'];
                if ($this->isAdmin)
                {
                    $this->aa[$v['id']]['read'] = true;
                    $this->aa[$v['id']]['edit'] = true;
                }
            }
        }
    }
    
    function getModulId($modulname)
    {
        if (isset($this->m[$modulname])) return $this->m[$modulname]; else return 0;
    }
    
    function isRead($modulname)
    {
        $result = false;
        $id = $this->getModulId($modulname);
        if (isset($this->aa[$id])) $result = $this->aa[$id]['read']; else $result = false;
        
        if ($this->isAdmin) $result = true;
        return $result;
    }
    
    function isEdit($modulname)
    {
        $result = false;
        $id = $this->getModulId($modulname);
        if (isset($this->aa[$id])) $result = $this->aa[$id]['edit']; else $result = false;
        
        if ($this->isAdmin) $result = true;
        return $result;
    }
    
    function isAccess($modulname)
    {
        return ($this->isRead($modulname) || $this->isEdit($modulname) || $this->isAdmin);
    }
    
    function isAdmin()
    {
        return $this->isAdmin;
    }
}