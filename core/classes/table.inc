<?php

//## Класс для работы с таблицей

Class Table extends Dbase
{
    var $tableTitle;

    var $table;
    var $tableName;
    var $tableConfig;
    var $defMaxRows = 50;
    
    protected $fieldsTitle;
    protected $fieldsName;
    protected $isStopCreateTable;

    var $errorMessage;
    
    private $defFields = Array('req' => 0, 'showtable' => 0);
    
    function __construct($table, $stopCreateTable = true)
    {
        parent::__construct();
        $this->table = $table;
        $this->tableName = cfg::$prfx.$table;
        $this->loadConfigTable();
        $this->setDefaultOptions();
        $this->isStopCreateTable = $stopCreateTable;
    }
    
    function getTableConfig()
    {
        $fields = $this->tableConfig->fields;
        foreach($fields as $fk => $fv)
        {
            foreach($this->defFields as $dfk => $dfv)
            {
                if (!isset($fv->$dfk)) $fv->$dfk = $dfv;
            }
            if (!isset($fv->inputText)) $fv->inputText = $fv->title;
        }
        $this->tableConfig->fields = $fields;
        return $this->tableConfig;
    }
    
    function post()
    {
        $name = $this->table.'_submit';
        return isset($_POST[$name]);
    }
    
    function unsetpost()
    {
        $name = $this->table.'_submit';
        unset($_POST[$name]);
    }
    
    function validForm($action = '', $id = 0)
    {
        global $router;
        $flds = $this->getTableConfig()->fields;
        $_err = '';
        foreach($flds as $fk => $fv)
        {
            if ($fv->req)
            {
                if ($fv->type == 'flag')
                {
                    if ($_POST[$fv->name] == 0) $_err .= 'Не отмечен пункт "'.$fv->title.'"<br>';
                }
                elseif ($fv->type == 'subtable')
                {
                    if ($_POST[$fv->name] == 'none') {
                        $_err .= 'Не выбрано значение в поле "'.$fv->title.'"<br>';
                    }
                }
                elseif ($fv->type == 'file')
                {
                    if (trim($_FILES[$fv->name]['name'])==''
                        && $_POST[$fv->name.'_exists']=='0'
                        ) $_err .= 'Не выбран файл "'.$fv->title.'"<br>';
                }
                else
                {
                    if (!isset($_POST[$fv->name]) || trim($_POST[$fv->name])=='') $_err .= 'Не заполнено поле "'.$fv->title.'"<br>';
                }
            }

            if (isset($fv->uniq))
            {
                $val = $_POST[$fv->name];
                
                if ($action == '' && $id == 0)
                {
                    if ($router->getAction(2) == 'new') $this->setOptions(Array('where'=> " `$fv->name`='$val' " ));
                    else $this->setOptions(Array('where'=> " (`$fv->name`='$val') and (`id`!='".$router->getAction(2)."') " ));
                }
                else
                {
                    if ($action == 'new') $this->setOptions(Array('where'=> " `$fv->name`='$val' " ));
                    else $this->setOptions(Array('where'=> " (`$fv->name`='$val') and (`id`!='$id') " ));
                }
                
                $r = $this->getRow();
                if ($r)
                {
                    $_err .= 'Значение поля "'.$fv->title.'" должно быть уникальным<br>';
                }
            }
        }
        $this->errorMessage .= $_err;
        return (trim($_err)=='');
    }
    
    function getFormValues($showData = false, $clear = false)
    {
        global $helper;
        $result = Array();
        $flds = $this->getTableConfig()->fields;
        #echo '<pre>'; print_r($_POST); print_r($flds);
        foreach($flds as $fk => $fv)
        {
            if ($clear)
            {
                $result[$fv->name] = '';
            }
            else
            {
                if (isset($_POST[$fv->name]))
                {
                    $value = $_POST[$fv->name];
                    
                    if ($fv->type == 'folder')
                    {
                        if (trim($value)=='') $value = $helper->createFolderName($_POST['title']);
                            else $value = $helper->createFolderName($value);
                    }

                    $result[$fv->name] = $value;
                    
                    if ($fv->type == 'password')
                    {
                        if (trim($value)=='') unset($result[$fv->name]);
                        else $result[$fv->name] = md5($value);
                    }
                    elseif ($fv->type == 'flag')
                    {
                        if ($value == '0') $value = false; else $value = true;
                        $result[$fv->name] = $value;
                    }
                    elseif ($fv->type == 'date' && trim($value)!='')
                    {
                        $result[$fv->name] = strtotime(substr($value,0,10));
                        /*
                        $dt = explode('.',$value);
                        if (count($dt)==3)
                        {
                            if ($showData) $result[$fv->name] = $value;
                                else $result[$fv->name] = mktime(0,0,0, $dt[1], $dt[0], $dt[2]);
                        }
                        /**/
                    }
                    elseif ($fv->type == 'datetime' && trim($value)!='')
                    {
                        $result[$fv->name] = strtotime($value);
                    }
                    elseif ($fv->type == 'subtable_checkboxes')
                    {
                        
                        $result[$fv->name] = json_encode($value);
                    }

                    if ($fv->type == 'date' && trim($value)=='') {
                        $result[$fv->name] = null;
                    }
                    
                }

                
                if ($fv->type == 'file' && isset($_FILES[$fv->name]))
                {
                    $result[$fv->name] = $_FILES[$fv->name]['name'];
                }

//                if ($fv->type == 'money') {
//                    $result[$fv->name] = round($value,2);
//                }
            }
        }
        return $result;
    }
    
    private function loadConfigTable()
    {
        $cfgFile = FULL_ROOT.'core'.DS.'db'.DS.$this->table.'.cfg';
        if (!file_exists($cfgFile))
        {
            if (!isset($_GET['createcfgtables']))
            {
                die('<b style="color:red;">Не могу найти конфигурационный файл для таблицы "'.$this->table.'"!</b>');
            }
            else
            {
                $cfgTableTempl = Array(
                    'title' => '{title}',
                    'name'  => '{name}',
                    'options' => Array('key' => 'value'),
                    'fields' => Array(
                        Array(
                            'title' => 'Наименование',
                            'name' => 'title',
                            'type' => 'string',
                            'req' => 1,
                        ),
                        Array(
                            'title' => 'Статус',
                            'name' => 'status',
                            'type' => 'flag'
                        ),
                    )
                );
                
                $cfgTableTempl = '
                {
                    "title":"{title}",
                    "name":"{name}",
                    "options":{
                        "key":"value"
                    },
                    "fields":[
                        {
                            "title":"Наименование",
                            "name":"title",
                            "type":"string",
                            "req":1
                        },
                        {
                            "title":"Статус",
                            "name":"status",
                            "type":"flag"
                        }
                    ]
                }
                ';
                
                $cfgTableTempl = str_replace('                ','',$cfgTableTempl);
                $cfgTableTempl = str_replace('{title}',$this->tableName,$cfgTableTempl);
                $cfgTableTempl = str_replace('{name}',$this->table,$cfgTableTempl);
                
                //##$dd = json_decode($cfgTableTempl); echo '<pre>'; print_r($dd); exit;
                
                $fp = fopen($cfgFile,'w'); fwrite($fp,$cfgTableTempl); fclose($fp);
                
                die('<b style="color:red;">Создался конфигурационный файл для таблицы "'.$this->tableName.'"! Настройте таблицу перезагрузите страницу.</b>');
            }
        }
        else
        {
            $bf = file_get_contents($cfgFile);
            $this->tableConfig = json_decode($bf);
            if (!$this->tableConfig)
            {
                $jsonerror = '';
                switch (json_last_error())
                {
                    case JSON_ERROR_NONE:
                        $jsonerror =  ' - Ошибок нет';
                        break;
                    case JSON_ERROR_DEPTH:
                        $jsonerror =  ' - Достигнута максимальная глубина стека';
                        break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $jsonerror =  ' - Некорректные разряды или не совпадение режимов';
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        $jsonerror =  ' - Некорректный управляющий символ';
                        break;
                    case JSON_ERROR_SYNTAX:
                        $jsonerror =  ' - Синтаксическая ошибка, не корректный JSON';
                        break;
                    case JSON_ERROR_UTF8:
                        $jsonerror =  ' - Некорректные символы UTF-8, возможно неверная кодировка';
                        break;
                    default:
                        $jsonerror =  ' - Неизвестная ошибка';
                        break;
                }
                die('Ошибка преобразования - '.$cfgFile.$jsonerror);
            }

            $this->fieldsName = Array(0=>'id',1=>'created', 2=>'updated');
            $this->fieldsTitle = Array(0=>'ID',1=>'создано', 2=>'обновлено');
            
            if (!$this->isTableExists())
            {
                $this->createTable();
            }
            
            global $helper;
            $sth = $this->q('show columns from `'.$this->tableName.'`');
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sth->fetchAll();
            $fields = $this->tableConfig->fields;
            
            $prevField = '';
            $stop = false;
            foreach($fields as $fk => $fv) if (!in_array($fv->type,Array('virtual','link','compile')))
            {
                $find = false;
                $name = $fv->name;
                if (!in_array($fv->type, Array('virtual','link','compile','gallery'))) {
                    foreach ($rows as $rk => $rv) {
                        $field = $rv['Field'];
                        if ($name == $field) $find = true;
                    }
                    if (!$find) {
                        $stop = true;
                        echo 'В базе не найдено поле ' . $name;
                        echo $this->createField($fv, $prevField);
                        echo '<br>';
                    }
                }
                $prevField = $name;
            }
            
            if ($stop && $this->isStopCreateTable) exit;
            
            $this->tableTitle = $this->tableConfig->title;

            $flds = $this->tableConfig->fields;
            foreach($flds as $fk => $fv)
            {
                $this->fieldsName[$fk+3] = $fv->name;
                $this->fieldsTitle[$fk+3] = $fv->title;
            }
        }
        
        $this->initTable();
    }
    
    function setDefaultOptions()
    {
        if (isset($this->getTableConfig()->options))
        {
            $cfg = $this->getTableConfig()->options;
            foreach($cfg as $ck => $cv)
            {
                if ($ck == 'sort')
                {
                    $this->arrayOptions['ORDER'] = $cv;
                }
            }
        }
        $this->setOptions($this->arrayOptions);
    }
    
    protected function initTable()
    {
        $this->initDBTable();
    }
}
