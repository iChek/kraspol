<?php

// Подключение всех остальных файлов и автозагрузка классов

global $_telo, $debug, $_errortelo, $_breadcrumbs;

function core_autoload($class)
{
    $classPath = cfg::$full_root . 'core'.DS.'classes'.DS;
    $modelPath = $classPath . 'models' . DS;
    $classFile = $classPath . strtolower($class) . '.inc';
    $modelFile = $modelPath . strtolower($class) . '.inc';

    if (file_exists($classFile)) {
        require $classFile;
    } else if (file_exists($modelFile)) {
        require $modelFile;
    }
}
$result = spl_autoload_register('core_autoload');

$ses = new Sessions();
$tpl = new Templ();
$router = new Router($tpl);
$helper = new Helper();

$tpl->add('header_link',SITE_ROOT);
$tpl->add('http_host',HTTP_HOST);
$tpl->add('site_host',SITE_HOST);

$router->start();

// загрузка настроек сайта
$options = new Table('options');
$opts = $options->getAllRows();
if ($opts)
{
    foreach($opts as $ok => $ov)
    {
        $key = '_o_'.$ov['key'];
        global $$key;
        $$key = $ov['value'];
        if (!strstr($key,'smtp')) $tpl->add($key,$ov['value']);
    }
}

include 'funcs.inc';