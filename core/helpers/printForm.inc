<?php

function printForm($args)
{
    global $debug, $helper, $router;
    $form = '';
    $formName = $args[0];
    
    $formFile = FULL_ROOT.$router->filePath.'tpl'.DS.'forms'.DS.$formName.'.html';
    
    $formTelo = file_get_contents($formFile);
    
    $form .= '
    <form action="" method="post" id="'.$formName.'form" class="form-horizontal">
    <input type="hidden" name="'.$formName.'submit" value="1">
    '.$formTelo.'
    </form>
    ';
    
    return $form;
}