<?php

/* Конвертирует название в правильный формат папки */

function iz_r2e($text)
{
    $mr = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
    'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е',
    'Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ',
    'Ъ','Ы','Ь','Э','Ю','Я',' ','№',',');
    $me = array(
    'a','b','v','g','d','e','yo','zh','z','i','i','k','l','m','n','o','p',
    'r','s','t','u','f','h','ts','ch','sh','sch','','y','','e','u','ya',
    'a','b','v','g','d','e','yo','zh','z','i','i','k','l','m','n','o','p',
    'r','s','t','u','f','h','c','ch','sh','sch','','y','','e','u','ya',
    '_','N','');

    $stringText = $text;
    for ($i = 0; $i < count($mr); $i++) $stringText = str_replace($mr[$i],$me[$i],$stringText);
    return strtolower($stringText);
}

function createFolderName($args)
{
    $name = $args[0];
    $tmp = iz_r2e(trim($name));
    $tmp = preg_replace('/[^\w\d]/','',$tmp);
    return $tmp;
}