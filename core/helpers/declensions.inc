<?php

// Функция для отображение ошибочныхсообщений!

function declensions($args)
{
    $val = $args[0];
    $expr = $args[1];
    #Array('день','дня','дней')
    settype($val, "integer"); 
    $count = $val % 100; 
    if ($count >= 5 && $count <= 20)
    { 
        $result = $expr['2']; 
    }
    else
    { 
        $count = $count % 10; 
        if ($count == 1)
        { 
            $result = $expr['0']; 
        }
        elseif ($count >= 2 && $count <= 4)
        { 
            $result = $expr['1']; 
        }
        else
        { 
            $result = $expr['2'];
        } 
    } 
    return $result; 
}