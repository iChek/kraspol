<?php

/* Функция aprn выводящая массив в форматированном виде */

function aprn($args,$retValue = true)
{
    $obj = $args[0];
    $ret = '<pre>'.print_r($obj,true).'</pre>';
    if ($retValue) return $ret; else echo $ret;
}