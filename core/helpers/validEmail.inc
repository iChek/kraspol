<?php

// Функция для проверки введённого e-mail

function validEmail($args)
{
    $email = $args[0];
    #if (!preg_match("/[^(\w)|(\@)|(\.)|(\-)]/",$email)) return true; else return false;
    if (function_exists("filter_var"))
    {
        $s = filter_var($email, FILTER_VALIDATE_EMAIL);
        return !empty($s);
    }
    else
    {
        $p = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*';
        $p.= '@([-a-z0-9]+\.)+([a-z]{2,3}';
        #$p.= '|info|arpa|aero|coop|name|museum|mobi)$';
        $p .= '/ix';
        return preg_match($p, $email);
    }
}
