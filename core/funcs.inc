<?php

function getGiftBalls($user_id)
{
    $gbballs = 0;
    
    $gb = new Table('giftballs');
    $gb->setOptions(Array('where' => " (`user_id`='$user_id') and (`status`='1') "));
    $gbrows = $gb->getAllRows();
    if ($gbrows) foreach($gbrows as $gbk => $gbv) $gbballs += ($gbv['balls'] - $gbv['useballs']);
    return $gbballs;
}

function getActiveBalls($user_id)
{
    $tme = time();
    // расчёт доступных баллов
    $conts = new Table("contracts");
    $conts->setOptions(Array('where' => "(`user_id`='$user_id') 
                                     and (`status`='2')
                                     and (`activeballs`='1')
                                     and (`useballs` < `balls`)
                                     "
                            ));
                            
    $activeSummBalls = 0;
    $contracts = $conts->getAllRows();
    if ($contracts)
    {
        foreach($contracts as $ck => $cv) $activeSummBalls += ($cv['balls']-$cv['useballs']);
    }
    return $activeSummBalls;
}

function useDownBalls($price, $user_id)
{
    $conts = new Table('contracts');
    $conts->setOptions(Array('where' => "(`user_id`='$user_id') 
                                     and (`status`='2')
                                     and (`activeballs`='1')
                                     and (`useballs` < `balls`)
                                     ",
                          'order' => " `activedate` "
                            ));
    $contracts = $conts->getAllRows();
    if ($contracts)
    {
        foreach($contracts as $ck => $cv)
        {
            // взяли уже использованные баллы в полисе
            $usePolisBalls = $cv['useballs'];
            
            $balls = $cv['balls'] - $usePolisBalls;
            if ($price >= $balls)
            {
                $useballs = $balls + $usePolisBalls;
                $price -= $balls;
                $cv['status'] = 4;
                $cv['useballsdate'] = time();
                // тут мы гасим все баллы полиса
            }
            else
            {
                $useballs = $usePolisBalls + $price;
                $price = 0;
                // тут мы частично гасим баллы полиса
            }
            $cv['useballs'] = $useballs;
            $conts->updateRow($cv);
            
            if ($price == 0) break;
        }
    }

    if ($price > 0)
    {
        // не все баллы использованы
        $gb = new Table('giftballs');
        $gb->setOptions(Array('where' => " (`user_id`='$user_id') 
                                       and (`status`='1')
                                       and (`useballs` < `balls`)
                                       ",
                              'order' => " `created` " ));
        $gbrows = $gb->getAllRows();
        foreach($gbrows as $gbk => $gbv)
        {
            $useGiftBalls = $gbv['useballs'];
            
            $balls = ($gbv['balls'] - $useGiftBalls);
            if ($price >= $balls)
            {
                $useballs = ($balls + $useGiftBalls);
                $price -= $balls;
                $gbv['status'] = 2;
                $gbv['useballsdate'] = time();
                // тут мы гасим все баллы полиса
            }
            else
            {
                $useballs = $useGiftBalls + $price;
                $price = 0;
                // тут мы частично гасим баллы полиса
            }
            $gbv['useballs'] = $useballs;
            $gb->updateRow($gbv);
            if ($price == 0) break;
        }
        
    }
}

function unuseDownBalls($spend, $user_id)
{
    // откатываем покупку подарков, начиная с подарочных
    $gb = new Table('giftballs');
    $gb->setOptions(Array('where' => " (`user_id`='$user_id') 
                                   and ((`status`='2') or ((`useballs` < `balls`) and (`useballs`>0)))
                                   ",
                          'order' => " `created` DESC " ));
                          
    global $_telo; $_telo .= '<br>';
    
    $gbrows = $gb->getAllRows();
    if ($gbrows)
    {
        foreach($gbrows as $gbk => $gbv)
        {
            $_telo .= $gbv['title'].'-'.$gbv['balls'].'-'.$gbv['useballs'].'<br>';
            
            $useballs = $gbv['useballs'];
            if ($spend >= $useballs)
            {
                $spend -= $useballs;
                $gbv['useballs'] = 0;
            }
            else
            {
                $gbv['useballs'] -= $spend;
                $spend = 0;
            }
            $gbv['status'] = 1;
            //$_telo .= '<pre>'.print_r($gbv,true).'</pre>'.$spend.'<br>';
            $gb->updateRow($gbv);
            
            if ($spend == 0) break;
        }
    }
    
    if ($spend > 0)
    {
        // продолжаем возвращать баллы
        $conts = new Table("contracts");
        $conts->setOptions(Array('where' => "(`user_id`='$user_id') 
                                         and ((`status`='4') 
                                         or ((`activeballs`='1')
                                         and (`useballs` < `balls`)
                                         and (`useballs` > 0)))
                                         ",
                                 'order' => " `created` DESC "
                                ));
        $contracts = $conts->getAllRows();
        foreach($contracts as $cok => $cov)
        {
            $_telo .= $cov['title'].'-'.$cov['balls'].'-'.$cov['useballs'].'<br>';
            $useballs = $cov['useballs'];
            if ($spend >= $useballs)
            {
                $spend -= $useballs;
                $cov['useballs'] = 0;
            }
            else
            {
                $cov['useballs'] -= $spend;
                $spend = 0;
            }
            $cov['status'] = 2;
            //$_telo .= 'balls: '.$cov['balls'].'<br>';
            //$_telo .= 'useballs: '.$cov['useballs'].'<br>';
            //$_telo .= 'status: '.$cov['status'].'<br>';
            //$_telo .= 'spend: '.$spend.'<br>';
            $conts->updateRow($cov);
            
            if ($spend == 0) break;
        }
    }
}

function clearTime($tme)
{
    $a = explode('.',date('d.m.Y',$tme));
    return mktime(0,0,0,$a[1],$a[0],$a[2]);
}
















