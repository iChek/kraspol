<?php
$__swfupload = '
<script>
    var upload1, upload2;
    function uploadSuccess(file, serverData)
    {
        document.getElementById("uploadSuccess").innerHTML = "Файл успешно загружен<br>&nbsp;<br>";
        //alert(\'Файл успешно загружен\');
        //window.location.href = window.location.href;
    }
    
    function uploadStart(file)
    {
        document.getElementById("uploadSuccess").innerHTML = "";
        document.getElementById("btnCancel1").className = "showen";
        return true;
    }
    
    function uploadError(file, code, msg)
    {
        var err = "<span style=color:red;>Ошибка № " + msg + "</span><br>&nbsp;<br>";
        document.getElementById("uploadSuccess").innerHTML = err;
    }
    
    window.onload = function(){
        upload1 = new SWFUpload({
            // Backend Settings
            upload_url: "core/plugins/swfupload/upload2.php",
            post_params:{
                "PHPSESSID" : "'.$sessionid.'",
                "upload_dir1" : "certs/"
            },
            // File Upload Settings
            file_size_limit : "100MB",
            file_types : "*.*",
            file_types_description : "All Files",
            file_upload_limit : "10",
            file_queue_limit : "0",
            // Event Handler Settings (all my handlers are in the Handler.js file)
            file_dialog_start_handler : fileDialogStart,
            file_queued_handler : fileQueued,
            file_queue_error_handler : fileQueueError,
            file_dialog_complete_handler : fileDialogComplete,
            upload_start_handler : uploadStart,
            upload_progress_handler : uploadProgress,
            upload_error_handler : uploadError,
            upload_success_handler : uploadSuccess,
            upload_complete_handler : uploadComplete,
            // Button Settings
            button_image_url : "/core/plugins/swfupload/xpbuttonuploadtext_120x22.png",
            button_placeholder_id : "spanButtonPlaceholder1",
            button_width: 120,
            button_height: 22,
            // Flash Settings
            flash_url : "/core/plugins/swfupload/swfupload.swf",
            custom_settings : {
                progressTarget : "fsUploadProgress1",
                cancelButtonId : "btnCancel1"
            },
            // Debug Settings
            debug: false
        });
    }
</script>';

