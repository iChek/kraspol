<?php
error_reporting(E_ALL);

include 'config.inc';
include cfg::$full_root . 'core/core.inc';
include $router->beginFile;
include $router->modulFile;

$tpl->add('_telo',$_telo);
$tpl->add('debug',$debug);
$tpl->add('_errortelo',$_errortelo);
$tpl->add('_breadcrumbs',$_breadcrumbs);
$tpl->show();
