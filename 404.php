<?php header("HTTP/1.0 404 Not Found");

$tpl->indexPage('kps');
$_telo .= '<style>
#slider {
    height: auto;
    background-image: none !important;
}
h1.page_title {
    /*display: none;*/
}
</style>';

$page = $router->getAction(0);

$structs = new Table('structs');
$structs->setOptions(Array('where'=>" (`folder`='$page') AND (`status`='1') "));
$row = $structs->getRow();
if ($row)
{
    $titl = ($row['page_title'] != '' ? $row['page_title'] : $row['title']);
    $tpl->add('_title',$titl);
    $tpl->add('page_title',$titl);
    $_telo .= '<h1 class="text_h1">'.$titl.'</h1>';
    $_telo .= $row['text'];
}
else
{
    $_telo .= '<table class="page_not_found" width="100%" border="0" cellpadding="0" cellspacing="0" style="background: url(/users/tpl/images/404-2.jpg) no-repeat center;height: 400px;background-size: contain;margin-bottom: 10px;">
  <tbody>
    <tr>
      <td class="image page_title" style="text-align: center;color: #27d0f5;font-size: 50px;line-height: 0px;text-shadow: 0 1px 3px #00000096;">404</td></tr>
      <tr><td><br><br></td></tr>
<tr>
      <td class="description" style="text-align: center;color: #fafcfd;font-size: 18px;/* line-height: 18px; */text-shadow: 0 1px 3px #000;text-transform: uppercase;">
<div class="atention_description" style="background: rgba(154, 153, 153, 0.5);max-width: 839px;width:100%;margin:auto;padding: 15px 0;">
    <div class="title404" style="/* padding-top: 15px; */font-weight: bold;">Страница не найдена</div>
    <div class="subtitle404"></div>
    <div class="descr_text404" style="font-weight: bold;padding-top: 15px;">Извините, такого адреса (раздела сайта) не существует</div><br>
    <a class="button big_btn" href="http://kraspol.albi.ru"><span style="/*color: #12cef7;*/">Вернуться на <span style="color: #000;">kraspol.org</span></span></a>
</div>
      </td>
    </tr>
  </tbody>
</table>';
}